package com.htax.core.web.template.template.vo;

import java.util.Arrays;
import java.util.Date;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn.TYPE;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;
/**
 *     测试用户类
 * @author joe
 * 2019年10月8日 下午7:56:10
 */
@WsdTable(name = "T_USER")
public class TemplateVo extends ModuleFeatureBean {

	/**
	* 主键
	*/
	@WsdColumn(isId = true)
	private String id;
	/**
	* 用户名
	*/
	@WsdColumn(name = "user_name")
	private String userName;
	/**
	* 年龄
	*/
	private Integer age;
	/**
	* 收入
	*/
	private Double wages;
	/**
	* 邮箱
	*/
	private String email;
	/**
	* 简历
	*/
	private String resume;
	/**
	* 头像
	*/
	@WsdColumn(type = TYPE.BLOB, name = "head_sculpture")
	private byte[] headSculpture;
	/**
	* 时间戳
	*/
	private Date ts;
	/**
	* 生日
	*/
	private Date birthday;
	/**
	* 状态
	*/
	private String status;
	/**
	 * 接收界面传递的值用于server的属性，不往数据库插入
	 */
	@WsdNotDbColumn
	private String token;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Double getWages() {
		return wages;
	}
	public void setWages(Double wages) {
		this.wages = wages;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}
	public byte[] getHeadSculpture() {
		return headSculpture;
	}
	public void setHeadSculpture(byte[] headSculpture) {
		this.headSculpture = headSculpture;
	}
	public Date getTs() {
		return ts;
	}
	public void setTs(Date ts) {
		this.ts = ts;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "UserVo [id=" + id + ", userName=" + userName + ", age=" + age + ", wages=" + wages + ", email=" + email
				+ ", resume=" + resume + ", headSculpture=" + Arrays.toString(headSculpture) + ", ts=" + ts
				+ ", birthday=" + birthday + ", status=" + status + ", token=" + token + "]";
	}
}
