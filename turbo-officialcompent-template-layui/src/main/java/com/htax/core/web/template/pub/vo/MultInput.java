package com.htax.core.web.template.pub.vo;
/**
 * 
 * @author Administrator
 * 2019年8月28日 上午9:41:19
 	*多输入框交互测试
 */
public class MultInput {

	public String name;
	public int    num;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	@Override
	public String toString() {
		return "MultInput [name=" + name + ", num=" + num + "]";
	}
	
}
