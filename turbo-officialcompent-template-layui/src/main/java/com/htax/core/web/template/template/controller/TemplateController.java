package com.htax.core.web.template.template.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.htax.core.web.template.template.service.TemplateService;
import com.htax.core.web.template.template.vo.TemplateVo;
import com.htax.turbo.innercompent.base.controller.BaseController;
import com.htax.turbo.innercompent.base.exception.ServiceException;
/**
 * crud示例
 * @author eric
 *
 */
@RequestMapping("/t")
@Controller
public class TemplateController extends BaseController{

	@Autowired
	private  TemplateService  tTemplateService;
	
	@ResponseBody
	@RequestMapping("list")
	public Map<String, Object> templateList(){
		List<TemplateVo> rs;
		try {
			rs = tTemplateService.listByPara();
		} catch (ServiceException e) {
			return jsonFailResult(e.getMessage());
		}
		return jsonSuccessDataResult("获取成功", rs);
	}
}
