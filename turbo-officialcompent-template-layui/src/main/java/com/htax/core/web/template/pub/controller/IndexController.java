package com.htax.core.web.template.pub.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 公共框架模块
 * @author Administrator
 * 2019年9月20日 上午10:51:10
 */
@Controller
@RequestMapping("/")
public class IndexController {

  @RequestMapping("index")
  public String index(Model model) {
	System.out.println(1111);
    return "pub/index";
  }
  @RequestMapping("top")
  public String top(Model model) {
    return "pub/top";
  }
  @RequestMapping("left")
  public String left(Model model) {
    return "pub/left";
  }
}
