package com.htax.core.web.template.template.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.htax.core.web.template.template.vo.TemplateVo;
import com.htax.turbo.innercompent.base.DaoService;
import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.base.exception.ServiceException;
/**
 * 模板服务层
 * @author eric
 *
 */
@Service
public class TemplateService {

	/**
	 *  条件查询列表
	 * @return
	 * @throws ServiceException
	 */
	public List<TemplateVo>  listByPara() throws ServiceException{
		try {
			return DaoService.getDao().listModule("条件查询所有模板对象", TemplateVo.class, null);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
}
