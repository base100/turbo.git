require.config({
  //统一的js的根目录
  baseUrl: _ctx+'/static/js',
  paths: {
	jquery: '_tool-js/libs/jquery/jquery-2.2.4.min',
	mars3d:'_gis/libs/cesium-marsv/mars3d',
	Cesium:'_gis/libs/Cesium/Cesium',
	layui: "libs/layui/layui.all",
  },
  map: {
		'*': {
			'css': '_tool-js/require-css'//加载require-css.js
		}
  },
  shim: {
    mars3d:{deps: ['css!_gis/libs/cesium-marsv/mars3d.css','css!_gis/libs/Cesium/Widgets/widgets.css'] },
    Cesium:{exports:'Cesium'},
    layui:{exports: 'layui' }
  }
});

