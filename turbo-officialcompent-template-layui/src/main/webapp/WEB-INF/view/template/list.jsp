<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="base_withlayui.jsp"%>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>首页</title>
<script>
	/*初始化加载layui*/
	require([ "jquery", "layui" ], function($, layui) {
		$("[_totpage]").on("click", function() {
			var _totpage = $(this).attr("_totpage");
			alert(_totpage);
			$("#iframe_content")[0].contentWindow.location.href = _totpage;
		});
	});
</script>
</head>
<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
		<!-- 顶部菜单 -->
		<%@include file="top.jsp"%>
		<!-- 左侧菜单 -->
		<%@include file="left.jsp"%>
		<!-- 内容主体区域 -->
		<div class="layui-body">
			<div class="layadmin-tabsbody-item layui-show">
				<iframe  class="layadmin-iframe" id="iframe_content"></iframe>
			</div>
		</div>
	</div>
</body>
</html>