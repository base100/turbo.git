<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
	pageContext.setAttribute("ctx", contextPath);
%>
<div class="layui-side layui-bg-black layui-side-menu">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree"  lay-filter="test">
        <li class="layui-nav-item layui-nav-itemed">
          <a class="" href="javascript:;">GIS</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;" _totpage="${ctx}/static/_examples/rq.jsp">简单示例</a></dd>
            <dd><a href="javascript:;" _totpage="http://www.sina.com">加载模型</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;">其他</a>
          <dl class="layui-nav-child">
            <dd><a href="javascript:;" _totpage="${ctx}/static/_examples/rq.jsp">列表一</a></dd>
            <dd><a href="javascript:;" _totpage="">列表二</a></dd>
            <dd><a href="javascript:;" _totpage="http://www.baidu.com">超链接</a></dd>
          </dl>
        </li>
        <li class="layui-nav-item"><a href="">其他</a></li>
        <li class="layui-nav-item"><a href="">其他</a></li>
      </ul>
    </div>
</div>