<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	pageContext.setAttribute("ctx", path);
%>

<script type="text/javascript">
	var _ctx = '${ctx}';
	var _static = _ctx+"/static/";
</script>

<link rel="stylesheet" href="${ctx}/static/js/libs/layui/css/layui.css"  media="all">
<link rel="stylesheet" href="${ctx}/static/css/layui_ext.css"  media="all">
<script src="${ctx}/static/js/_tool-js/require.min.js"></script>
<script src="${ctx}/static/js/require.conf.js"></script>