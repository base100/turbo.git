require.config({
  //统一的js的根目录
  baseUrl: _ctx+'/static/js',
  paths: {
	jquery: '_tool-js/libs/jquery/jquery-2.2.4.min',
	mars3d:'_gis/libs/cesium-marsv/mars3d',
	Cesium:'_gis/libs/Cesium/Cesium',
	layui: "libs/layui/layui.all",
	treeTable:"libs/treeTable/treeTable",
	echarts:"_tool-js/libs/echarts3/echarts.min",
	uml:"libs/js2uml/UDModules",
	xmSelect:"libs/xm-select/xm-select-my"
  },
  map: {
		'*': {
			'css': '_tool-js/require-css'//加载require-css.js
		}
  },
  shim: {
    mars3d:{deps: ['css!_gis/libs/cesium-marsv/mars3d.css','css!_gis/libs/Cesium/Widgets/widgets.css'] },
    treeTable:{deps: ['css!libs/treeTable/treeTable.css','css!../css/treeTable_ext.css']},
    Cesium:{exports:'Cesium'},
    layui:{exports: 'layui',deps: ['css!libs/layui/css/layui.css','css!../css/layui_ext.css'] },
    uml:{deps:['css!libs/js2uml/css/UDStyle.css','libs/js2uml/UDCore']}
  }
});

