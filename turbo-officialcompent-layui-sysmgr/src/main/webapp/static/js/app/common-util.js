/**
 * 一些工具类
 * v1.0.1
 * author:joe
 */
define([ "jquery","layui"], function($,layui) {
	var {table,layer,form,upload} = layui;
	var util = {};util.time = {};util.ui = {};util.ui.lay = {};util.obj = {};util.ajax = {}
	//时间相关
	util.time.now = function(){
		function _warp0(value){
    		return value<10?("0"+value):value;
    	}
        var today = new Date();
        var yyyy=today.getFullYear();
        var mm=_warp0(today.getMonth()+1);
        var dd=_warp0(today.getDate());
        var hh=_warp0(today.getHours());
        var m=_warp0(today.getMinutes());
        var ss=_warp0(today.getSeconds());
        return yyyy+'-'+mm+'-'+dd+' '+hh+':'+m+':'+ss;
	}
	util.time.worldNow = function(){
     	function _warp0(value){
     		return value<10?("0"+value):value;
     	}
     	 var localNow = new Date();
			 var utc = localNow.getTime()+ localNow.getTimezoneOffset() * 1000*60; //得到国际标准时间
         var today = new Date(utc);
         var yyyy=today.getFullYear();
         var mm=_warp0(today.getMonth()+1);
         var dd=_warp0(today.getDate());
         var hh=_warp0(today.getHours());
         var m=_warp0(today.getMinutes());
         var ss=_warp0(today.getSeconds());
         return yyyy+'-'+mm+'-'+dd+' '+hh+':'+m+':'+ss;
	}
	util.time.timerNow = function(){
		 window.setTimeout(util.time.now(),1000);
	 }
	util.time.toDataTimeStr = function(dataObj){
		 function _warp0(value){
	    		return value<10?("0"+value):value;
	    	}
        var d = new Date(dataObj);
        var yyyy=d.getFullYear();
        var mm=_warp0(d.getMonth()+1);
        var dd=_warp0(d.getDate());
        var hh=_warp0(d.getHours());
        var m=_warp0(d.getMinutes());
        var ss=_warp0(d.getSeconds());
        return yyyy+'-'+mm+'-'+dd+' '+hh+':'+m+':'+ss;
	 }
	util.obj.arrayExistAndNotEmpty = function(arrayObj){
		return typeof(arrayObj)!='undefined'&&Array.isArray(arrayObj)&&arrayObj.length!=0;
	}
	util.ui.form2Obj = function(form){
		var f_array = form.serializeArray();
		var r_obj = {};
		$(f_array).each(function(){
			r_obj[this.name] = this.value;
		});
		return r_obj;
	}	
	util.ui.pid2children2 = function(data,idStr,pidStr,childrenStr){
		var r = [],idArrayObj = {},i = 0,j = 0,len = data.length;
		for(;i<len;i++){
			idArrayObj[data[i][idStr]] = data[i];
		}
		for(;j<len;j++){
			var dataObj = data[j],pObj = idArrayObj[dataObj[pidStr]];
			if(pObj){
				!pObj[childrenStr]&&(pObj[childrenStr]=[]);
				pObj[childrenStr].push(dataObj)
			}else{
				r.push(dataObj);
			}
		}
		return r;
	}
	
	util.ui.children2array = function(childrenObj){
		var r = [];
		pushObj(childrenObj)
		function pushObj(arr){
			for(let i = 0;i<arr.length;i++){
				var o = arr[i];
				r.push(o);
				if(typeof(o.children)!='undefined'){
					pushObj(o.children);
				}
			}
		}
		return r;
	}
	
	util.ui.pid2children = function(data){
		return util.ui.pid2children2(data,'id','pid','children');
	}
	util.ui.pid2childrenAndSpreadAll = function(data,spread_all,field){
		if(spread_all){
			data.forEach((v)=>{
				v[field?field:'spread']=true;//layui 默认是spread
			});	
		}
		return util.ui.pid2children(data);
	}
	/**
	 * 表单赋值
	 */
	util.ui.layUiFormInit = function(formId/*不要#号*/,nameValueObj){
		form.val(formId,nameValueObj);
	}
	util.ui.showHide = function(showArray,hideArray){
		if(showArray){
			showArray.forEach((v)=>{$("#"+v).show()});
		}
		if(hideArray){
			hideArray.forEach((v)=>{$("#"+v).hide()});
		}
	}
	/**
	 * 动态构建layui下拉框并，绑定事件
	 */
	util.ui.layUIinitSelect = function(select_data_onchage_arry){
		var {form_id,option_data} = select_data_onchage_arry;
		option_data.forEach((sdoa)=>{
			var {selectId,value_col,text_col,data,onchange} = sdoa;
			if(typeof(data)!='undefined'&&data.length!=0){
				$("#"+selectId).empty();
				$("#"+selectId).append("<option value=''>请选择</option>");
				data.forEach((v)=>{
					var opiton_value = v[value_col],opiton_text = v[text_col];
					$("#"+selectId).append("<option value='"+opiton_value+"'>"+opiton_text+"</option>");
				});
				if(typeof(onchange)=='function'){
					form.on('select('+selectId+')',function(){
						var selected = $("#"+selectId).val();
						onchange(selected);
					});
				}
			}
		});
		//重新渲染所有下拉框
		form.render('select',form_id);
	}
	util.ui.layUIinitSelectMapData = function(select_data_onchage_arry){
		var {form_id,option_data} = select_data_onchage_arry;
		option_data.forEach((sdoa)=>{
			var {selectId,data,onchange} = sdoa;
			if(typeof(data)!='undefined'&&data.length!=0){
				$("#"+selectId).empty();
				$("#"+selectId).append("<option value=''>请选择</option>");
				data.forEach((v,k)=>{
					var opiton_value = k,opiton_text = v;
					$("#"+selectId).append("<option value='"+opiton_value+"'>"+opiton_text+"</option>");
				});
				if(typeof(onchange)=='function'){
					form.on('select('+selectId+')',function(){
						var selected = $("#"+selectId).val();
						onchange(selected);
					});
				}
			}
		});
		//重新渲染所有下拉框
		form.render('select',form_id);
	}
	util.time.tsColtoDataTimeStr = function(row){
		return util.time.toDataTimeStr(row.ts);
	}
	/**
	 * 成功提示
	 */
	util.ui.lay.successMsg = function(msg){
		layer.msg(msg, { icon : 1 });
	}
	/**
	 * 失败提示
	 */
	util.ui.lay.failMsg = function(msg){
		layer.msg(msg, { icon : 2 });
	}
	/**
	 * loadding提示
	 */
	util.ui.lay.loadding = function(){
		this.loadding_id = layer.load(0, {shade : false });
	}
	/**
	 * 关闭loadding提示
	 */
	util.ui.lay.closeLoadding = function(obj){
		if(obj.loadding_id){
			layer.close(obj.loadding_id);
		}
	}
	/**
	 * 这个时候的回调处理函数success是jquery成功后的回调，jquery调用失败，弹出操作失败提示
	 * 
	 * 
	 */
	util.ajax.busiJsonGet = function(option){
		option.type = "GET";
		busiJsonPost(option);
	}
	/**
	 * jsonPostWithBusiStatusCallBack
	 * 这个时候的回调处理函数success是layuiPagerJsonSuccessDataResult时的处理，jsonFailResult默认弹出异常信息。
	 */
	util.ajax.busiJsonPost = function(option){
		var showSuccessMsg = showSuccessMsg||true;//默认提示成功
		$.ajax({
			url : option.url,
			type : option.type||'POST',
			dataType : 'json',
			data : option.data,
			beforeSend:util.ui.lay.loadding,
			success : function(rep){
				util.ui.lay.closeLoadding(this);
				var {status,msg,data} = rep;
				if(status==0){//默认失败提示
					util.ui.lay.failMsg(msg);
				}else{
					if(showSuccessMsg){
						util.ui.lay.successMsg(msg);
					}
					if(option.success){
						option.success(data);
					}
				}
			},
			error : option.error||function(a,b) {
				util.ui.lay.closeLoadding(this);
				util.ui.lay.failMsg('操作失败');
			}
		});
	}
	/**jsonPostAndShowMsgAndCloseLayerAndReLoadTable
	 * 操作后仅仅返回提示信息，无需数据操作，需要对表做刷新和弹窗进行关闭动作，如：测试校验等动作
	 * 需要:option.url、option.data、table_id、layero（弹窗id）等参数
	 */
	util.ajax.busiJsonPostAndCloseLayerReLoadTable = function(option){
		var {url,data,table_id,layero,success} = option;
		var _success = function(rep_data){
			util.ui.lay.successMsg(msg);
			if(table_id){
				table.reload(table_id);
			}
			if(success){
				success(rep_data);
			}
			if(layero){
				layer.close(layero);
			}
		}
		option.success = _success;
		util.ajax.busiJsonPost(option);
		
	}
	return util;
});