/**
 * 对layui增删该查的封装
 * v1.0.1
 * author:joe
 */
define([ "jquery", "layui" ,'app/common-util','treeTable'], function($, layui,util,tt) {
	
	var {table,layer,form} = layui,treeTable = tt.treeTable,insTb,initTbOptions;
	var initTreeTable = function(options){
		initTbOptions = options;//用于刷新时使用
		var spread_all = options.spread_all;
		//初始化树
		$.ajax({
			url : options.url,
			type : 'POST',
			dataType : 'json',
			success : function(res) {
				var data = util.ui.pid2childrenAndSpreadAll(res.data,spread_all,"open");//tabletree组件的展开属性是open，layui tree是spread。
				console.log(data);
				// 渲染表格
				insTb = treeTable.render({
					treeColIndex:1,
					elem : options.elem,
					data : data,
					tree: {
		                iconIndex: options.iconIndex||2,
		                arrowType: 'arrow2',
		                getIcon: function (d) {
		                    if (d.children&&d.children.length>0) {
		                        return '<i class="ew-tree-icon ew-tree-icon-folder"></i>';
		                    } else {
		                        return '<i class="ew-tree-icon ew-tree-icon-file"></i>';
		                    }
		                }
		            },
					cols : options.cols,
					toolbar: options.toolbar,//这个是对treetable做了修改，原生不支持
					ctrl:options.ctrl,////这个是对treetable做了修改，原生不支持
					style : 'margin-top:0;'
				});
			},
			error : function(a,b) {
				layer.msg('初始化失败', { icon : 6 });
			}
		});
		
		/**
		 * treeTable支持行操作，可以使用layui的方法
		 */
		treeTable.on('tool('+options.elem.substring(1)+')',function(obj){
			options.ctrl[obj.event].call(this,obj.data);
			
		});
		return initTreeTable;
	}
	//crud 简化封装
	var tableAdd = function(options){
		var title = options.title;
		var btn = options.btn;
		var form_id = options.form_id;
		var url = options.url;
		layer.open({
			type:1,
			skin:'layui-layer-molv',
			title:title,
			shade:0,
			maxmin:true,
			anim:0,
			area:'auto',
			btn:['确定','关闭'],
			success:function(layero){
				layero.addClass('layui-form');
				layero.find('.layui-layer-btn0').attr({'lay-submit':'','lay-filter':''+form_id+''});
			},
			yes:function(layero){
				form.on('submit('+form_id+')',function(form){
					$.ajax({
						url : url,
						type : 'POST',
						dataType : 'json',
						data : $("#"+form_id).serialize(),
						success : function(msg) {
							layer.msg('添加成功', { icon : 6 });
							initTreeTable(initTbOptions);
							layer.close(layero);
						},
						error : function(a,b) {
							layer.msg('添加失败', { icon : 6 });
						}
					});
				});
			},
			btn1:function(layero){
				layer.close(layero);
			},
			content:$("#"+form_id)
		})
	}
	var tableDel = function(options){
		var url = options.url;
		var hasChild = options.hasChild
		if(hasChild){
			layer.msg('有子节点，无法删除！', { icon : 6 });
			return;
		}
		layer.confirm("确定要删除么？",function(index){
			$.ajax({
				url : url,
				type : 'get',
				dataType : 'json',
				success : function(msg) {
					layer.msg('删除成功', { icon : 6 });	
					initTreeTable(initTbOptions);
					layer.close(index);
				},
				error : function(a,b) {
					layer.msg('删除失败', { icon : 6 });
				}
			});
			layer.close(index)
		});	
	}
	//crud 简化封装
	var tableEdit = function(options){
		var title = options.title;
		var btn = options.btn;
		var form_id = options.form_id;
		var get_url = options.get_url;
		var url = options.url;
		var formDataSet = options.formDataSet;
		layer.open({
			type:1,
			skin:'layui-layer-molv',
			title:title,
			shade:0,
			maxmin:true,
			anim:0,
			area:'auto',
			btn:btn,
			success:function(layero){
				layero.addClass('layui-form');
				layero.find('.layui-layer-btn0').attr({'lay-submit':'','lay-filter':''+form_id+''});
				$.ajax({
					url : get_url,
					type : 'get',
					dataType : 'json',
					success : function(data) {
						if(formDataSet){
							formDataSet(data.data);
						}else{
							util.ui.layUiFormInit(form_id,data.data);
						}
					},
					error : function(a,b) {
						layer.msg('获取数据失败', { icon : 6 });
					}
				});
			},
			yes:function(layero){
				form.on('submit('+form_id+')',function(form){
					$.ajax({
						url : url,
						type : 'POST',
						dataType : 'json',
						data : $("#"+form_id).serialize(),
						success : function(msg) {
							layer.msg('修改成功', { icon : 6 });	
							initTreeTable(initTbOptions);
							layer.close(layero);
						},
						error : function(a,b) {
							layer.msg('修改失败', { icon : 6 });
						}
					});
				});
			},
			btn1:function(layero){
				layer.close(layero);
			},
			content:$("#"+form_id)
		})
	}
	
	return {tableCtrl:{initTreeTable,tableAdd,tableDel,tableEdit}} 
});