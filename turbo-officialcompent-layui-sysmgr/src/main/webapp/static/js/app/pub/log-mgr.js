require([ "jquery", "layui","app/common-layui-crud",'app/common-util'], function($, layui,crud,util) {
	var form = layui.form;
	var table = layui.table;
	//日志类型枚举
	var log_type = new Map([["01","业务日志"],["02","系统管理日志"],["03","安全日志"],["04","运维日志"]]);
	//表单操作时间参数，实际调用在app/common-layui-crud中封装
	var customCtrl = {
			batchDel:function(data){
				crud.tableCtrl.batchDel({
					table_id : "usertab",
					url:_ctx+'/_logmgr/batchDel',
					para_key:"ids"
				},data);
			}
	}
	//表格初始化参数
	var option = {
			elem : '#logtab',//表格id
			url : _ctx+'/_logmgr/list', //数据请求接口
			cols:[[ //表头
			    {type: 'checkbox',fixed: 'left',},
			    {type: 'numbers',fixed: 'left',align : 'center',width: '5%',title : '序号'},
			    {field : 'id',align : 'center',width: '0%',title : 'ID',hide:true},
			    {field : 'logType',width : '15%',align : 'center',title : '日志类型 ',templet:function(row){
			    	return log_type.get(row.logType);
			    }},
				{field : 'model',width : '15%',align : 'center',title : '模块'},
				{field : 'action',width : '15%',align : 'center',title : '操作'},
				{field : 'result',width : '15%',align : 'center',title : '操作结果'},
				{field : 'logLevel',width : '15%',align : 'center',title : '日志级别'},
				{field : 'msg',width : '15%',align : 'center',title : '日志详情'},
				{field : 'requestIp',width : '15%',align : 'center',title : '客户端ip'},
				{field : 'logTime',width : '15%',align : 'center',title : '时间',templet:util.time.tsColtoDataTimeStr},
				{fixld : 'right',width : '15%',align : 'center',toolbar:"#ctrlbar",title : '操作'}
				]],
			where:util.ui.form2Obj($("#search_form")),
			ctrl:customCtrl//操作方法的实现，方法名和按钮的id需一致。传入选择的数据，行操作返回当前条，toolbar操作返回数组
		}
	
	//执行初始化table
	crud.tableCtrl.initTable(option);
	//初始化查询条件
	util.ui.layUIinitSelectMapData({
		form_id:"search_form",
		option_data:[{selectId:"logType",data:log_type}]
	});
	//点击查询，可能会有特殊处理，暂时没有封装，
	$("#search_btn").on("click",function(){
		table.reload("logtab",{where:util.ui.form2Obj($("#search_form"))});
	});
});