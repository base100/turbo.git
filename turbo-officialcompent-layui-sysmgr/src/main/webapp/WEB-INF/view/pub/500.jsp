<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%
	String path = request.getContextPath();
	pageContext.setAttribute("ctx", path);
%>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>500</title>
<style type="text/css">
 .S404{
	 width: 500px;
	 height: 300px;
	 line-height:300px;
	 margin: 0 auto;
	 margin-top:100px;
	 size:20px;
	 background-color: #f1f1f1;
	 border-radius:10px;
	 text-align: center;
	 color: #ff0000;
	 background-image: url('${ctx}/static/img/pub/404.png') 
 }
</style>
</head>
<body >
	<div class="S404">
		系统异常,请联系管理员...
	</div>
</body>
</html>