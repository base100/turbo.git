<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../../pub/base_withlayui.jsp"%>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>首页</title>
<script type="text/javascript" src="${ctx}/static/js/app/pub/log-mgr.js"></script>
</head>
<body class="layui-layout-body" >
<!-- 查询及表单部分 -->
	<div style="padding: 15px">
		<form class="layui-form layui-form-pane" id="search_form" lay-filter="search_form" >
			<div class="layui-form-item" style="display: inline;">
				<div class="layui-inline">
					<label class="layui-form-label">日志类型</label>
					<div class="layui-input-inline">
						<select  lay-filter="logType" name="logType" id="logType">
							<option value="">请选择</option>
						</select>
					</div>
				</div>
				<div class="layui-inline">
					<div class="layui-btn" data-type="reload"  id="search_btn"><i class="layui-icon layui-icon-search"></i></div>
				</div>
			</div>
		</form>
		<table  id="logtab" lay-filter="logtab"></table>
		<script type="text/html" id="toolbar">
			<div class = "ayui-btn-container">
				<a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="batchDel">批量删除</a>
			</div>
		</script>
	</div>
<!-- 查询及表单部分end -->
</body>
</html>