package com.htax.turbo.innercompent.sysmgr.log.service;

import org.springframework.stereotype.Service;

import com.htax.turbo.innercompent.sysmgr.log.vo.LogBean.LOG_TYPE;

/**
 * 日志记录api
 * @author qiao
 * 2020年4月27日下午3:17:19
 */
@Service
public class LogApi {

	//业务操作入库
	public static void busiLog(String module,String action,String result,String msg){
		LogMgrService.dbLog(module, LOG_TYPE.BUSILOG, action, result, msg);
	}
	//安全审计日志操作入库
	public static void safeLog(String module,String action,String result,String msg){
		LogMgrService.dbLog(module, LOG_TYPE.SAFELOG, action, result, msg);
	}
	//运维操作日志入库
	public static void sysLog(String module,String action,String result,String msg){
		LogMgrService.dbLog(module, LOG_TYPE.SYSLOG, action, result, msg);
	}
}
