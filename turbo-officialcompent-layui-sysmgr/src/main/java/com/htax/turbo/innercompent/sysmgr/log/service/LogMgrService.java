package com.htax.turbo.innercompent.sysmgr.log.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.htax.core.tools.util.NetUtil;
import com.htax.core.tools.util.UUIDGenerator;
import com.htax.turbo.innercompent.base.DaoService;
import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.base.handler.thread.ThreadPool;
import com.htax.turbo.innercompent.base.para.ParaGetTool;
import com.htax.turbo.innercompent.orm.ormcreater.WhereCondition;
import com.htax.turbo.innercompent.orm.ormcreater.valuebean.BetweenValue;
import com.htax.turbo.innercompent.orm.ormcreater.valuebean.LikeValue;
import com.htax.turbo.innercompent.orm.pager.Pager;
import com.htax.turbo.innercompent.orm.pager.PagerData;
import com.htax.turbo.innercompent.sysmgr.auth.vo.AuthUserType;
import com.htax.turbo.innercompent.sysmgr.auth.vo.SsoUser;
import com.htax.turbo.innercompent.sysmgr.auth.vo.UserSessionVo;
import com.htax.turbo.innercompent.sysmgr.log.vo.LogBean;
import com.htax.turbo.innercompent.sysmgr.log.vo.LogBean.LOG_TYPE;
import com.htax.turbo.innercompent.sysmgr.pub.service.LayUiSimpleCrudDaoService;
import com.htax.turbo.innercompent.sysmgr.pub.vo.LayuiPager;
import com.htax.turbo.innercompent.sysmgr.util.RequestAuthTool;
import com.htax.turbo.innercompent.sysmgr.util.SpringUtil;
/**
 * 日志管理
 * @author qiao
 * @2015-7-6 @上午11:59:46
 */
@Service
public class LogMgrService extends LayUiSimpleCrudDaoService{

	private static Logger LOG = Logger.getLogger(LogMgrService.class);
	
	private static final String LOG_POOL_TYPE = "LOG_THREAD_POOL";
	
	private static String LOG_TABLE_PARA_KEY = "component.sysmgr.to_db_table";
	
	private static String LOG_TABLE_PARA_VALUR = null;
	
	static {
		ThreadPool.createPool(LOG_POOL_TYPE, 2, 10);
	}
	
	public static String getLogTaleName() {
		if(LOG_TABLE_PARA_VALUR == null) {
			LOG_TABLE_PARA_VALUR = ParaGetTool.getKernelPara(LOG_TABLE_PARA_KEY);
		}
		if(LOG_TABLE_PARA_VALUR==null) {
			throw new RuntimeException("无日志表信息,请检查kernel.properties的"+LOG_TABLE_PARA_KEY+"参数是否配置！");
		}
		return LOG_TABLE_PARA_VALUR;
	}
	
	/**
	 * 获取日志列表
	 * @return
	 * @throws ServiceException 
	 */
	public PagerData<LogBean> getLogList(HttpSession httpSession,LogBean logBean,LayuiPager pager) throws ServiceException{
		//日志类型判断
		//String[] canses = getCanSeeLogTypes(httpSession,logBean.getLogType());
		WhereCondition condition = new WhereCondition();
		condition
		.where1Eq1()
		.andEq("id", logBean.getId())
		.andLike("module", LikeValue.roundLike(logBean.getModule()))
		.andLike("action", LikeValue.roundLike(logBean.getAction()))
		.andLike("user_type", logBean.getUserType())
		.andLike("user_id", LikeValue.roundLike(logBean.getUserId()))
		.andLike("user_name", LikeValue.roundLike(logBean.getUserName()))
		.andLike("request_ip", LikeValue.roundLike(logBean.getRequestIp()))
		.andEq("log_level", logBean.getLogLevel())
		.andEq("result", logBean.getResult())
		//.andIn("log_type", canses)
		.andLike("msg", LikeValue.roundLike(logBean.getMsg()))
		.andBetween("log_time", new BetweenValue(logBean.getBeginDate(), logBean.getEndDate()))
		.orderBy(" log_time desc ");
		try {
			return DaoService.getDao().getPagerModuleList("查询日志列表", LogBean.class, condition, pager);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	/**
	 * 批量删除日志
	 * @param args
	 * @throws ServiceException
	 */
	public void batchDeleteLogs(List<Object[]> args,boolean allAppenderSameHour)throws ServiceException{
		String sql = "delete from "+getLogTaleName()+" where module=? and log_time<?";
		if(allAppenderSameHour){
			sql = "delete from "+getLogTaleName()+" where  log_time<?";
		}
		try {
			DaoService.getDao().batchDelete("批量删除日志", sql, args);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	//-------------------api
	private static void saveLog(final LogBean logBean){
		try {
			//多线程操作，防止日志记录影响业务性能
			
			ThreadPool.execute(LOG_POOL_TYPE,()->{
				List<LogBean> logBeans  = new ArrayList<LogBean>();
				logBeans.add(logBean);
				saveLogBean(logBeans);
			});
		} catch (ServiceException e) {
			//ignore
			LOG.error("多线程保存日志出错",e);
		}
	}
	
	//日志操作入库
	public static void dbLog(String module,LOG_TYPE type,String action,String result,String msg){
		final LogBean logBean = new LogBean();
		logBean.setId(UUIDGenerator.getUUID());
		setRequestInfo(logBean);
		logBean.setModule(module);
		logBean.setAction(action);
		logBean.setResult(result);
		logBean.setLogLevel("INFO");
		logBean.setCodeTrace(getPathInfo());
		logBean.setMsg(msg);
		logBean.setLogTime(new Date());
		logBean.setLogType(type.getCode());
		saveLog(logBean);
	}
	//入库
	private static void saveLogBean(List<LogBean> logBeans) {
		try {
			DaoService.getDao().batchInsertModule("日志入库", logBeans);
		} catch (DaoException e) {
			LOG.error("日志入库失败",e);
		}
	}
	//设置request先关请求
	private static void setRequestInfo(final LogBean logBean){
		HttpSession httpSession = SpringUtil.getSession();
		UserSessionVo userSessionVo = null;
		if(httpSession!=null){
		  userSessionVo = RequestAuthTool.getUserSessionVo();
		}
		String authUserTypeStr = "["+AuthUserType.SYSTEM_BACK_OFFICER.getCode()+"]";
		String ip = null;
		String userId = null;
		String userName = null;
		if(userSessionVo!=null){
			SsoUser loginUser = userSessionVo.getLoginUser();
			if(loginUser!=null){
				ip = loginUser.getIp();
				userName = loginUser.getUserName();
				userId = loginUser.getId();
			}
		}else{
			authUserTypeStr = "["+AuthUserType.SYSTEM_BACK_OFFICER.getCode()+"]";
			userName = userId = "系统后台";
		}
		HttpServletRequest httpServletRequest = SpringUtil.getRequest();
		if(httpServletRequest!=null){
			ip =  NetUtil.getRemoteIp(SpringUtil.getRequest());//获取真实ip，如通过集群的ip
		}
		logBean.setUserType(authUserTypeStr);
		logBean.setRequestIp(ip);
		logBean.setUserId(userId);
		logBean.setUserName(userName);
	}
	private static String getPathInfo(){  
		StringBuffer sb = new StringBuffer(); 
		
	    StackTraceElement[] stacks = Thread.currentThread().getStackTrace();   
	    if (null == stacks || stacks.length < 4){
	    	return sb.toString();
	    }
	    StackTraceElement cus = stacks[3];
	    sb.append(cus.getClassName()+"."+cus.getMethodName()+"("+cus.getFileName()+":"+cus.getLineNumber()+")" ); 
	    return sb.toString();
    }
}
