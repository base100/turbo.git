package com.htax.turbo.innercompent.sysmgr.pub.controller;

import java.util.HashMap;
import java.util.Map;

import com.htax.turbo.innercompent.base.controller.BaseController;
import com.htax.turbo.innercompent.orm.pager.PagerData;
/**
 * layui的controller封装
 * @author joe
 *
 */
public class LayuiBaseController extends BaseController{

	private static String LAYUI_TOTLAL_KEY = "count";
	/**
	 * 
	 * 返回json类型的数据,带有提示信息和返回数据
	 * @param errorMsgForDubug
	 * @param errorMsgForUser
	 * @return
	 */
	protected Map<String, Object> layuiPagerJsonSuccessDataResult(String successMsgForUser, PagerData<?> pagerData) {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put(AJAX_STATUS_KEY, AJAX_SUCCESS);
		resultMap.put(AJAX_MSG_KEY, successMsgForUser);
		if(pagerData!=null) {
			resultMap.put(AJAX_DATA_KEY, pagerData.getRows());
			resultMap.put(LAYUI_TOTLAL_KEY, pagerData.getTotal());
		}
		return resultMap;
	}
	
}
