package com.htax.turbo.innercompent.sysmgr.auth.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.htax.turbo.innercompent.base.codetable.CommonStaticCode.SUCCESSFAIL;
import com.htax.turbo.innercompent.base.controller.BaseController;
import com.htax.turbo.innercompent.base.para.CorePara.StaticPara;
import com.htax.turbo.innercompent.sysmgr.auth.annotation.WsdNoNeedLogin;
import com.htax.turbo.innercompent.sysmgr.auth.service.AuthService;
import com.htax.turbo.innercompent.sysmgr.auth.vo.SsoUser;
import com.htax.turbo.innercompent.sysmgr.auth.vo.UserSessionVo;
import com.htax.turbo.innercompent.sysmgr.log.service.LogApi;
import com.htax.turbo.innercompent.sysmgr.util.RequestAuthTool;

/**
 * 包含非中央节点用户登录，注销，注册等功能，
 * 
 * @see AuthInterceptor
 * @日期：2012-12-27下午4:54:28
 * @作者：乔兵
 * @版权所有：HTAX
 * @版本：1.0
 */
@Controller
@RequestMapping("_account/")
public class LoginController extends BaseController {

	public static final String LOG_PAGE = "pub/sysmgr/account/login";
	@RequestMapping("login")
	@WsdNoNeedLogin
	public String  logPage() {
		return LOG_PAGE;
	}
	
	@Autowired
	private AuthService authService;
	/**
	 * 登录验证 针对没有单点登录的节点
	 * 
	 * @param model
	 * @param httpSession
	 * @param userCode
	 * @param password
	 * @return
	 */
	@RequestMapping("login_submit")
	@WsdNoNeedLogin
	@ResponseBody
	public Map<String, Object> login(Model model, HttpSession httpSession, String userName, String password) {
		if (RequestAuthTool.getUserSessionVo(httpSession) != null) {
			return jsonSuccessResult("登录成功");
		}
		if (StringUtils.hasText(userName) && StringUtils.hasText(password)) {
			try {
				SsoUser superAdmin = authService.getSuperAdmin();
				if (!superAdmin.getUserName().equalsIgnoreCase(userName)) {
					return jsonFailResult("登录失败，用户名错误！");
				}
				if (!superAdmin.getPwd().equalsIgnoreCase(password)) {
					return jsonFailResult("登录失败，密码错误！");
				}
				UserSessionVo sessionVo = new UserSessionVo();
				sessionVo.setLoginSuccess(true);
				sessionVo.setLoginUser(superAdmin);
				httpSession.setAttribute(StaticPara.USER_SESSION_KEY, sessionVo);
				LogApi.sysLog("用户登录", "login", SUCCESSFAIL.SUCCESS.getStatus(), "登录成功");
				return jsonSuccessResult("登录成功");
			} catch (Exception e) {
				model.addAttribute("error", "系统异常,请联系管理员！");
				LogApi.sysLog("用户登录", "login", SUCCESSFAIL.FAIL.getStatus(), e.getMessage());
				return jsonFailResult("登录失败");
			}
		} else {
			return jsonFailResult("登录失败，用户名密码不能为空！");
		}
	}

	@WsdNoNeedLogin
	@RequestMapping("logout")
	public String logout(HttpSession httpSession) {
		LogApi.safeLog("登陆管理", "用户注销", SUCCESSFAIL.SUCCESS.getStatus(),"用户注销成功");
		httpSession.invalidate();
		return "redirect:/_account/login";
	}
	/**
	 * 密码修改页面
	 * @param model
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/password_change")
	public String passwordChange(Model model, HttpSession httpSession) {
		SsoUser user=null;
		user = RequestAuthTool.getCurrentUserInfo(httpSession);
		model.addAttribute("user", user);
		return "account/password_change";
	}
/*
	*//**
	 * 执行密码修改操作
	 * @param model
	 * @param httpSession
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 *//*
	@RequestMapping(value = "/password_do_change")
	@ResponseBody
	public Map<String, Object> changePassword(Model model, HttpSession httpSession, String oldPassword,
			String newPassword) {
		try {
			if (StringUtils.hasText(oldPassword)) {
				UserInfoVo user = userMgrService.getUserById(getCurrentUserInfo(httpSession).getId());
				if (user != null) {
					if (!user.getPassWd().equals(oldPassword)) {
						return jsonFailResult("用户密码错误");
					} else {
						userMgrService.changePwdByUserId(user.getId(), newPassword);
						return jsonSuccessResult("用户密码成功");
					}
				} else {
					return jsonFailResult("用户名不存在");
				}
			} else {
				return jsonFailResult("用户名或密码为空");
			}
		} catch (Exception e1) {
			model.addAttribute("error", "系统异常,请联系管理员！");
			return jsonFailResult("系统异常,请联系管理员！");
		}
	}*/
}
