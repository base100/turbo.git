package com.htax.turbo.innercompent.sysmgr.log.vo;

import java.util.Date;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;
import com.htax.turbo.innercompent.sysmgr.log.service.LogMgrService;

/**
 * 通用的日志对象
 * @author qiao
 * @2015-6-5 @下午3:52:15
 */
@WsdTable
public class LogBean extends ModuleFeatureBean{

	public LogBean() {
		super();
	}
	public LogBean(String userType, String userId, String userName,
			String requestIp,String logType, String module, String action, String result,
			String logLevel, String codeTrace, String msg, Date logTime
			) {
		super();
		this.userType = userType;
		this.userId = userId;
		this.userName = userName;
		this.requestIp = requestIp;
		this.logType = logType;
		this.module = module;
		this.action = action;
		this.result = result;
		this.logLevel = logLevel;
		this.msg = msg;
		this.logTime = logTime;
		this.codeTrace = codeTrace;
	}
	@Override
	public String tableName() {
		return LogMgrService.getLogTaleName();
	}
	
	//日志类型
	public static enum LOG_TYPE{
		SYSLOG("02","系统管理日志"),
		SAFELOG("03","安全日志"),
		RUNLOG("04","运维日志"),
		BUSILOG("01","业务日志");
		private String code;
		private String name;
		private LOG_TYPE(String code, String name) {
			this.code = code;
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public String getName() {
			return name;
		}
	}
	@WsdColumn(isId=true)
	private String id;
	//操作用户类型 例如：审计员、安全员、管理员、业务员、系统@see AuthUserType
	@WsdColumn(name="user_type")
	private String userType;
	//用户id
	@WsdColumn(name="user_id")
	private String userId;
	//用户名称
	@WsdColumn(name="user_name")
	private String userName;
	//日志类型
	@WsdColumn(name="log_type")
	private String logType;
	//日志产生ip
	@WsdColumn(name="request_ip")
	private String requestIp;
	//模块。 
	private String module;
	//操作。 
	private String action;
	//操作结果。 
	private String result;
	@WsdColumn(name="log_level")
	private String logLevel;
	//日志发生上下文
	private String codeTrace;
	//日志描述
	private String msg;
	//日志时间
	@WsdColumn(name="log_time")
	private Date logTime; 
	
	@WsdNotDbColumn
	private Date beginDate;
	@WsdNotDbColumn
	private Date endDate;
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRequestIp() {
		return requestIp;
	}
	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getLogLevel() {
		return logLevel;
	}
	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Date getLogTime() {
		return logTime;
	}
	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getLogType() {
		return logType;
	}
	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCodeTrace() {
		return codeTrace;
	}
	public void setCodeTrace(String codeTrace) {
		this.codeTrace = codeTrace;
	}
	@Override
	public String toString() {
		return "LogBean [id=" + id + ", userType=" + userType + ", userId=" + userId + ", userName=" + userName
				+ ", logType=" + logType + ", requestIp=" + requestIp + ", module=" + module + ", action=" + action
				+ ", result=" + result + ", logLevel=" + logLevel + ", codeTrace=" + codeTrace + ", msg=" + msg
				+ ", logTime=" + logTime + ",  beginDate=" + beginDate + ", endDate="
				+ endDate + "]";
	}
	
}
