package com.htax.turbo.innercompent.sysmgr.auth.vo;

import java.io.Serializable;
import java.util.List;

public class UserSessionVo implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 是否登录成功
	 */
	private boolean loginSuccess;
	/**
	 * 登录用户的中文名
	 */
	private String userName;
	/**
	 * 初始化路径
	 */
	private List<String> initResIds;
	
	/**
	 * 登录用户信息
	 */
	private SsoUser loginUser;

	public boolean isLoginSuccess() {
		return loginSuccess;
	}

	public void setLoginSuccess(boolean loginSuccess) {
		this.loginSuccess = loginSuccess;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<String> getInitResIds() {
		return initResIds;
	}

	public void setInitResIds(List<String> initResIds) {
		this.initResIds = initResIds;
	}

	public SsoUser getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(SsoUser loginUser) {
		this.loginUser = loginUser;
	}

}
