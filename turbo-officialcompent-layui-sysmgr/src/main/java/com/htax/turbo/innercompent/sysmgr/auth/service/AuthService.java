package com.htax.turbo.innercompent.sysmgr.auth.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.htax.core.tools.util.NetUtil;
import com.htax.core.tools.util.StrUtil;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.base.para.ParaGetTool;
import com.htax.turbo.innercompent.sysmgr.auth.vo.SsoUser;
import com.htax.turbo.innercompent.sysmgr.util.SpringUtil;
@Service
public class AuthService {
	
	public static final String  SUPERADMIN_STR_PARA_KEY = "component.sysmgr.superadmin";
	private SsoUser superAdmin = null;
	public SsoUser getSuperAdmin() throws ServiceException {
		if(superAdmin==null) {
			String superAdminStr = ParaGetTool.getKernelPara(SUPERADMIN_STR_PARA_KEY);
			if(!StrUtil.isNotBLank(superAdminStr)) {
				throw new ServiceException("无超级管理员配置信息,请检查kernel.properties的"+SUPERADMIN_STR_PARA_KEY+"参数是否配置！");
			}else {
				String[] user_pwd = superAdminStr.split("/");
				superAdmin = new SsoUser();
				superAdmin.setUserName(user_pwd[0]);
				superAdmin.setPwd(user_pwd[1]);
				superAdmin.setSuperAdmin(true);
				superAdmin.setLoginTime(new Date());
				String ip = NetUtil.getRemoteIp(SpringUtil.getRequest());//获取真实ip，如通过集群的ip
				superAdmin.setIp(ip);
			}
		}
		return superAdmin;
	}
}
