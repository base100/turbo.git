package com.htax.turbo.innercompent.sysmgr.log.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.orm.pager.PagerData;
import com.htax.turbo.innercompent.sysmgr.log.service.LogMgrService;
import com.htax.turbo.innercompent.sysmgr.log.vo.LogBean;
import com.htax.turbo.innercompent.sysmgr.pub.controller.LayuiBaseController;
import com.htax.turbo.innercompent.sysmgr.pub.vo.LayuiPager;

/**
 * 通用日志管理
 * @author qiao
 * @2015-7-6 @下午3:01:32
 */
@Controller
@RequestMapping("_logmgr")
public class LogMgrController extends LayuiBaseController{

	@Autowired
	private LogMgrService logMgrService;
	
	@RequestMapping("/index")
	public String list(){
		return "pub/sysmgr/log/log-list";
	}
	@RequestMapping("/list")
	@ResponseBody
	public Map<String, Object> list(HttpSession httpSession,LogBean logBean,LayuiPager pager){
		try {
			PagerData<LogBean> rs = logMgrService.getLogList(httpSession,logBean, pager);
			return  layuiPagerJsonSuccessDataResult("获取成功", rs);
		} catch (ServiceException e) {
			return jsonFailResult("操作失败，请联系管理员");
		}
	}
}
