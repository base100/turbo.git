package com.htax.turbo.innercompent.sysmgr.auth.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;


/**
 * 点单登录用户
 * @author qiao
 * @2015-8-11 @下午3:20:15
 */
public class SsoUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String userName;
	private String pwd;
	private String userCode;
	private String depCode;
	private String orgCode;
	private boolean superAdmin;
	private String adminOrgCode;
	//private List<RoleInfo> roles;
	private Set<AuthUserType> authUserTypes;
	private boolean fromSso;
	private Date loginTime;
	private String ip;
	private Object ssoRawUserInfo;
	
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public boolean isSuperAdmin() {
		return superAdmin;
	}
	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}
	
	public Set<AuthUserType> getAuthUserTypes() {
		return authUserTypes;
	}
	public void setAuthUserTypes(Set<AuthUserType> authUserTypes) {
		this.authUserTypes = authUserTypes;
	}
	public boolean isFromSso() {
		return fromSso;
	}
	public void setFromSso(boolean fromSso) {
		this.fromSso = fromSso;
	}
	public Date getLoginTime() {
		return loginTime;
	}
	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Object getSsoRawUserInfo() {
		return ssoRawUserInfo;
	}
	public void setSsoRawUserInfo(Object ssoRawUserInfo) {
		this.ssoRawUserInfo = ssoRawUserInfo;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getDepCode() {
		return depCode;
	}
	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getAdminOrgCode() {
		return adminOrgCode;
	}
	public void setAdminOrgCode(String adminOrgCode) {
		this.adminOrgCode = adminOrgCode;
	}
}
