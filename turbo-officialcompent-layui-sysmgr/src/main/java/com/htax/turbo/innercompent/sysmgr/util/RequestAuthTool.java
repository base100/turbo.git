package com.htax.turbo.innercompent.sysmgr.util;

import java.util.Set;

import javax.servlet.http.HttpSession;

import com.htax.turbo.innercompent.base.para.CorePara.StaticPara;
import com.htax.turbo.innercompent.sysmgr.auth.vo.AuthUserType;
import com.htax.turbo.innercompent.sysmgr.auth.vo.SsoUser;
import com.htax.turbo.innercompent.sysmgr.auth.vo.UserSessionVo;
/**
 * 获取request上下文权限信息
 * @author qiao
 * 2020年4月27日下午3:21:46
 */
public class RequestAuthTool {

	/**
	 * 获取当前登录信息
	 * @param httpSession
	 * @return
	 */
	public static UserSessionVo  getUserSessionVo(HttpSession httpSession) {
		if (null == httpSession){
			return null;
		}
		UserSessionVo sessionVo = (UserSessionVo)httpSession.getAttribute(StaticPara.USER_SESSION_KEY);
		return sessionVo;
	}
	/**
	 * 获取当前登录信息
	 * @param httpSession
	 * @return
	 */
	public static UserSessionVo  getUserSessionVo() {
		HttpSession httpSession = SpringUtil.getSession();
		if (null == httpSession){
			return null;
		}
		UserSessionVo sessionVo = (UserSessionVo)httpSession.getAttribute(StaticPara.USER_SESSION_KEY);
		return sessionVo;
	}
	/**
	 * 从当前session中获取在线用户信息
	 * 调用此方法请确保用户已经登录且在有效的session生命周期内
	 * @param request
	 * @param httpSession
	 * @return
	 * @throws Exception
	 */
	public static SsoUser getCurrentUserInfo(HttpSession httpSession) {
		UserSessionVo sessionVo = getUserSessionVo(httpSession);
		if(sessionVo!=null){
			return sessionVo.getLoginUser();
		}
		return null;
	}
	public static boolean isSuperAdmin(HttpSession httpSession){
		SsoUser ssoUser = getCurrentUserInfo(httpSession);
		if(ssoUser!=null){
			return ssoUser.isSuperAdmin();
		}
		return false;
	}
	public static String getUid(HttpSession httpSession){
		SsoUser loginUser = getCurrentUserInfo(httpSession);
		if(loginUser!=null){
			return loginUser.getId();
		}
		return null;
	}
	public static String getUid(){
		HttpSession httpSession = SpringUtil.getSession();
		if(httpSession!=null){
			SsoUser loginUser = getCurrentUserInfo(httpSession);
			if(loginUser!=null){
				return loginUser.getId();
			}
		}
		return null;
	}
	/**
	 * 获取当前登陆用户的角色类型列表
	 * @param httpSession
	 * @return
	 */
	public static Set<AuthUserType> getLoginUserType(HttpSession httpSession){
		/*SsoUser loginUser = getCurrentUserInfo(httpSession);
		if(loginUser!=null){
			Set<AuthUserType> authUserTypes = loginUser.getAuthUserTypes();
			if(!CollectionUtil.hasElement(authUserTypes)){
				authUserTypes = getLoginUserTypeFromUserRole(loginUser.getRoles());
				loginUser.setAuthUserTypes(authUserTypes);
			}
			return loginUser.getAuthUserTypes();
		}*/
		return null;
	}
	/**
	 * 登陆用户是否包含安全员角色
	 * @param httpSession
	 * @return
	 */
	public static boolean hasSafeRole(HttpSession httpSession){
		Set<AuthUserType> authUserTypes = getLoginUserType(httpSession);
		if(authUserTypes!=null){
			return authUserTypes.contains(AuthUserType.SAFETY_OFFICER);
		}
		return false;
	}
	/**
	 * 登陆用户是否包含审计角色
	 * @param httpSession
	 * @return
	 */
	public static boolean hasAuditRole(HttpSession httpSession){
		Set<AuthUserType> authUserTypes = getLoginUserType(httpSession);
		if(authUserTypes!=null){
			return authUserTypes.contains(AuthUserType.AUDIT_OFFICER);
		}
		return false;
	}
	/**
	 * 登陆用户是否包含系统管理员角色
	 * @param httpSession
	 * @return
	 */
	public static boolean hasSysMgrRole(HttpSession httpSession){
		Set<AuthUserType> authUserTypes = getLoginUserType(httpSession);
		if(authUserTypes!=null){
			return authUserTypes.contains(AuthUserType.SYSTEM_MANAGER_OFFICER);
		}
		return false;
	}
	/**
	 * 登陆用户是否包含业务员角色
	 * @param httpSession
	 * @return
	 */
	public static boolean hasBusiRole(HttpSession httpSession){
		Set<AuthUserType> authUserTypes = getLoginUserType(httpSession);
		if(authUserTypes!=null){
			return authUserTypes.contains(AuthUserType.BUSINESS_OFFICER);
		}
		return false;
	}
}
