package com.htax.turbo.innercompent.sysmgr.auth.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import com.htax.turbo.innercompent.base.interceptor.InterceptorArgs;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean;
import com.htax.turbo.innercompent.base.para.CorePara.StaticPara;
import com.htax.turbo.innercompent.sysmgr.auth.annotation.WsdNoNeedAuth;
import com.htax.turbo.innercompent.sysmgr.auth.annotation.WsdNoNeedLogin;
import com.htax.turbo.innercompent.sysmgr.auth.vo.UserSessionVo;
import com.htax.turbo.innercompent.sysmgr.util.RequestAuthTool;


/**
 * 系统权限的控制
 * 
 * @日期：2012-12-14下午11:21:12
 * @作者：乔兵
 * @版权所有：HTAX
 * @版本：1.0
 */
@Component
public class SimpleAuthInterceptor extends InterceptorBean {

	private static Logger logger = Logger.getLogger(SimpleAuthInterceptor.class);

	@Override
	public SIGNAL beforeControllerMethodExecute(InterceptorArgs interceptorArgs) {
		HandlerMethod handlerMethod = interceptorArgs.getHandlerMethod();
		if(interceptorArgs.isStatic())return SIGNAL.CONTINUE;
		HttpServletRequest request = interceptorArgs.getRequest();
		if (request == null || request.getMethod() == null) {
			return SIGNAL.CONTINUE;
		}
		HttpServletResponse response = interceptorArgs.getResponse();
		//无需登录权限认证
		if (null != handlerMethod.getMethodAnnotation(WsdNoNeedLogin.class)) {
			logger.info("佚名资源：" + request.getRequestURI());
			return SIGNAL.CONTINUE;
		} else {
			Object user = getSessionVo(interceptorArgs);
			if (user == null) {
				try {
					response.sendError(StaticPara._NO_SESSION_ERROR_CODE, "请登录！");
				} catch (IOException e) {
				}
				return SIGNAL.STOP;
			} else {
				if (null != handlerMethod.getMethodAnnotation(WsdNoNeedAuth.class)) {
					logger.info("公共资源：" + request.getRequestURI());
					return SIGNAL.CONTINUE;
				}
			}
			return SIGNAL.CONTINUE;
		}
	}

	@Override
	public SIGNAL interceptorExecuteErrorSignal(Exception beforeControllerMethodExecuteException) {
		return SIGNAL.STOP;
	}

	private Object getSessionVo(InterceptorArgs interceptorArgs) {
		HttpServletRequest request = interceptorArgs.getRequest();
		if (request != null) {
			HttpSession httpSession = null;
			try {
				httpSession = request.getSession(true);
			} catch (Exception e) {
				logger.error("获取Session发生异常！" + request.getRequestURI());
				return null;
			}
			UserSessionVo userSessionVo = RequestAuthTool.getUserSessionVo(httpSession);
			return userSessionVo;
		}
		return null;
	}
}
