package com.htax.turbo.innercompent.sysmgr.pub.service;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.htax.turbo.innercompent.base.CommonSimpleCrudDaoService;
import com.htax.turbo.innercompent.base.DaoService;
import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.orm.ormcreater.WhereCondition;
import com.htax.turbo.innercompent.orm.pager.PagerData;
import com.htax.turbo.innercompent.sysmgr.pub.vo.LayuiPager;


/**
 * 具有layui特点的service
 * @author qiao
 * 2020年4月16日上午10:05:58
 */
@Service
public class LayUiSimpleCrudDaoService extends CommonSimpleCrudDaoService{
	
	private static Logger LOG = Logger.getLogger(CommonSimpleCrudDaoService.class);
	
	/**
	 * 简单分页查询封装，无查询条件
	 * @param pg
	 * @return
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> PagerData<T> layUIpagerList(Class<T> clazz,LayuiPager pg) throws ServiceException {
		String action = "分页查询"+clazz.getName();
		try {
			return  DaoService.getDao().getPagerModuleList("查询数据源信息", clazz, null, pg);
		} catch (DaoException e) {
			LOG.error(action+" 操作失败！",e);
			throw new ServiceException(e);
		}
	}

	/**
	 * 简单分页查询封装，无查询条件
	 * @param pg
	 * @return
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> PagerData<T> layUIpagerListOrderByTsDesc(Class<T> clazz,LayuiPager pg) throws ServiceException {
		String action = "分页查询"+clazz.getName();
		try {
			WhereCondition condition = new WhereCondition();
			condition.orderBy(" ts desc");
			return  DaoService.getDao().getPagerModuleList("查询数据源信息", clazz, condition, pg);
		} catch (DaoException e) {
			LOG.error(action+" 操作失败！",e);
			throw new ServiceException(e);
		}
	}
}
