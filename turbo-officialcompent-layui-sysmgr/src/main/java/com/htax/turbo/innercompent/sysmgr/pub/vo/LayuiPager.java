package com.htax.turbo.innercompent.sysmgr.pub.vo;

import com.htax.turbo.innercompent.orm.pager.Pager;
/**
 * layui 分页相关
 * @author htax
 *
 */
public class LayuiPager extends Pager{

	private Integer limit;
	private Integer page;
	
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer size) {
		this.limit = size;
		super.getPg().setPagesize(size);
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer currentPage) {
		this.page = currentPage;
		super.getPg().setCurrentPage(currentPage);
	}
}
