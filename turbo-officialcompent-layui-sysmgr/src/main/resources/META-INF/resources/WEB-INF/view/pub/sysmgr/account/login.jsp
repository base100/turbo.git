<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="../../base_withlayui.jsp"%>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>首页</title>
<style type="text/css">
 .login {
  	background-image: url('${ctx}/static/img/pub/login-bg.png');
 }
 .login-panel{
    display:inline-block;
    position:absolute;
  	width:600px;
  	height: 50px;
  	top:40%;
  	left:35%;
  	margin:0 auto;
   	background-color: #444444;
   	text-align: center;
 }
 .login-content{
 	display:inline-block;
   	width:200px;
   	height: 40px;
   	margin:5px;
   	background-color: #332929;
 }
  .login-btn {
  	display:inline-block;
   	width:160px;
   	height: 40px;
   	margin:5px;
 }
 .login-btn button{
   	width:160px;
   	height: 40px;
 }
   .login-input{
 	border: 1px;
 	height: 100%;
 	width: 100%;
 	background-color: #332929;
 	margin-left: 5px;
 	color: #e4e4e4
 }
  .login-log{
    display:inline-block;
    position:absolute;
  	top:32%;
  	left:35%;
  	width:600px;
  	height: 60px;
 }
  .login-log span{
  	display:inline-block;
    font-size: 28px;
    color: #fff;
    margin-left:10px;
    vertical-align: middle;
    text-shadow: #539ae3 2px 2px 3px ;
 }
 .mark{
 	background-color: rgba(0,0,0,0.4);
 	position:absolute;
 	width: 100%;
 	height: 100%;
 }
</style>
<script>
	/*初始化加载layui*/
	require([ "jquery", "layui",'app/common-util' ], function($, layui,util) {
		var form = layui.form;
		form.on('submit(login_submit)',function(data){
			util.ajax.busiJsonPost({
				url : _ctx+"/_account/login_submit",
				showSuccessMsg:true,
				data:$("#login-form").serialize(),
				success:function(){
					self.location=_ctx;
				}
			});
		});
		return false;
	});
</script>
</head>
<body class="layui-layout-body login">
<div class="mark">
	<div class="login-log">
	    <img alt="" src="${ctx}/static/img/pub/log.png"><span>元数据管理</span>
	 </div>
	 <form class="layui-form" id="login-form">
		 <div class="login-panel">
		    <div class="login-content">
		     <input class="login-input" placeholder="请输入登录名"  name="userName" lay-verify="required"/>
		 	</div>
		 	<div class="login-content">
		      <input type="password" class="login-input" placeholder="请输入密码" name="password" lay-verify="required"/>
		 	</div>
		 	<div class="login-btn" >
		      <button lay-submit="" lay-filter="login_submit" class="layui-btn layui-btn-normal tijiao" >登录</button>
		 	</div>
		 </div>
	 </form>
 </div>
</body>
</html>