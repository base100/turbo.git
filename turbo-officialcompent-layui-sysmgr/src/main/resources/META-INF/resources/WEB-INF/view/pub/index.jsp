<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="base_withlayui.jsp"%>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>首页</title>
<script>
	/*初始化加载layui*/
	require([ "jquery", "layui" ], function($, layui) {
		$("[_totpage]").on("click", function() {
			var _totpage = $(this).attr("_totpage");
			with(window.G){
				setNavigate(getNavigate($(this)));
			}
			$("#iframe_content")[0].contentWindow.location.href = _totpage;
		});
		
		//公共方法，可以自行调用设置导航值
		window.G = {
			setNavigate : function(content,append,hidden){
				//是否隐藏
				if(hidden){
					$("#navigate").parent().hide();
				}else{
					if(append){
						$("#navigate").val($("#navigate").val()+" / "+content);
					}else{
						$("#navigate").html(content);
					}
				}
			} ,
		    getNavigate: function (clickObj){
				var html = clickObj.html();
				var navigateContent = "";
				//没有子菜单
				var clickObjText = clickObj.text();
				if(html.indexOf("site")!=-1){
					navigateContent = clickObjText;
				}else{
					clickObj.parents("li").find("a").not(".leaf").each(function(){
						var text = $(this).text();
						var html = $(this).html();
						if($(this).next().prop("outerHTML").indexOf(clickObjText)!=-1){
							navigateContent += " / "+ text 
						}
					})
					navigateContent += " / "+ clickObjText;
				}
				return navigateContent;
			}
		}
		
	});
</script>
</head>
<body class="layui-layout-body">
	<div >
		<div class="layui-layout layui-layout-admin">
			<!-- 顶部菜单 -->
			<%@include file="top.jsp"%>
			<!-- 导航 -->
			<%@include file="navigate.jsp"%>
			<!-- 左侧菜单 -->
			<%@include file="left.jsp"%>
			<!-- 内容主体区域 -->
			<div class="layui-body" style="margin-top: 40px">
				<div class="layadmin-tabsbody-item layui-show" >
					<iframe  class="layadmin-iframe" id="iframe_content"></iframe>
				</div>
			</div>
		</div>
	</div>
</body>
</html>