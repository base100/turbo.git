/**
 * 对layui增删该查的封装
 * v1.0.1
 * author:joe
 */
define([ "jquery", "layui",'app/common-util'], function($, layui,util) {
	var tree = layui.tree;
	var initTree = function(options){
		var {elem,url,root_title,onclickfn,showName,showCheckbox,id,spread_all} = options;
		var root_id = options.root_id||'0';
		var root = [{id:'0',pid:"-1",title:root_title,children:[],spread:true}];
		util.ajax.jsonPostAndBusiStatusCallBack({
			url : url,
			success:function(data){
				if(showName){
					//layui好像只能用title作为显示节点，需要动态添加一个
					data.forEach(item => item['title']=item[showName]);
				}
				var data = util.ui.pid2childrenAndSpreadAll(data,spread_all);
				root[0].children = data;
				tree.render({
					elem:elem,
					data:root,
					id:id||elem,
					showCheckbox:showCheckbox,
					click:function(obj){
						var data = obj.data;
						setChecked(data.id);
						if(onclickfn){
							onclickfn(data);
						}
					}
				});
			}
		});
	}
	function setChecked(id){
		$('.layui-tree-txt').removeClass("ext-layui-tree");
		$("[data-id="+id+"]").find('.layui-tree-txt').first().addClass("ext-layui-tree");
	}
	
	return {initTree:initTree} 
});