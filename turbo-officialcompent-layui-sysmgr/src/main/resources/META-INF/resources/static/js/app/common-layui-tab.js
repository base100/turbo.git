/**
 * 对layui增删该查的封装
 * v1.0.1
 * author:joe
 */
define([ "jquery", "layui",'app/common-util','ztree'], function($, layui,util,ztree) {
	var tree = layui.tree;
	var initZTree = function(options){
		//获取数据的地址
		var url = options.url;
		var data = options.data;//没有此参数，就通过url去取。
		var dataEtlFn = options.dataEtlFn
		if(!data){
			//动态取值
			$.ajax({
				url : url,
				type : 'POST',
				dataType : 'json',
				success : function(res) {
					var data = dataEtlFn(res.data);
							//初始化ztree
					console.log(data)
					initZtree(data,options);
				},
				error : function(a,b) {
					layer.msg('初始化失败', { icon : 6 });
				}
			});
		}else{
			initZtree(data,options);
		}
	}
	//初始化ztree
	function initZtree(data,options){
		var inputElem = options.inputElem;//输入框,用于回显选择内容
		var checkType = options.checkType||"radio";//
		var onclickFn = options.onclickFn;//回调函数
		var hiddenInputId = options.hiddenInputId  //隐藏域,用于设置选中的id，如果有onclickFn，可以没有此项，自己去处理返回值
		var setting = {
				check: {
					enable: true,
					chkStyle: checkType,
					radioType: "all"
				},
				view: {
					dblClickExpand: false
				},
				data: {
					simpleData: {
						enable: true
					}
				},
				callback: {
					onClick: onClick,
					onCheck: onCheck
				}
			};
		//动态注入隐藏域
		var index = inputElem;//用input的name作为变量，
		var hiddenDom = '<div id="menuContent" class="menuContent" '+
							'style="display: none; position: absolute; left: 0px:top:0px; z-index: 9999">'+
								'<ul id="treeHidden" class="ztree" style="margin-top: 0; width: 180px; height: 300px;"></ul></div>';
		$("#"+inputElem).parent().after(hiddenDom);
		
		ztree.init($("#treeHidden"),setting,data);
		
		$("#"+inputElem).on("click",function(){
			showMenu();
		})
		function showMenu() {
			var inputobj = $("#"+inputElem);
			var inputobjOffset = $("#"+inputElem).offset();
			var left = inputobj.parent().prev().css("width");
			$("#menuContent").css({left:left, top:inputobj.outerHeight()+3+"px"}).slideDown("fast");
		} 
		function hideMenu() {
			$("#menuContent").fadeOut("fast");
		}
		function onClick(e, treeId, treeNode) {
			var zTree = $.fn.zTree.getZTreeObj("treeHidden");
			zTree.checkNode(treeNode, !treeNode.checked, null, true);
			return false;
		}

		function onCheck(e, treeId, treeNode) {
			var zTree = $.fn.zTree.getZTreeObj("treeHidden"),
			nodes = zTree.getCheckedNodes(true),
			name_data = "";
			id_data = "";
			for (var i=0, l=nodes.length; i<l; i++) {
				name_data += nodes[i].name + ",";
				id_data += nodes[i].id + ",";
			}
			if (name_data.length > 0 ) {
				name_data = name_data.substring(0, name_data.length-1);
				id_data = id_data.substring(0, id_data.length-1);
			}
			//有回调函数，自己处理返回值
			if(onclickFn){
				onclickFn(name_data,id_data);
			}else{
				//没有回调函数，给输入框和隐藏域设置值
				$("#"+inputElem).val(name_data);
				$("#"+hiddenInputId).val(id_data);
			}
			hideMenu();
		}
	}
	
	return {initZTree:initZTree} 
});