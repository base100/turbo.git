/**
 * 对layui增删该查的封装
 * v1.0.1
 * author:joe
 */
define([ "jquery", "layui" ,'app/common-util'], function($, layui,util) {
	var {table,layer,form,upload} = layui;
	var initTable = function(options){
		var {elem,url,toolbar,rowtoolbar,cols,ctrl,where,timeCol,table_id} = options;
		var t = table.render({
			elem: elem, url:url, toolbar:toolbar, method:'POST', even: true, cols: cols, id: table_id,
			page: {
				layout: ['prev', 'page', 'next', 'skip', 'limit', 'count'],
				limits: [10, 20, 30, 50, 100],
				curr: 1,
				groups: 10,
				first: '首页',
				last: '尾页',
			},
			parseData: function(res) {
				if(res.status==0){
					layer.msg(res.msg);
				}else{
					return {
						'code': !res.status,
						'count': res.count,
						'data' : res.data
					};
				}
			},
			where:where
		}); 

		table.on('toolbar('+elem.substring(1)+')',function(obj){
			var checked = table.checkStatus(obj.config.id);
			ctrl[obj.event].call(this,checked.data);
		});
		table.on('tool('+elem.substring(1)+')',function(obj){
			ctrl[obj.event].call(this,obj.data);
		});
		return t;
	}
	//crud 简化封装
	var tableAdd = function(options){
		var {title,btn,form_id,table_id,url,dataFn,btn1Success} = options;//
		var area = options.area||'auto';
		var layerOption = {
			title:title,form_id:form_id,btn:options.btn||['确认','关闭'],area:area,
			success:function(layero){
				addSubmitFeature(layero);
			},
			yes:options.btn1||function(layero){
				var data;
				if(typeof(dataFn)!='undefined'){//自定义了数据组织函数
					var data = dataFn();
					if(!data){//通过返回false，阻止提交，比如没有选中任何东西
						return;
					}
				}else{//如果没有自定义函数，使用表单序列化
					data = $("#"+form_id).serialize();
				}
				form.on('submit(*)',function(form){
					util.ajax.jsonPostAndShowMsgAndCloseLayerAndReLoadTable({
						url : url,
						data : data,
						layero:layero,
						table_id:table_id,
						success:btn1Success
					});
				});
			},
			//固定为关闭按钮
			btn2:options.btn2||function(layero){layer.close(layero);},
			//其他按钮，可选,预留3个
			btn3:options.btn3,btn4:options.btn4,btn4:options.btn5,
			content:$("#"+form_id)
		};
		layerOption = Object.assign(layerOption,layerCommonOption);
		layer.open(layerOption);
	}
	var tableDel = function(options,data){
		var {table_id,url} = options;
		layer.confirm("确定要删除么？",function(layero){
			util.ajax.jsonPostAndShowMsgAndCloseLayerAndReLoadTable({
				url : url,
				layero:layero,
				table_id:table_id
			});
		});	
	}
	var batchDel = function(options,data){
		var {table_id,url,para_key} = options;
		var size = data.length;
		if(size==0){
			layer.msg("请选择要删除的数据!");
		}else{
			var ids = data.map(d=>d.id);
			layer.confirm("确定要删除么这"+size+"条数据么？",function(layero){
				util.ajax.jsonPostAndShowMsgAndCloseLayerAndReLoadTable({
					url : url,
					layero:layero,
					table_id:table_id,
					data:para_key+"="+ids,
				});
			});
		}
	}
	/**
	 * 查看
	 */
	var tableInfo = function(options){
		var {title,btn,form_id,table_id,get_url,formDataSet} = options;
		var area = options.area||'auto';
		var layerOption ={
			title:title,area: area,form_id:form_id,btn:btn||['确定'],
			success:function(layero){
				util.ajax.jsonPost({
					url : get_url,
					success : function(rep) {
						var {status,data,msg} = rep;
						if(status==1){
							if(formDataSet){
								formDataSet(data);
							}else{
								util.ui.layUiFormInit(form_id,data);
							}
						}else{
							util.ui.lay.failMsg(msg);
						}
					}
				});
			},
			yes:options.btn1||function(layero){
				layer.close(layero);
			},
			content:$("#"+form_id)
		}
		layerOption = Object.assign(layerOption,layerCommonOption);
		layer.open(layerOption);
	}
	//crud 简化封装
	var tableEdit = function(options){
		var {title,btn,form_id,table_id,get_url,url,formDataSet} = options;
		var area = options.area||'auto';
		var layerOption ={
			title:title,area: area,form_id:form_id,btn:btn||['确定','关闭'],
			success:function(layero){
				addSubmitFeature(layero);
				util.ajax.jsonPost({
					url : get_url,
					success : function(rep) {
						var {status,data,msg} = rep;
						if(status==1){
							if(formDataSet){
								formDataSet(data);
							}else{
								util.ui.layUiFormInit(form_id,data);
							}
						}else{
							util.ui.lay.failMsg(msg);
						}
					}
				});
			},
			yes:options.btn1||function(layero){
				form.on('submit(*)',function(form){
					util.ajax.jsonPostAndShowMsgAndCloseLayerAndReLoadTable({
						url : url,
						layero:layero,
						table_id:table_id,
						data:$("#"+form_id).serialize(),
					});
				});
			},
			btn2:options.btn2||function(layero){
				layer.close(layero);
			},
			//其他按钮，可选,预留3个
			btn3:options.btn3,btn4:options.btn4,btn4:options.btn5,
			content:$("#"+form_id)
		}
		layerOption = Object.assign(layerOption,layerCommonOption);
		layer.open(layerOption);
	}
	
	var uploadwithLayui = {}
	uploadwithLayui.uploadSimple = function(option){
		var {/*上传触发按钮的id，需带#号*/uploadBtnId,/*预览图片的id，需带#号*/preimg_id,/*提示信息展示id，需带#号*/upload_msg_id,url,uploadBtnId} = option;
		upload.render({
			elem:uploadBtnId,
			url:url,
			multiple:true,
			field:'file',
			before:function(obj){
				obj.preview(function(index,file,result){
					if(preimg_id){
						$(preimg_id).attr('src',result);
					}
				})
			},
			done:function(res){
				if(res.code==0){
					$(upload_msg_id).html('<span style="color:#ff5722">上传失败</span>')
				}else{
					$(upload_msg_id).html('<span style="color:#00ff22">上传成功</span>')
				}
			}
		});
	}
	var layerCommonOption = {
			type:1,
			skin:'layui-layer-molv',
			shade:0,
			maxmin:true,
			anim:0,
	}
	//动态添加按钮提交绑定，需要表单提交的都绑定上lay-submit':'','lay-filter':'*'属性，好恶心...
	function addSubmitFeature(layero){
		layero.addClass('layui-form');
		layero.find('.layui-layer-btn0').attr({'lay-submit':'','lay-filter':'*'});
		//.layui-layer-btn1为关闭，不填提交操作
		layero.find('.layui-layer-btn2').attr({'lay-submit':'','lay-filter':'*'});
		layero.find('.layui-layer-btn3').attr({'lay-submit':'','lay-filter':'*'});
		layero.find('.layui-layer-btn4').attr({'lay-submit':'','lay-filter':'*'});
	}
	/**
	 * 给定数据初始化表格，适用于一次性加载所有数据
	 */
	initTableWithData = function(options){
		var {elem,data,toolbar,rowtoolbar,cols,ctrl,where,timeCol,table_id,page} = options;
		var t = table.render({
			elem: elem,toolbar:toolbar, even: true, cols: cols, id: table_id,
			page: page||false,
			data:data,
			where:where
		}); 
		table.on('toolbar('+elem.substring(1)+')',function(obj){
			var checked = table.checkStatus(obj.config.id);
			ctrl[obj.event].call(this,checked.data);
		});
		table.on('tool('+elem.substring(1)+')',function(obj){
			ctrl[obj.event].call(this,obj.data);
		});
		return t;
	}
	return {tableCtrl:{initTable,initTableWithData,tableInfo,tableAdd,tableDel,tableEdit,batchDel},uploadwithLayui} 
});