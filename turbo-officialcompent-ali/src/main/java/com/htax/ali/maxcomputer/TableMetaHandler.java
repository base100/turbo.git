package com.htax.ali.maxcomputer;

import com.aliyun.odps.Odps;
import com.aliyun.odps.Table;
import com.aliyun.odps.account.Account;
import com.aliyun.odps.account.AliyunAccount;

/**
 * maxcomputer  数据表结构元数据抽取
 * @author eric
 * 2020-6-23
 */
public class TableMetaHandler {

	public static void main(String[] args) {
		Account account = new AliyunAccount("my_access_id", "my_access_key");
		Odps odps = new Odps(account);
		String odpsUrl = "<your odps endpoint>";
		odps.setEndpoint(odpsUrl);
		odps.setDefaultProject("my_project");
		for (Table t : odps.tables()){
			System.out.println(t.getName());
		}            
	}
}
