<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String contextPath = request.getContextPath();
	pageContext.setAttribute("ctx", contextPath);
%>
<!DOCTYPE html>
<html>

<head>
<script type="text/javascript">
 var lib_path = '${ctx}/static/js/libs';
 var _ctx = '${ctx}';
</script>
<meta charset="utf-8">
<title>三维球测试</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<script type="text/javascript" src="${ctx}/static/js/_tool-js/require.min.js"></script>
<script type="text/javascript" src="${ctx}/static/_examples/require.conf.js"></script>
<script type="text/javascript">
var data_dir = "${ctx}/static/_examples/data";
var xmlfile= data_dir+"/tiles/tms.xml";//瓦片地图中XMl的路径;
var imgfile=data_dir+"/tiles";//地图图片位置
require(["mars3d"],function(mars3d){
	 mars3d.createMap({
		    id: 'cesiumContainer',
		    url: "${ctx}/static/js/_gis/libs/config.json",
		    imageryProvider:new Cesium.WebMapServiceImageryProvider({
				  url:xmlfile
				}),  
		    success: function (viewer, gisdata, jsondata) {//地图成功加载完成后执行
		    	console.log(viewer);
		    	var layers = viewer.imageryLayers;
		    	var blackMarble = layers.addImageryProvider(Cesium.createTileMapServiceImageryProvider({
		    		url:imgfile,
		    	}));
		    	var dixing = viewer.entities.add({
					position : Cesium.Cartesian3.fromDegrees(118.4,32.9,0),//经纬高
					name:"阵地",
					model : {
						uri : data_dir+'/models/zj/dixing.gltf',//模型所在位置
						minimumPixelSize : 10,  //球缩小后的尺寸
						maximumScale : 20000,
						scale:3 //调整大小
					},
				}); 
		    }
		});
});

</script>
</head>
<body>
<div id="cesiumContainer"></div>
</body>
</html>