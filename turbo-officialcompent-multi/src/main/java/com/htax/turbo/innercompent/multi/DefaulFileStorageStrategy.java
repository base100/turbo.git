package com.htax.turbo.innercompent.multi;

/**
 * 默认的文件路径策略
 * @日期：2019-12-16上午5:50:40
 * @作者：乔兵
 * @版权所有：joe
 * @版本：1.0
 */
public class DefaulFileStorageStrategy extends FileStorageStrategy {

	
	@Override
	public String generalUploadFilePath(String rawFileName) {
		return super.getBaseDir()+rawFileName;
	}

}
