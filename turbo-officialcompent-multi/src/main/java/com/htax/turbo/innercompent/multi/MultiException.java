package com.htax.turbo.innercompent.multi;


/**
 * service 相关类异常
 * @日期：2019-09-14下午11:17:32
 * @作者：joe
 */
public class MultiException extends Exception {
	private static final long serialVersionUID = 1L;
   
	public MultiException(Throwable e){
		super( e);
	}
	
	public MultiException(String msg,Throwable e){
		super(msg, e);
	}
	public MultiException(String msg){
		super(msg);
	}
}
