package com.htax.turbo.innercompent.multi;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.htax.core.tools.util.StrUtil;
import com.htax.turbo.innercompent.base.para.CorePara.CoreInitCtx;
import com.htax.turbo.innercompent.base.para.ParaGetTool;

/**
 * 文件目录和文件名存放策略
 * @日期：2018-12-16上午5:51:16
 * @作者：乔兵
 * @版权所有：joe
 * @版本：1.0
 */
public abstract class FileStorageStrategy {
	//10m
	public static final long DEFAULT_UPLOADSIZE = 10485760l;
	public static final String UPLOAD_MAX_SIZE_KEY = "core.multi.upload.default.maxsize";
	public static final String UPLOAD_DEFAULT_BASEDIR = "core.multi.upload.default.basedir";
	public  String getTmpDir() {
		return getBaseDir()+File.separator+"tmp";
	}
    public String getBaseDir(){
    	String baseDir = ParaGetTool.getKernelPara(UPLOAD_DEFAULT_BASEDIR, System.getProperty("user.home"));
    	if(StrUtil.isBlank(baseDir)){
    		baseDir = CoreInitCtx.WORKDIR+File.separator+"upload"+File.separator;
		}
    	return baseDir;
    }
	public long getMaxSize() {
		Long maxSize = ParaGetTool.getKernelPara(Long.class,UPLOAD_MAX_SIZE_KEY, DEFAULT_UPLOADSIZE);
		if(maxSize==0||maxSize.longValue()==0l){
			maxSize = DEFAULT_UPLOADSIZE;
		}
		return maxSize;
	}
	/**
	 * 文件存储的全路径eg:E:/temp/soft/file.txt
	 * @param rawFileName
	 * @return
	 */
	public abstract String generalUploadFilePath(String rawFileName);
	
	/**
	 * 判断是否上传有有效文件
	 * @return
	 */
	public static boolean hasUploadFile(MultipartHttpServletRequest request) {
		boolean flag = false;
		Map<String, MultipartFile> multipartFiles = request.getFileMap();
		for (Iterator<Map.Entry<String, MultipartFile>> iterator = multipartFiles.entrySet().iterator(); iterator.hasNext();) {
			Map.Entry<String, MultipartFile> upLoadResult = iterator.next();
			CommonsMultipartFile file = (CommonsMultipartFile) upLoadResult.getValue();
			if (file.getFileItem().getSize() > 0 && file.getSize() > 0  ) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
}
