package com.htax.turbo.innercompent.base.controller;

import java.beans.PropertyEditorSupport;

import org.springframework.util.StringUtils;
/**
 * 整形处理
 * @日期：2019-09-14下午7:39:14
 * @作者：joe
 */
public class IntEditor extends PropertyEditorSupport {

	@Override
	public String getAsText() {
		return super.getAsText();
	}
	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		if (!StringUtils.hasText(text)) {
			setValue(0);
		} else {
			setValue(Integer.parseInt(text));
		}
	}
}
