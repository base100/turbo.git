package com.htax.turbo.innercompent.base.listener.inner;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.htax.core.tools.util.FileUtil;
import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;
import com.htax.turbo.innercompent.base.listener.AppInitListener;
import com.htax.turbo.innercompent.base.vo.LogType;


/**
 * 核心参数监听器
 * APP启动后默认加载必要的系统配置参数
 * @author joe
 * 2019-09-14 下午3:20:03
 */
public class BannerInitListener implements AppInitListener {
	
  static Logger logger = Logger.getLogger(LogType.SYSINNER);
	public void init(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("/banner.txt")));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = in.readLine()) != null){
			    buffer.append(line).append("\r\n");
			}
			System.out.println(buffer.toString());
		} catch (IOException e) {
			//do nothing
		}
	}

	public void destroyed(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
	}

	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam)
			throws InitException {
		return SIGNAL.STOP;
	}
}
