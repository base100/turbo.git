package com.htax.turbo.innercompent.base.controller;

/**
 * 如果implements 此类，将会被注入 request请求头中的的referer，可用于返回前页使用
 * @日期：2019-09-14下午11:21:04
 * @作者：joe
 */
public interface RefererAble {

	public void setReferer(String referer);
	public String getReferer();
	
}
