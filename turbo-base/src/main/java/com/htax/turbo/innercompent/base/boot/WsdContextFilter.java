package com.htax.turbo.innercompent.base.boot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

import com.htax.turbo.innercompent.base.filter.AppInitFilter;
import com.htax.turbo.innercompent.base.vo.LogType;


/**
 * filter管理类
 * @author joe
 * 2019-09-14 下午5:09:34
 */
public class WsdContextFilter implements Filter{
    
	private static  Logger logger = Logger.getLogger(LogType.SYSINNER);

	private static List<AppInitFilter> registeredInterceptorBeans = new ArrayList<AppInitFilter>();
	
	
	public void init(FilterConfig filterConfig) throws ServletException {
		
		
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		
	}

	public void destroy() {
	
		
	}

}
