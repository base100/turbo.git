package com.htax.turbo.innercompent.base.handler.thread;
/**
 * 解析数据回调
 * @author joe
 * @time  2016-4-7 下午12:32:50
 */
public interface DataParseCallBack {

	public void parse(Object obj) throws Exception;
}
