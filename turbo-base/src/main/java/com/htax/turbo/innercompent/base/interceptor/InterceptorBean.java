package com.htax.turbo.innercompent.base.interceptor;

import org.springframework.web.servlet.ModelAndView;

/**
 * mvc拦截器基础类
 * @author joe
 * 2019-09-14 下午3:24:45
 */
public abstract class InterceptorBean {

	/**
	 * 当前连接器拦截处理完成后是继续后续操作
	 * @日期：2019-09-14上午6:45:45
	 * @作者：joe
	 */
	public enum SIGNAL {
		STOP, CONTINUE
	}

	/**在controller方法执行之前进行拦截
	 * @param request
	 * @param response
	 * @param handlerMethod
	 * @return
	 */
	public SIGNAL beforeControllerMethodExecute(InterceptorArgs interceptorArgs) {
		return SIGNAL.CONTINUE;
	}

	/**
	 * 在controller方法执行之后进行拦截
	 * @param request
	 * @param response
	 * @param handlerMethod
	 * @param modelAndView
	 */
	public void afterControllerMethodExecute(InterceptorArgs interceptorArgs, ModelAndView modelAndView) {
	}

	/**
	 * 拦截器执行异常之后的动作
	 * @return
	 */
	public abstract SIGNAL interceptorExecuteErrorSignal(Exception beforeControllerMethodExecuteException);

	/**
	 * 拦截器执行异常之后的动作,有默认处理
	 * @return
	 */
	public SIGNAL errorSignal(Exception beforeControllerMethodExecuteException) {
		SIGNAL signal = interceptorExecuteErrorSignal(beforeControllerMethodExecuteException);
		return signal == null ? SIGNAL.CONTINUE : signal;
	}
}
