package com.htax.turbo.innercompent.base.exception;

/**
 * 数据库操作异常
 * @日期：2019-09-14下午11:16:59
 * @作者：joe
 */
public class DaoException extends Exception {
	private static final long serialVersionUID = 1L;
   
	public DaoException(String msg,Throwable e){
		super(msg, e);
	}
	public DaoException(Throwable e){
		super(e);
	}
}
