package com.htax.turbo.innercompent.base;

import java.util.Map;

import org.springframework.context.ApplicationContext;


/**
 * 服务提供类,
 * 注：spring加载完成之后才可使用，如要在spring加载期间使用某个服务，需使用注入方式注入BaseService.或者extends BaseService例如：@autoware BaseService
 * 
 * @日期：2019-09-14下午11:22:45
 * @作者：joe
 */
public class CmCommon {


	// -----------------------------------------------------
	public static Object getBeanByName(String beanName) {
		return context.getBean(beanName);
	}

	public static <T> Map<String, T> getBeanByType(Class<T> type) {
		return context.getBeansOfType(type);
	}

	public static <T> T getBean(Class<T> type) {
		return context.getBean(type);
	}

	private static ApplicationContext context;

	/**
	 * inject by @InitListener after spring context inited...
	 * 
	 * @param contextinject
	 */
	public static void setContext(ApplicationContext contextinject) {
		if (context != null) {
			return;
		}
		context = contextinject;
	}
}
