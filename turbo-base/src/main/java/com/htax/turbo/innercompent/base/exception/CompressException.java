package com.htax.turbo.innercompent.base.exception;


/**
 * 压缩相关异常
 * @author joe
 * @2019年1月21日 @下午3:38:44
 */
public class CompressException extends Exception{

	public CompressException(Throwable e) {
		super();
	}
	public CompressException(String msg) {
		super(msg);
	}
	public CompressException(String msg,Throwable e) {
		super(msg,e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
