package com.htax.turbo.innercompent.base.exception;


/**
 * Contoller 相关类异常
 * @日期：2019-09-14下午11:16:49
 * @作者：joe
 */
public class ContollerException extends Exception implements MessageAlertable,Logable{
	private static final long serialVersionUID = 1L;
   
	public ContollerException(String msg,Throwable e){
		super(msg, e);
	}
	public ContollerException(String msg){
		super(msg);
	}
}
