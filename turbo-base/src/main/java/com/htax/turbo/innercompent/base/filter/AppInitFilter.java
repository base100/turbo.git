package com.htax.turbo.innercompent.base.filter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;


/**
 * 项目自定义filter接口
 * @author joe
 * 2019-09-14 下午5:14:05
 */
public interface AppInitFilter {

	/**
	 * 初始化
	 * @param contextEvent
	 * @param bootParam
	 * @throws InitException
	 */
	public void init(FilterConfig filterConfig, BootParam bootParam) throws InitException;

	/**
	 * 关闭
	 */
	public void destroy() throws InitException;
	/**
	 * 执行filter
	 * @param contextEvent
	 * @param bootParam
	 * @throws InitException
	 */
	public void doFilter(ServletRequest request, ServletResponse response, BootParam bootParam) throws InitException;
	/**
	 * filter成功之后是否继续后续的filter
	 * @param contextEvent
	 * @param bootParam
	 * @return
	 * @throws InitException
	 */
	public SIGNAL onSuccess(ServletContextEvent contextEvent, BootParam bootParam) throws InitException;
	/**
	 * filter失败之后是否继续后续的filter
	 * @param contextEvent
	 * @param bootParam
	 * @return
	 * @throws InitException
	 */
	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam) throws InitException;
}
