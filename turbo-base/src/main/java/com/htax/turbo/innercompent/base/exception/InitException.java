package com.htax.turbo.innercompent.base.exception;


/**
 * service 相关类异常
 * @日期：2019-09-14下午11:17:12
 * @作者：joe
 */
public class InitException extends Exception implements MessageAlertable,Logable{
	private static final long serialVersionUID = 1L;
   
	public InitException(Throwable e){
		super( e);
	}
	public InitException(String msg,Throwable e){
		super(msg, e);
	}
	public InitException(String msg){
		super(msg);
	}
}
