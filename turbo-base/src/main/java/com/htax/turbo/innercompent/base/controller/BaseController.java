package com.htax.turbo.innercompent.base.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.htax.turbo.innercompent.base.para.CorePara.StaticPara;


/**
 * Contoller 基类，所有controll请继承此类 
 * @日期：2019-9-14下午11:05:48
 * @作者：joe
 */
public class BaseController extends AjaxJsonBaseController implements RefererAble {
	public static final String COMMON_ALERT_KEY = "alert_key";
	public static final String VO_KEY = "vo";  //单个实体类型
	public static final String DATA_KEY = "_data"; //默认的查询结果返回数据key值，返回结果可能是一个VO对象，也可能是一个List<T>对象， 在JSP页面通过${_data}取值
	public static final String TOTAL_KEY = "_total"; //记录总条数
	public static final String LAYOUT_XML_FILENAME = "xml_filename";
	public static final String LAYOUT_INIT_URL = "inti_url";

	public String backUrl = "";

	public void setLayOutData(Model model, String xmlFileName, String initUrl) {
		model.addAttribute(LAYOUT_XML_FILENAME, xmlFileName);
		model.addAttribute(LAYOUT_INIT_URL, initUrl);
	}

	public void setReferer(String referer) {
		this.backUrl = referer;
	}
	/**
	 * @param model
	 * @param msg
	 * @return
	 */
	public String toAlertPage(Model model, String msg) {
		model.addAttribute(COMMON_ALERT_KEY, msg);
		return StaticPara.COMMON_ALERT_MODEL_PAGE;
	}
	/**
	 * @param alertPage
	 * @param model
	 * @param msg
	 * @return
	 */
	public String toAlertPage(String alertPage, Model model, String msg) {
		model.addAttribute(COMMON_ALERT_KEY, msg);
		return alertPage;
	}
	
	/**
	 * @param alertPage
	 * @param request
	 * @param msg
	 * @return
	 */
	public String toAlertPage(String alertPage, HttpServletRequest request, String msg) {
		request.setAttribute(COMMON_ALERT_KEY, msg);
		return alertPage;
	}

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(int.class, new IntEditor());
		binder.registerCustomEditor(Date.class, new MultiCustomDateEditor(true));
	}

	public void setTotoal(Model model, int total) {
		model.addAttribute(TOTAL_KEY, total);
	}



	
	public String getAppRoot(HttpServletRequest request)
	  {
	    return request.getSession().getServletContext().getRealPath("/");
	  }

	public String getReferer() {
		return backUrl;
	}
}
