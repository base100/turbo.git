package com.htax.turbo.innercompent.base.listener.inner;

import java.io.File;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.htax.core.tools.util.StrUtil;
import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;
import com.htax.turbo.innercompent.base.listener.AppInitListener;
import com.htax.turbo.innercompent.base.para.CorePara.CoreInitCtx;
import com.htax.turbo.innercompent.base.vo.LogType;


/**
 * 核心参数监听器
 * APP启动后默认加载必要的系统配置参数
 * @author joe
 * 2019-09-14 下午3:20:03
 */
public class CoreParamInitListener implements AppInitListener {
	
  static Logger logger = Logger.getLogger(LogType.SYSINNER);
	public void init(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
		//设置项目绝对路径
		CoreInitCtx.APP_ROOT = contextEvent.getServletContext().getRealPath("/");
		
		
		if(contextEvent!=null) {
		 //处理初始化数据
		}
		/**
		 * 设置环境变量
		 */
		if(StrUtil.isNotBLank(CoreInitCtx.WORKDIR)) {
		  File f = new File(CoreInitCtx.WORKDIR);
		  if(!f.exists()) {
		    f.mkdir();
		  }
		  if(contextEvent!=null) {
		     contextEvent.getServletContext().log("使用工作目录："+CoreInitCtx.WORKDIR);
	      }
		}else {
		  if(contextEvent!=null) {
		    contextEvent.getServletContext().log("未设置工作目录参数["+CoreInitCtx.WORKDIR_KEY+"]...");
		  }
		}
	}

	public void destroyed(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
	}

	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam)
			throws InitException {
		return SIGNAL.STOP;
	}
}
