package com.htax.turbo.innercompent.base.filter.inner;

import java.io.UnsupportedEncodingException;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.filter.AppInitFilter;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;
import com.htax.turbo.innercompent.base.para.CorePara.StaticPara;

/**
 * http请求相应的编码filter
 * @author joe
 * 2019-09-14 下午5:10:54
 */
public class CharacterEncodingFilter implements AppInitFilter{

	public static  String ENCODING = StaticPara.HTTP_CHARSET;
	public void init(FilterConfig filterConfig,BootParam bootParam) throws InitException {
		
	}
	public void doFilter(ServletRequest request, ServletResponse response
			,BootParam bootParam) throws  InitException {
			try {
				request.setCharacterEncoding(ENCODING);
				response.setCharacterEncoding(ENCODING);
			} catch (UnsupportedEncodingException e) {
				//不会报错
			}
	}

	public void destroy() {
		
	}

	public SIGNAL onSuccess(ServletContextEvent contextEvent,
			BootParam bootParam) throws InitException {
		return SIGNAL.CONTINUE;
	}

	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam)
			throws InitException {
		return SIGNAL.CONTINUE;
	}

}
