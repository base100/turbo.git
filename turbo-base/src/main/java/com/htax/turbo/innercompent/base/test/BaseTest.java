package com.htax.turbo.innercompent.base.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.htax.turbo.innercompent.base.boot.BootParam;


/**
 * 测试用基类
 * @日期：2019-09-14下午11:06:42
 * @作者：joe
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({ "classpath:spring/applicationContext.xml", "classpath:spring/spring-mvc-servlet.xml" })
public class BaseTest {
	
	public BootParam getContext() throws Exception {
		return BootParam.getBootParam();
	}
}
