package com.htax.turbo.innercompent.base.interceptor;

import org.springframework.stereotype.Component;

@Component
public class NavigationInterceptor  extends InterceptorBean{

	public static final String NAVIGATION_KEY = "navigation";

	@Override
	public SIGNAL beforeControllerMethodExecute(InterceptorArgs interceptorArgs) {
		
		return SIGNAL.CONTINUE;
	}

	@Override
	public SIGNAL interceptorExecuteErrorSignal(Exception beforeControllerMethodExecuteException) {
		return SIGNAL.CONTINUE;
	}
}
