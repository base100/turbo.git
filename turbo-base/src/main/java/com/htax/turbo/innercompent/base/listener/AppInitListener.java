package com.htax.turbo.innercompent.base.listener;

import javax.servlet.ServletContextEvent;

import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;

/**
 * 项目启动器接口
 * @author joe
 * 2019-09-14 下午3:19:15
 */
public interface AppInitListener {
	public void init(ServletContextEvent contextEvent, BootParam bootParam) throws InitException;

	public void destroyed(ServletContextEvent contextEvent, BootParam bootParam) throws InitException;
	
	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam) throws InitException;
}
