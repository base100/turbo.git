package com.htax.turbo.innercompent.base.boot;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoaderListener;

import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.listener.AfterSpringListener;
import com.htax.turbo.innercompent.base.listener.BeforeSpringListener;
import com.htax.turbo.innercompent.base.listener.inner.CoreParamInitListener;
import com.htax.turbo.innercompent.base.vo.LogType;

/**
 * 项目监听器，无需另外配置spring的监听
 * 
 * @author joe 2019-5-26 下午2:00:00
 */
public class WsdContextLoaderListener extends ContextLoaderListener {
	
	private Logger logger = Logger.getLogger(LogType.SYSINNER);

	private BeforeSpringListener beforeSpringListener = null;
	private AfterSpringListener afterSpringListener = null;

	/**
	 * 启动监听
	 */
	public void contextInitialized(ServletContextEvent event) {
		try {
			// 初始化boot配置文件
			BootParam bootParam = BootParam.getBootParam();
			// 初始化内部需要在spring之前初始化的listener
			initBeforeSpringCoreListener(event, bootParam);
			// spring之前监听-用户配置的
			beforeSpringListener = new BeforeSpringListener();
			beforeSpringListener.contextInitialized(event);
			// 初始化 spring 的监听
			super.contextInitialized(event);
			// 初始化内部需要在spring之后初始化的listener
			initAfterSpringCoreListener(event, bootParam);
			// spring之后监听-用户配置的
			afterSpringListener = new AfterSpringListener();
			afterSpringListener.contextInitialized(event);
		} catch (Exception e) {
			logger.error("由于项目监听启动失败,已停止项目启动，请检查问题后重新启动...", e);
			contextDestroyed(event);
		}
	}

	/**
	 * 关闭监听
	 */
	public void contextDestroyed(ServletContextEvent event) {
		try {
			// spring之前监听-用户配置的
			beforeSpringListener.contextDestroyed(event);
			// spring 监听
			super.contextDestroyed(event);
			// spring之后监听-用户配置的
			afterSpringListener.contextDestroyed(event);

			// 内部listener，在所有监听之后关闭
			destoryCoreListener(event);
		} catch (Exception e) {
			logger.error("关闭监听失败.", e);
			// 只能成功不能失败
		}

	}

	// before
	private CoreParamInitListener coreParamInitListener = null;

	/**
	 * 初始化内置的
	 * 
	 * @param event
	 * @throws InitException
	 */
	private void initBeforeSpringCoreListener(ServletContextEvent event, BootParam bootParam) throws InitException {
		try {
			coreParamInitListener = new CoreParamInitListener();
			coreParamInitListener.init(event, bootParam);

		} catch (InitException e) {
			throw e;
		}
	}

	private void initAfterSpringCoreListener(ServletContextEvent event, BootParam bootParam) throws InitException {
		/*
		 * try {
		 * 
		 * daoInitListener = new DaoInitListener();
		 * daoInitListener.init(event,bootParam);
		 * 
		 * } catch (InitException e) { throw e; }
		 */
	}

	private void destoryCoreListener(ServletContextEvent event) throws InitException {
		coreParamInitListener.destroyed(event, null);
		/*
		 * daoInitListener.destroyed(event, null); cacheInitListener.destroyed(event,
		 * null);
		 */
	}
}
