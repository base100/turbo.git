package com.htax.turbo.innercompent.base.handler.thread;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;
import com.htax.turbo.innercompent.base.listener.AppInitListener;


/**
 * 启动线程池监听启动服务
 */
public class ThreadPoolMonitorListener implements AppInitListener {

	//间隔时间
	public static int  gapTime = 10*1000;
	public Thread threadPoolMonitorThread;
	private static Logger logger = Logger.getLogger(ThreadPoolMonitorListener.class);
	@Override
	public void init(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
		if(threadPoolMonitorThread==null){
			threadPoolMonitorThread = new Thread(()->{
					while(true){
						logger.info(ThreadPool.printAllPoolStatic());
						try {
							Thread.sleep(gapTime);
						} catch (InterruptedException e) {
							//
						}
					}
			});
		}
		if(!threadPoolMonitorThread.isAlive()){
			threadPoolMonitorThread.start();
			contextEvent.getServletContext().log("线程池监听启动,间隔"+gapTime+" ms");
		}
	}
	@Override
	public void destroyed(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
		if(threadPoolMonitorThread!=null){
			threadPoolMonitorThread.interrupt();
		}
	}
	@Override
	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
		return SIGNAL.CONTINUE;
	}
}
