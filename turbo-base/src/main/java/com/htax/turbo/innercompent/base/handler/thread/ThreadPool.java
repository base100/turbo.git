package com.htax.turbo.innercompent.base.handler.thread;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.htax.turbo.innercompent.base.exception.ServiceException;
/**
 * 线程池
 * @author qiao
 * @2015-3-3 @上午10:43:23
 */
public class ThreadPool {

	
	private static ConcurrentHashMap<String, ThreadPoolTaskExecutor> concurrentHashMap = new ConcurrentHashMap<String, ThreadPoolTaskExecutor>();
	
	/**
	 * 创建线程池
	 * @param poolType 线程池名称，
	 * @param xms 线程池最小值
	 * @param xmx 线程池最大值
	 * @param cache 线程池缓存数，当达到xms时，任务会放入cache中，当cache满后再使用xmx参数。
	 */
	public static  void createPool(String poolType,int xms,int xmx,int cache){
		if(concurrentHashMap.get(poolType)==null){
			ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
			executor.setThreadNamePrefix(poolType);
			executor.setWaitForTasksToCompleteOnShutdown(false);
	        executor.setCorePoolSize(xms);
	        executor.setMaxPoolSize(xmx);
	        executor.setQueueCapacity(cache);
	        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
	        executor.initialize();
	        concurrentHashMap.put(poolType, executor);
		}
	}
	/**
	 * 创建线程池
	 * @param poolType 线程池名称，
	 * @param xms 线程池最小值
	 * @param xmx 线程池最大值
	 */
	public static  void createPool(String poolType,int xms,int xmx){
		createPool(poolType, xms, xmx, Integer.MAX_VALUE);
	}
	
	public static String printPoolStatic(String poolType){
		ThreadPoolTaskExecutor pool = getPool(poolType);
		ThreadPoolExecutor work = pool.getThreadPoolExecutor();
		return  "线程池名称："+poolType+
				"/活动线程数："+work.getActiveCount()+
				"/排队线程数："+work.getQueue().size();
	}
	public static String printAllPoolStatic(){
		StringBuffer buffer = new StringBuffer();
		for (Iterator<String> iterator = concurrentHashMap.keySet().iterator(); iterator.hasNext();) {
			String poolType = iterator.next();
			buffer.append(printPoolStatic(poolType));
			if(iterator.hasNext()){
				buffer.append("\r\n");
			}
		}
		return  buffer.toString();
	}
	private static ThreadPoolTaskExecutor getPool(String poolType){
		return concurrentHashMap.get(poolType);
	}
	public static  void execute(String poolType,Runnable task) throws ServiceException{
		ThreadPoolTaskExecutor executor = getPool(poolType);
		if(executor==null){
			throw new ServiceException(poolType+" 线程池未初始化，请使用createPool方法初始化");
		}
		executor.execute(task);
	}
	public static  void destroy(String poolType) throws ServiceException{
		ThreadPoolTaskExecutor executor = getPool(poolType);
		if(executor==null){
			throw new ServiceException(poolType+" 线程池未初始化，请使用createPool方法初始化");
		}
		executor.destroy();
	}
	//------------测试
	 @SuppressWarnings("unused")
	private static void testPool() throws Exception{
	    	ThreadPool.createPool("jsm-custom", 10, 10, Integer.MAX_VALUE);
	    	for (int i = 0; i < 10; i++) {
	    		String id = RandomUtils.nextInt(1)+"";
	    		//System.out.println("gen id:"+id);
	    		while (lockContain(id)) {
	    			Thread.sleep(500);
				}
//    			System.out.println(" do task id:"+ id);
    			ThreadPool.execute("jsm-custom", getTask(id));
			}
	 }
    public static Runnable getTask(final String num){
    	return new Runnable() {
			@Override
			public void run() {
				 try {
	                    Thread.sleep(1000);
	                    idLocks.remove(num);
	                } catch (Exception e) {
	                    e.printStackTrace();
	                }
				
			}
		};
    }
    private static final Set<String> idLock = new HashSet<String>();
    private static Set<String> idLocks =Collections.synchronizedSet(idLock);
    @SuppressWarnings("unused")
	public static void pring(){
    	if(idLocks.isEmpty())
//    		System.out.println("locks empty");
    	for (Iterator<String> iterator = idLocks.iterator(); iterator.hasNext();) {
			String type = iterator.next();
//			System.out.print(type+"/");
//			System.out.println();
		}
    }
    private static  boolean lockContain(String id){
    	synchronized (idLocks) {
    		boolean has = false;
    		has = idLocks.contains(id);
//    		if(has){
//    			System.out.println("id:"+id+" is locked");
//    		}else{
//    			System.out.println("id:"+id+" is not locked");
//    		}
    		if(!has){idLocks.add(id);}
    		return has;
		}
	}
//    public static void main(String[] args) throws Exception {
//    	testPool();
//    	Thread.sleep(5000);
//    	pring();
//	}
}
