package com.htax.turbo.innercompent.orm.ormcreater;

import java.lang.reflect.Field;

/**
 * module 解析过滤接口
 * @日期：2019-09-14下午11:18:49
 * @作者：joe
 */
public interface ModuleHandlerFilter {

	/**
	 * 自定义规则来决定当前的field是否不解析
	 * @param moduleField
	 * @return
	 */
	public boolean exclude(Field moduleField);
	
	/**
	 * 自定义规则来决定当前的field是否解析
	 * @param moduleField
	 * @return
	 */
	public boolean include(Field moduleField);
}
