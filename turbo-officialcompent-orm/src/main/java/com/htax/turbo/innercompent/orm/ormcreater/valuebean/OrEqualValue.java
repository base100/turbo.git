package com.htax.turbo.innercompent.orm.ormcreater.valuebean;
/**
 * OrEqual
 * @author joe
 * 2019-5-3 下午5:25:05
 */
public class OrEqualValue implements OrValue{
	private Object value;

	public OrEqualValue(Object value){
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
}
