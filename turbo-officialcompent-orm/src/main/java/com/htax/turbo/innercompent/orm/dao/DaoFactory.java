package com.htax.turbo.innercompent.orm.dao;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.util.StringUtils;

import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.orm.dao.impl.DbcpPoolDao;
import com.htax.turbo.innercompent.orm.jdbcpool.DataSourceProperty;

/**
 * 创建DAO的工厂方法
 * @author joe
 * 2019-5-26 上午10:44:37
 */
public class DaoFactory {

	public static final String DEFAULT_DAOSTR = "DEFAULT";
	private static  ConcurrentHashMap<String, Dao> daoPool = new ConcurrentHashMap<String, Dao>();
	/**
	 * 获取dao
	 * @param dateSourceName
	 * @return
	 */
	public static Dao getDao(String dateSourceName){
		return daoPool.get(dateSourceName.toUpperCase());
	}
	/**
	 * 获取默认的dao
	 * @param dateSourceName
	 * @return
	 */
	public static Dao getDao(){
		return daoPool.get(DEFAULT_DAOSTR);
	}
	/**
	 * 获取dao
	 * @param dateSourceName
	 * @param dataSourceProperty
	 * @return
	 */
	public static synchronized Dao createDao(String dateSourceName,DataSourceProperty dataSourceProperty){
		if(!StringUtils.hasText(dateSourceName)){
			dateSourceName = DEFAULT_DAOSTR;
		}
		dateSourceName = dateSourceName.trim().toUpperCase();
		if(!daoPool.contains(dateSourceName)){
			DbcpPoolDao dbcpPoolDao = new DbcpPoolDao();
			dbcpPoolDao.initDao(dataSourceProperty);
			daoPool.put(dateSourceName, dbcpPoolDao);
		}
		return daoPool.get(dateSourceName);
	}
	/**
	 * 关闭所有dao
	 */
	public static synchronized void closeAll(){
		for (Iterator<Dao> iterator = daoPool.values().iterator(); iterator.hasNext();) {
			Dao dao = (Dao) iterator.next();
			dao.closeDao();
		}
	}
	public static void createOneTimeDao(String datasourceName,DataSourceProperty dataSourceProperty,OneTimeDaoForUse daoForUse ) throws DaoException {
		Dao onTimeDao = new DbcpPoolDao();
		onTimeDao.initDao(dataSourceProperty);
		if(daoForUse!=null) {
			try {
				daoForUse.doSomeThing(onTimeDao);
			} finally {
				onTimeDao.closeDao();
			}
		}
		
	}
	
	public interface OneTimeDaoForUse{
		public void doSomeThing(Dao dao) throws DaoException;
	}
}
