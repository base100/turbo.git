package com.htax.turbo.innercompent.orm.ormcreater;
/**
 * 自增长的int
 * @author joe
 * 2019-5-3 下午5:27:44
 */
public class IncreaseInt {

	private int value;
	public IncreaseInt(int init){
		value = init;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public void increase(){
		value ++;
	}
	
}
