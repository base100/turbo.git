package com.htax.turbo.innercompent.orm.jdbcpool;

/**
 * 连接池处理
 * @author joe
 * 2019-5-26 上午10:11:23
 */
public interface JdbcPoolHandler {

	/**
	 * 初始化数据源
	 * @param dataSourceProperty
	 */
	public void initDao(DataSourceProperty dataSourceProperty);
	/**
	 * 关闭数据源
	 * @param dataSource
	 */
	public void closeDao();
}
