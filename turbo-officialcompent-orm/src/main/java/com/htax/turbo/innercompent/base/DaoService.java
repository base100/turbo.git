package com.htax.turbo.innercompent.base;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.htax.core.tools.util.StrUtil;
import com.htax.turbo.innercompent.orm.dao.Dao;
import com.htax.turbo.innercompent.orm.dao.DaoFactory;


/**
 * 数据库操作基类，如果有自定义的数据库操作类，请继承此方法
 * @日期：2019-09-14下午11:03:56
 * @作者：joe
 */
@Component
@Lazy
public class DaoService {
	
	/**
	 * 获取数据库操作类
	 * 
	 * @return
	 */
	public static Dao getDao() {
		return (Dao) DaoFactory.getDao();
	}
	/**
	 * 获取数据库操作类
	 * 
	 * @return
	 */
	public static Dao getDao(String dateSourceName) {
		if(StrUtil.isBlank(dateSourceName)){
			return getDao();
		}
		return (Dao) DaoFactory.getDao(dateSourceName);
	}
}
