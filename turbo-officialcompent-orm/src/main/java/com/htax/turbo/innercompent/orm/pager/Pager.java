package com.htax.turbo.innercompent.orm.pager;


/**
 * 分页处理封装
 * @日期：2019-09-14下午11:23:42
 * @作者：joe
 */
public class Pager {

    
	public Pager(){
		
	}
	public Pager(int offset,int pagesize){
		this.pg.setOffset(offset);
		this.pg.setPagesize(pagesize);
	}
	private Pg pg = new Pg();

	public Pg getPg() {
		return pg;
	}

	public void setPg(Pg pg) {
		this.pg = pg;
	}

	@Override
	public String toString() {
		return "Pager [pg=" + pg + "]";
	}
}
