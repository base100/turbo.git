package com.htax.turbo.innercompent.orm.ormcreater;

import java.sql.Types;

import org.springframework.jdbc.core.SqlParameterValue;

import com.htax.turbo.innercompent.orm.dao.Dao;


/**
 * 
 * @author bing
 *
 */
public class DateValue extends SqlParameterValue{

	public DateValue(Object value,Dao dao) {
		//mysql 用date 会截断 oracle不会
		super(dao.isOracle()?Types.TIME:Types.TIMESTAMP, value);
	}
	public DateValue(String value,Dao dao) {
		//mysql 用date 会截断 oracle不会
		super(dao.isOracle()?Types.TIME:Types.TIMESTAMP, value);
	}
}
