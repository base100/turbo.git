package com.htax.turbo.innercompent.orm.ormcreater;

/**
 * 单事务操作类，有回值
 * @日期：2019-09-14下午11:22:37
 * @作者：joe
 */
public interface SingleTransationCircleWithResult<T> {

	public   T actionInCircle() throws RuntimeException;
}
