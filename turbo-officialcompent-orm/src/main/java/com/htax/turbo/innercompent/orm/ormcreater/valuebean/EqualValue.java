package com.htax.turbo.innercompent.orm.ormcreater.valuebean;

/**
 * 相等值 
 * @author joe
 * 2019-5-3 下午5:23:33
 */
public class EqualValue {

	private Object value;
	public EqualValue(Object value){
		this.value = value;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
} 
