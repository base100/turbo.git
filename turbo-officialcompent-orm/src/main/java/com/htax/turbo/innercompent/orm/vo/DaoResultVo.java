package com.htax.turbo.innercompent.orm.vo;

/**
 * 通用结果类
 *
 * @author joe
 * @2014-11-21 @下午5:30:09
 */
public class DaoResultVo<T> {

    private String code;
    private String msg;
    private boolean success;
    private T data;

    public DaoResultVo() {

    }

    public DaoResultVo(String code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public DaoResultVo(boolean success) {
        super();
        this.success = success;
    }

    public DaoResultVo(String code, String msg, T data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "ResultVo [code=" + code + ", msg=" + msg + ", success=" + success + ", data=" + data
                + "]";
    }

}
