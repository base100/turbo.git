package com.htax.turbo.innercompent.orm.ormcreater.valuebean;
/**
 * 区间值 
 * @日期：2019-6-6下午7:38:15
 * @作者：joe
 */
public class OrBetweenValue extends BetweenValue implements OrValue{

	public OrBetweenValue(Object begin, Object end) {
		super(begin, end);
	}
	public OrBetweenValue(Object begin, Object end,boolean includeBegin,boolean includeEnd) {
		super(begin, end,includeBegin,includeEnd);
	}
} 
