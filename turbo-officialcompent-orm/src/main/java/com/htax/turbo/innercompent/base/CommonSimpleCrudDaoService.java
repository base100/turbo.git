package com.htax.turbo.innercompent.base;

import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.htax.core.tools.util.StrUtil;
import com.htax.core.tools.util.UUIDGenerator;
import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.orm.ormcreater.WhereCondition;


/**
 * 为防止重复写无用代码，这里对对象的简单操作做了service方法的封装，在controller调用。
 * 为防止过多的代码sql组装代码渗透入controller层，这里只对最简单的常用方法做通用封装,并且都是单事物操作。有参数的查询请自行编写。
 * 
 * @author joe
 *
 */
@Service
public class CommonSimpleCrudDaoService {

	/**
	 * 通用的时间戳字段名
	 */
	public static final String TIMESTAMP_FIELD_NAME = "ts";
	/**
	 * 通用的id字段名
	 */
	public static final String ID_FIELD_NAME = "id";
	
	private static Logger LOG = Logger.getLogger(CommonSimpleCrudDaoService.class);
	/**
	 * 添加
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void insert(T modulefeatureBean) throws ServiceException {
		String action = "添加"+modulefeatureBean.getClass().getName();
		try {
			DaoService.getDao().insertModule(action, modulefeatureBean);
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	/**
	 * 添加
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void insertAndSetUUid(T modulefeatureBean) throws ServiceException {
		String action = "添加"+modulefeatureBean.getClass().getName()+"并设置id值为当前时间";
		try {
			setUUid(modulefeatureBean);
			DaoService.getDao().insertModule(action, modulefeatureBean);
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
		
	}
	/**
	 * 注入时间戳字段值，注意此属性是一般是业务无关的，如果是有关系的，请自行设置。
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> T insertAndSetTsUUid(T modulefeatureBean) throws ServiceException {
		String action = "添加"+modulefeatureBean.getClass().getName()+"并设置ts值为当前时间";
		try {
			setUUid(modulefeatureBean);
			setTs(modulefeatureBean);
			DaoService.getDao().insertModule(action, modulefeatureBean);
			return modulefeatureBean;
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	/**
	 * 注入时间戳字段值，注意此属性是一般是业务无关的，如果是有关系的，请自行设置。
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void insertAndSetTs(T modulefeatureBean) throws ServiceException {
		String action = "添加"+modulefeatureBean.getClass().getName()+"并设置ts值为当前时间";
		try {
			setTs(modulefeatureBean);
			DaoService.getDao().insertModule(action, modulefeatureBean);
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	/**
	 * 添加
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据,确保有id属性
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void deleteById(Class<T> clazz,Object id) throws ServiceException {
		String action = "根据id删除"+clazz.getName();
		try {
			DaoService.getDao().deleteModuleById(action, clazz, new Object[] {id});
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	/**
	 * 批量删除
	 * @param ids
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void batchDel(Class<T> clazz, String[] ids) throws ServiceException{
		try {
			DaoService.getDao().deleteModuleById("批量删除对象"+clazz, clazz, ids);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	/**
	 * 添加
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据,确保有id属性
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void updateById(T modulefeatureBean) throws ServiceException {
		String action = "修改"+modulefeatureBean.getClass().getName();
		try {
			DaoService.getDao().updateModuleById(action, modulefeatureBean,null);
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	/**
	 * 注入时间戳字段值，注意此属性是一般是业务无关的，如果是有关系的，请自行设置。
	 * @param action 操作名称
	 * @param modulefeatureBean 插入对象数据
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> void updateByIdAndSetTs(T modulefeatureBean) throws ServiceException {
		String action = "修改"+modulefeatureBean.getClass().getName()+"并设置ts为当前值";
		try {
			setTs(modulefeatureBean); 
			DaoService.getDao().updateModuleById(action, modulefeatureBean,new String[]{});
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	
	/**
	 * 查询所有
	 * @param action
	 * @param clazz
	 * @return
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> List<T> listAll(Class<T> clazz) throws ServiceException{
		String action = "查询所有"+clazz.getName();
		try {
			return DaoService.getDao().listModule(action, clazz, null);
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	/**
	 * 查询所有
	 * @param action
	 * @param clazz
	 * @return
	 * @throws ServiceException
	 */
	public <T extends ModuleFeatureBean> List<T> listAllAndOrderByTs(Class<T> clazz) throws ServiceException{
		String action = "查询所有"+clazz.getName();
		try {
			WhereCondition condition = new WhereCondition();
			condition.orderBy(TIMESTAMP_FIELD_NAME+" desc");
			return DaoService.getDao().listModule(action, clazz, condition);
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	
	public <T extends ModuleFeatureBean> T getById(Class<T> clazz,Object id) throws ServiceException{
		String action = "根据id查询"+clazz.getName();
		try {
			return DaoService.getDao().getModuleById(action, clazz, new Object[] { id });
		} catch (DaoException e) {
			LOG.error(action+"操作失败！",e);
			throw new ServiceException(e);
		}
	}
	
	//---tools------------------------------------------------------------------------------------------------------------
	private <T extends ModuleFeatureBean> void setUUid(T modulefeatureBean) throws ServiceException {
		try {
			modulefeatureBean.getClass().getDeclaredField(ID_FIELD_NAME);
			try {
				BeanUtils.setProperty(modulefeatureBean, ID_FIELD_NAME, UUIDGenerator.getUUID());
			} catch (Exception e) {
				//有公有的设置方法
				LOG.error("无法给"+modulefeatureBean+"设置"+ID_FIELD_NAME+"值！",e);
				throw new ServiceException(e);
			} 
		} catch (Exception e) {
			LOG.error(modulefeatureBean+"中未发现"+ID_FIELD_NAME+"属性！",e);
			throw new ServiceException(e);
		} 
	}
	/**
	 * 判断id是否有值
	 * @param modulefeatureBean
	 * @throws ServiceException 
	 */
	public <T extends ModuleFeatureBean> boolean hasIdValue(T modulefeatureBean) throws ServiceException {
		try {
			modulefeatureBean.getClass().getDeclaredField(ID_FIELD_NAME);
			try {
				String idValue = BeanUtils.getProperty(modulefeatureBean, ID_FIELD_NAME);
				if(StrUtil.isNotBLank(idValue)) {
					return true;
				}else {
					return false;
				}
			} catch (Exception e) {
				//有公有的设置方法
				LOG.error("无法从"+modulefeatureBean+"获取"+ID_FIELD_NAME+"值！",e);
				throw new ServiceException(e);
			} 
		} catch (Exception e) {
			LOG.error(modulefeatureBean+"中未发现"+ID_FIELD_NAME+"属性！",e);
			throw new ServiceException(modulefeatureBean+"中未发现"+ID_FIELD_NAME+"属性！",e);
		} 
	}
	private <T extends ModuleFeatureBean> void setTs(T modulefeatureBean) throws ServiceException {
		try {
			modulefeatureBean.getClass().getDeclaredField(TIMESTAMP_FIELD_NAME);
			try {
				BeanUtils.setProperty(modulefeatureBean, TIMESTAMP_FIELD_NAME, new Date());
			} catch (Exception e) {
				//有公有的设置方法
				LOG.error("无法给"+modulefeatureBean+"设置"+TIMESTAMP_FIELD_NAME+"值！",e);
				throw new ServiceException(e);
			} 
		} catch (Exception e) {
			LOG.error(modulefeatureBean+"中未发现"+TIMESTAMP_FIELD_NAME+"属性！",e);
			throw new ServiceException(e);
		} 
	}
}
