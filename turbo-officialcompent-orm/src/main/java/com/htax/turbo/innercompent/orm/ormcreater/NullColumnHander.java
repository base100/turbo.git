package com.htax.turbo.innercompent.orm.ormcreater;

/**
 * 当前属性的值为空时的处理，需根据数据库情况进行定义，
 *  例如null值是否需要最终存为“Null”字符串
 *  @see DefaultNullColumnHander
 * @日期：2019-09-14下午11:19:17
 * @作者：joe
 */
public interface NullColumnHander {

	public Object warpNull(ColumnInfo nullColumn);
}
