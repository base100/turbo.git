package com.htax.turbo.innercompent.orm.util;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn.DB_TYPE_GROUP;
import com.htax.turbo.innercompent.orm.dao.Dao;
import com.htax.turbo.innercompent.orm.dao.DaoFactory;
import com.htax.turbo.innercompent.orm.jdbcpool.DataSourceProperty;

/**
 * 	反向生成代码
 * @author joe
 * 2019年10月8日 下午8:00:11
 */
public class ReverseUtil {

		@SuppressWarnings("serial")
		public static void main(String[] args) throws Exception {
			getReverseCode("jdbc:postgresql://localhost:5432/test","test","123456","t_user","id",new HashMap<String,String>() {{
				put("head_sculpture", "headSculpture");
				put("user_name", "userName");
			}});
		}
		
		/**
		 * 
		 * @param jdbcUrl 数据库连接
		 * @param userName 用户名
		 * @param pwd 密码
		 * @param tableName 要生成反向的表民
		 * @param idClumn id字段名称
		 * @param mappping  字段映射，如java 属性和数据库不一样，需添加映射，如果一样，无需添加
		 * @throws Exception
		 */
		public static void getReverseCode(String jdbcUrl,String userName,String pwd,String tableName,String idClumn,HashMap<String,String> mappping) throws  Exception {
			  
			//数据源参数
			DataSourceProperty dataSourceProperty = new DataSourceProperty();
			dataSourceProperty.setJdbcUrl(jdbcUrl);
			dataSourceProperty.setUsername(userName);
			dataSourceProperty.setPassword(pwd);
			dataSourceProperty.setInitialSize(1);
			dataSourceProperty.setMaxActive(1);
			
			Dao dmDao = DaoFactory.createDao("动态数据源数据源", dataSourceProperty);
			
			ResultSet rsColimns = dmDao.getDataSource().getConnection().getMetaData().getColumns(null, null, tableName, null);
			List<ClumnInfo> cs = new ArrayList<ClumnInfo>();
			
			while (rsColimns.next()) {
				String c_name = rsColimns.getString("COLUMN_NAME");
				String c_type_name = rsColimns.getString("TYPE_NAME");
				String c_length = rsColimns.getString("COLUMN_SIZE");
				String c_remarks = rsColimns.getString("REMARKS");
				cs.add(new ClumnInfo(c_name, c_type_name, c_remarks,c_name.equalsIgnoreCase(idClumn),mappping.get(c_name)));
				
				System.out.println("字段名：" + c_name + "------" + "类型："
				+ c_type_name + "------" + "长度：" + c_length
				+ "------" + "备注：" + c_remarks);
			}
			//反向生成代码
			StringBuffer rReverseCode = new StringBuffer();
			rReverseCode.append(createFiledStr(cs));
			//打印代码
			System.out.println("--反向代码内容---");
			System.out.println(rReverseCode);
			
		    dmDao.closeDao();
		}
		public static String createFiledStr(List<ClumnInfo> cs) {
			StringBuffer fieldStr = new StringBuffer();
			for (Iterator<ClumnInfo> iterator = cs.iterator(); iterator.hasNext();) {
				ClumnInfo clumnInfo = (ClumnInfo) iterator.next();
				fieldStr.append(createField(clumnInfo));
			}
			return fieldStr.toString();
		}
		public static String createField(ClumnInfo clumnInfo ) {
			StringBuffer field = new StringBuffer();
			field
			.append(createFieldRemarks(clumnInfo))
			.append(getAnnotation(clumnInfo))
			.append(getDefine(clumnInfo));
			return field.toString();
		}
		public static String createFieldRemarks(ClumnInfo clumnInfo) {
			StringBuffer rReverseCodeRemarks = new StringBuffer();
			rReverseCodeRemarks.append("/**").append("\r").append("* ").append(clumnInfo.remark).append("\r").append("*/").append("\r");
			return rReverseCodeRemarks.toString();
		}
		private static String getAnnotation(ClumnInfo clumnInfo) {
			StringBuffer fieldAnnotation = new StringBuffer();
			DB_TYPE_GROUP type_enum =  WsdColumn.DB_TYPE_GROUP.getTypeGroup(clumnInfo.type);
			
			if(type_enum.equals(DB_TYPE_GROUP.BLOB)&&clumnInfo.javaMapping==null) {
				fieldAnnotation.append("@WsdColumn(type = TYPE.BLOB)").append("\r");
			}else if(type_enum.equals(DB_TYPE_GROUP.BLOB)&&clumnInfo.javaMapping!=null) {
				fieldAnnotation.append("@WsdColumn(type = TYPE.BLOB, name = \""+clumnInfo.name+"\")").append("\r");
			}else if(!type_enum.equals(DB_TYPE_GROUP.BLOB)&&clumnInfo.javaMapping!=null) {
				fieldAnnotation.append("@WsdColumn(name = \""+clumnInfo.name+"\")").append("\r");
			}
			
			if(clumnInfo.isId) {
				fieldAnnotation.append("@WsdColumn(isId = true)").append("\r");
			}
			
			return fieldAnnotation.toString();
		}

		private static String getDefine(ClumnInfo clumnInfo) {
			StringBuffer define = new StringBuffer();
			define.append("private ").append(getType(clumnInfo.type)).append(" ")
			.append(clumnInfo.javaMapping==null?clumnInfo.name:clumnInfo.javaMapping).append(";")
			.append("\r");
			return define.toString();
		}
		private static String  getType(String type) {
			DB_TYPE_GROUP type_enum =  WsdColumn.DB_TYPE_GROUP.getTypeGroup(type);
			switch (type_enum) {
			case INT_NUMBER:
				return "Integer";
			case FLOAT_NUMBER:
				return "Double";
			case CLOB:
			case STR:
				return "String";
			case TIMESTAMP:
			case DATE:
				return "Date"; 
			case BLOB:
				return "byte[]"; 
			default:
				break;
			}
			return null;
		}
}
class ClumnInfo{
	public String name;
	public String type;
	public String remark;
	public boolean isId;
	public String javaMapping;
	public ClumnInfo(String name, String type, String remark,boolean isId,String javaMapping) {
		super();
		this.name = name;
		this.type = type;
		this.remark = remark;
		this.isId = isId;
		this.javaMapping =  javaMapping;
	}
}
	
