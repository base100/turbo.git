package com.htax.turbo.innercompent.orm.pager;

import java.util.LinkedHashMap;
/**
 * 内存分页
 * @author joe
 * 2019-5-25 下午3:40:23
 */
@SuppressWarnings("rawtypes")
public class MemoryPagerData  {
	
	
	private LinkedHashMap datas;
	private int total;
	
	
	public LinkedHashMap getDatas() {
		return datas;
	}
	public void setDatas(LinkedHashMap datas) {
		this.datas = datas;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}

}
