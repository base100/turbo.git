package com.htax.turbo.innercompent.orm.dao.impl;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.htax.turbo.innercompent.orm.adapter.DbTypeEnum;
import com.htax.turbo.innercompent.orm.dao.Dao;
import com.htax.turbo.innercompent.orm.jdbcpool.DataSourceProperty;
import com.htax.turbo.innercompent.orm.jdbcpool.DbcpPoolHandler;
import com.htax.turbo.innercompent.orm.ormcreater.DefaultDmlCreateor;
import com.htax.turbo.innercompent.orm.util.DbUtil;


/**
 * dbcp 连接池类型的数据操作类   非线程安全的
 * @日期：2019-12-14下午11:21:26
 * @作者：joe
 *  缺少部分功能：存储过程调用等，按后期需求添加
 */
public  class DbcpPoolDao extends Dao{
	
	private Logger logger = Logger.getLogger(DbcpPoolDao.class);
	
	public void initDao(DataSourceProperty dataSourceProperty){
		DataSource dataSource = DbcpPoolHandler.createDataSource(dataSourceProperty);
		setJdbcTemplate(new JdbcTemplate(dataSource));
		setLobHandler(DbcpPoolHandler.createLobHandler(dataSourceProperty));
		setTransactionManager(new DataSourceTransactionManager(dataSource));
		setModuleDmlCreator(new DefaultDmlCreateor());
		
		logger.info("init :" + getJdbcTemplate() + "//" + getLobHandler() + "//" + getTransactionManager() + "//"
				+ getModuleDmlCreator());
	}
    
	public void closeDao() {
		DataSource dataSource = getJdbcTemplate().getDataSource();
		DbcpPoolHandler.closeDataSource(dataSource);
	}

	@Override
	public DbTypeEnum getDbType() {
		DataSource dataSource = getJdbcTemplate().getDataSource();
		BasicDataSource basicDataSource = (BasicDataSource)dataSource; 
		String connectUrl = basicDataSource.getUrl();
		return DbUtil.getDbType(connectUrl);
	}
}
