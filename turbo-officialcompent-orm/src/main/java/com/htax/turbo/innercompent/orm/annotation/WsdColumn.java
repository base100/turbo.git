package com.htax.turbo.innercompent.orm.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;

/**
 * 标示数据库字段
 * @日期：2019-09-14下午11:04:47
 * @作者：joe
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WsdColumn {
	/**
	 * 字段名
	 * @return
	 */
	String name() default "";
	/**
	 * 如果使用@see _DEFAULT值，将会使用Field的类型
	 * @return
	 */
	TYPE type() default TYPE._DEFAULT;	
	boolean isId() default false;
	
	boolean UUID() default false;
	ID_TYPE idType() default ID_TYPE.BY_CODE;
	enum ID_TYPE{
		BY_CODE,//代码设置，如uuid。在新增的时候，如果id为空，解析为null。
		BY_DB;//id由数据库设置，如自增id。在新增的时候，如果id为空，则不解析为null。
	}
    enum TYPE{
    	_DEFAULT,
    	NUMBER(Integer.class,Long.class,Double.class,int.class,double.class,long.class),
    	TIMESTEMP(Date.class),
    	VARCHAR2(String.class),
    	BLOB(byte[].class),
    	CLOB(String.class);
    	
    	private TYPE(){
    		this.equalClazz = new Class<?>[]{};
    	}
    	private Class<?>[] equalClazz;
    	private TYPE(Class<?>... equalClazz){
    		this.equalClazz = equalClazz;
    	}
    	public Class<?>[] getequalClazz(){
    		return equalClazz;
    	}
    }
    /**
         * 数据库字段分组
     * @author joe
     * @2014-11-21 @下午7:53:24
     */
    public static enum DB_TYPE_GROUP{
    	
    	INT_NUMBER("int","tinyint","bigint","int2","int4","int8"),
    	FLOAT_NUMBER("number","decimal","float","double","numeric"),
    	NUMBER("number","int","tinyint","decimal","float","double","bigint","numeric","int2","int4","int8"),
    	TIMESTAMP("timestamp","timestamp with local time zone","timestamp with time zone"),
    	DATE("date","datetime","time"),
    	STR("varchar2","varchar","char","nvarchar2","nvarchar","bpchar"),
    	BLOB("blob","bytea"),
    	CLOB("clob","ntext","text","longtext");
    	private String[] dbType;
    	DB_TYPE_GROUP(String... dbType){
    		this.dbType = dbType;
    	}
		public String[] getDbType() {
			return dbType;
		}
		/*
		 * 是否可以是时间戳字段
		 */
		public static boolean isDateColumn(String columnType){
			DB_TYPE_GROUP[] groups = {TIMESTAMP,DATE};
			for (int i = 0; i < groups.length; i++) {
				String[] dbTypes =  groups[i].dbType;
		    	for (int j = 0; j < dbTypes.length; j++) {
					if(columnType.toLowerCase().startsWith(dbTypes[j])){
						return true;
					}
				}
			}
			return false;
		}
		public static boolean isBigColumn(String columnType){
			DB_TYPE_GROUP[] groups = {BLOB,CLOB};
			for (int i = 0; i < groups.length; i++) {
				String[] dbTypes =  groups[i].dbType;
		    	for (int j = 0; j < dbTypes.length; j++) {
					if(columnType.toLowerCase().startsWith(dbTypes[j])){
						return true;
					}
				}
			}
			return false;
		}
		public static DB_TYPE_GROUP getTypeGroup(String dbTypeArg){
			DB_TYPE_GROUP[] groups = DB_TYPE_GROUP.values();
		    for (int i = 0; i < groups.length; i++) {
		    	 String[] dbTypes =  groups[i].dbType;
		    	 for (int j = 0; j < dbTypes.length; j++) {
					if(dbTypeArg.toLowerCase().startsWith(dbTypes[j])){
						return groups[i];
					}
				}
			}
		    return null;
		}
    }
}