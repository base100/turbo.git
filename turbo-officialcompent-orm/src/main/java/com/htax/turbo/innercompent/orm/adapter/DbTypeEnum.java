package com.htax.turbo.innercompent.orm.adapter;
/**
 * 数据库类型
 * @author joe
 * 2019-5-26 上午11:10:18
 */
public enum DbTypeEnum {
	
	ORACLE("1","oracle","oracle.jdbc.driver.OracleDriver","select 1 from dual"),
	MYSQL("2","mySql","com.mysql.cj.jdbc.Driver","select 1 "),
	SQLSERVER("3","sql server","com.microsoft.sqlserver.jdbc.SQLServerDriver","select 1 "),
    POSTGRESQL("4","postgresql","org.postgresql.Driver","select 1 "),
    DM("5","dm","dm.jdbc.driver.DmDriver","select 1 "),
    ACCESS("6","access","net.ucanaccess.jdbc.UcanaccessDriver","Select 1  from sys.MSysObjects ");
	private String code;
	private String name;
	private String driverClassName;
	private String validataSql;
	
	private DbTypeEnum(String code,String nameToDisPlay,String driverClassName,String validataSql){
		this.code = code;
		this.name = nameToDisPlay;
		this.driverClassName = driverClassName;
		this.validataSql = validataSql;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getValidataSql() {
		return validataSql;
	}

	public void setValidataSql(String validataSql) {
		this.validataSql = validataSql;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
