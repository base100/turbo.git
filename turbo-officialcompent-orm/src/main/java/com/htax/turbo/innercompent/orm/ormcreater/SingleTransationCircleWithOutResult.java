package com.htax.turbo.innercompent.orm.ormcreater;
/**
 * 单事务操作类，无返回值
 * @日期：2019-09-14下午11:22:30
 * @作者：joe
 */
public interface SingleTransationCircleWithOutResult {

	public void actionInCircle() throws RuntimeException;
}
