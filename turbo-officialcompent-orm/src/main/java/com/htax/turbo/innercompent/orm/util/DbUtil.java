package com.htax.turbo.innercompent.orm.util;

import org.springframework.util.StringUtils;

import com.htax.turbo.innercompent.base.para.CorePara.CoreInitCtx;
import com.htax.turbo.innercompent.orm.adapter.DbTypeEnum;
import com.htax.turbo.innercompent.orm.tablesharding.TableShardingParser;

/**
 * 数据库util
 * @author joe
 * 2019-5-26 上午11:22:16
 */
public class DbUtil {

	/**
	 * 从拦截字符串中查看是否是mysql
	 * @param connectUrl
	 * @return
	 */
	public static boolean isMysqlFromConnectUrl(String connectUrl){
		if(connectUrl!=null&&!("".equals(connectUrl))){
			if(connectUrl.toLowerCase().indexOf("mysql")!=-1){
				return true;
			}
		}
		return false;
	}
	/**
	 * 从拦截字符串中查看是否是oracle
	 * @param connectUrl
	 * @return
	 */
	public static boolean isOracleFromConnectUrl(String connectUrl){
		if(connectUrl!=null&&!("".equals(connectUrl))){
			if(connectUrl.toLowerCase().indexOf("oracle")!=-1){
				return true;
			}
		}
		return false;
	}
	/**
	 * 从拦截字符串中查看是否是sqlserver
	 * @param connectUrl
	 * @return
	 */
	public static boolean isSqlServerFromConnectUrl(String connectUrl){
		if(connectUrl!=null&&!("".equals(connectUrl))){
			if(connectUrl.toLowerCase().indexOf("sqlserver")!=-1){
				return true;
			}
		}
		return false;
	}
	/**
     * 从拦截字符串中查看是否是sqlserver
     * @param connectUrl
     * @return
     */
    public static boolean isPostgresqlFromConnectUrl(String connectUrl){
        if(connectUrl!=null&&!("".equals(connectUrl))){
            if(connectUrl.toLowerCase().indexOf("postgresql")!=-1){
                return true;
            }
        }
        return false;
    }
    /**
     * 从拦截字符串中查看是否是ACCESS
     * @param connectUrl
     * @return
     */
    public static boolean isAccessFromConnectUrl(String connectUrl){
        if(connectUrl!=null&&!("".equals(connectUrl))){
            if(connectUrl.toLowerCase().indexOf("access")!=-1){
                return true;
            }
        }
        return false;
    }
    public static DbTypeEnum getDbType(String  connectUrl) {
		if(isOracleFromConnectUrl(connectUrl)){
			return DbTypeEnum.ORACLE;
		}else if(isMysqlFromConnectUrl(connectUrl)){
			return DbTypeEnum.MYSQL;
		}else if(isSqlServerFromConnectUrl(connectUrl)){
			return DbTypeEnum.SQLSERVER;
		}else if(isPostgresqlFromConnectUrl(connectUrl)){
          return DbTypeEnum.POSTGRESQL;
		}else if(isAccessFromConnectUrl(connectUrl)){
	          return DbTypeEnum.ACCESS;
		}else{
			throw new RuntimeException("不支持的数据库类型...");
		}
	}
    public static String getValidateUrlByConnectUrl(String connectUrl) {
		return getDbType(connectUrl).getValidataSql();
	}
    /**
	 * 
	 * @return 
	 */
	public static String WSD_TABLE(String tableName){
		return CoreInitCtx.DEFAULT_SCHEMA_NAME+"."+tableName;
	}
	/**
	 * 
	 * @return 
	 */
	public static String WSD_TABLE(String scheamName,String tableName){
		if(!StringUtils.hasText(scheamName)){
			return WSD_TABLE(tableName);
		}
		return scheamName+"."+tableName;
	}
	/**
	 * DML(insert update delete)表拆分的支持 目前只支持表后缀为_数字的，拆分
	 * @param rawTableName
	 * @param id
	 * @return
	 */
	public static String  SHARDING_TABLE_DML(String rawTableName,String id){
		if(TableShardingParser.tableIsSharding(rawTableName)){
			return TableShardingParser.SHARDING_TABLE_DML(rawTableName, id);
		}else{
			throw new RuntimeException("未对当前表"+rawTableName+"配置水平拆分，请检查先关配置文件");
		}
		
	}
	/**
	 * 查询，当没有给定id时，为全表查询
	 * @param rawTableName
	 * @param id
	 * @returns
	 */
	public static String SHARDING_TABLE_SEL(String rawTableName,String id){
		if(TableShardingParser.tableIsSharding(rawTableName)){
			return TableShardingParser.SHARDING_TABLE_SEL(rawTableName, id);
		}else{
			throw new RuntimeException("未对当前表"+rawTableName+"配置水平拆分，请检查相关配置文件");
		}
	}
	public static void main(String[] args) {
		System.out.println(isMysqlFromConnectUrl("jdbc:mysql://localhost:3306/birs?useUnicode=true&characterEncoding=UTF-8"));
		System.out.println(isOracleFromConnectUrl("jdbc:oracle:thin:@localhost:1521"));
		System.out.println(isSqlServerFromConnectUrl("jdbc:microsoft:sqlserver://localhost:1433;DatabaseName"));
	}
}
