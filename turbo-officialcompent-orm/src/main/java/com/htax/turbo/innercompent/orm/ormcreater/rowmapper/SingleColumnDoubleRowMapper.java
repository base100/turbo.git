package com.htax.turbo.innercompent.orm.ormcreater.rowmapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
/**
 * 单个返回值 Double处理
 * @author joe
 * 2019-5-3 下午5:22:39
 */
public class SingleColumnDoubleRowMapper implements RowMapper<Double>{
	public Double mapRow(ResultSet rss, int rowNum) throws SQLException {
		BigDecimal decimal = rss.getBigDecimal(1);
		return decimal==null?null:decimal.doubleValue();
	}
}
