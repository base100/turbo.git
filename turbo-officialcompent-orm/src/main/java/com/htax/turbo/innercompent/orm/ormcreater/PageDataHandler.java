package com.htax.turbo.innercompent.orm.ormcreater;

import java.util.List;
/**
 * 处理分页数据
 * @author joe
 * @time  2019-2-1 下午3:54:46
 */
public interface PageDataHandler<T> {

	public void handerPagerData(List<T> datas);
}
