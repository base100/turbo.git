package com.htax.turbo.innercompent.orm;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.orm.dao.Dao;
import com.htax.turbo.innercompent.orm.dao.DaoFactory;
import com.htax.turbo.innercompent.orm.jdbcpool.DataSourceProperty;
import com.htax.turbo.innercompent.orm.ormcreater.TimeStampValue;
import com.htax.turbo.innercompent.orm.util.DbCollectionUtil;

/**
 *   测试无spring上下文情况使用dao操作
 * @author joe
 * 2019-5-11 下午3:56:33
 */
public class RuntimeDaoTest {

	public static void main(String[] args) throws Exception {
		testNoSpring();
	}
	public static void testNoSpring() throws DaoException, ParseException {
	  
	    //数据源参数
		DataSourceProperty dataSourceProperty = new DataSourceProperty();
		dataSourceProperty.setJdbcUrl("jdbc:postgresql://localhost:5432/test");
		dataSourceProperty.setUsername("test");
		dataSourceProperty.setPassword("123456");
		dataSourceProperty.setInitialSize(1);
		dataSourceProperty.setMaxActive(1);
		
		Dao dmDao = DaoFactory.createDao("动态数据源数据源", dataSourceProperty);
		//new DateValue(value, dao)
		List<Map<String, Object>> users = dmDao.listMap("查询", "select *  from t_user where ts<?", new Object[] {new TimeStampValue("2019-10-08 19:53:43")});
	    DbCollectionUtil.printCollection(users);
	    dmDao.closeDao();
	}
}
