package com.htax.turbo.innercompent.orm;

import com.htax.turbo.innercompent.orm.annotation.WsdColumn;

public abstract class BaseVo {

  @WsdColumn(name="c_name")
  private String name;
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  
}
