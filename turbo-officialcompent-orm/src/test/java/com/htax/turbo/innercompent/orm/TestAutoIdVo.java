package com.htax.turbo.innercompent.orm;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn.ID_TYPE;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;

@WsdTable(name = "test_auto")
public class TestAutoIdVo extends ModuleFeatureBean{

	@WsdColumn(isId = true,idType = ID_TYPE.BY_DB)
	private Integer Id;
	private String name;
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
