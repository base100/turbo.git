package com.htax.turbo.innercompent.orm;

import java.util.HashMap;

import com.htax.turbo.innercompent.orm.util.ReverseUtil;

/**
 * 	反向生成代码
 * @author joe
 * 2019年10月8日 下午8:00:11
 */
public class ReverseTest {

		@SuppressWarnings("serial")
		public static void main(String[] args) throws Exception {
			ReverseUtil.getReverseCode("jdbc:postgresql://localhost:5432/test","test","123456","t_user","id",new HashMap<String,String>() {{
				put("head_sculpture", "headSculpture");
				put("user_name", "userName");
			}});
		}
}
	
