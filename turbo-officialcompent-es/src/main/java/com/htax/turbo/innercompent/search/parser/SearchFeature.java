package com.htax.turbo.innercompent.search.parser;

import java.util.List;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;

public class SearchFeature extends ModuleFeatureBean{

	@WsdNotDbColumn
	private List<HighlightItem> highlightItems;
	@WsdNotDbColumn
	private float _score;
	@WsdNotDbColumn
	private String indexName;

	public List<HighlightItem> getHighlightItems() {
		return highlightItems;
	}

	public void setHighlightItems(List<HighlightItem> highlightItems) {
		this.highlightItems = highlightItems;
	}

	public float get_score() {
		return _score;
	}

	public void set_score(float _score) {
		this._score = _score;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	
	
}
