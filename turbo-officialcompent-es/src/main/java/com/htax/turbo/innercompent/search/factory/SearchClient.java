package com.htax.turbo.innercompent.search.factory;

import java.io.IOException;
import java.util.List;

import com.htax.core.tools.util.api.ResultVo;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.search.impl.SearchHitRowWapper;
import com.htax.turbo.innercompent.search.impl.SearchPager;
import com.htax.turbo.innercompent.search.parser.IndexMappingFieldVo;
import com.htax.turbo.innercompent.search.parser.SearchFeature;

public interface SearchClient {

	void testConnect() throws ServiceException;

	ResultVo<Object> deleteIndex(String indexName);

	boolean existsIndex(String index) throws IOException;

	ResultVo<Object> updataIndex(String indexName, List<IndexMappingFieldVo> propertiesList);

	ResultVo<Object> createIndex(String indexName, List<IndexMappingFieldVo> propertiesList);

	ResultVo<Object> createOrUpdateIndex(String indexName, List<IndexMappingFieldVo> propertiesList,
			boolean existsIndexUpdate);

	ResultVo<Object> createOrUpdateIndexByBean(Class<?> clazz, boolean existsIndexUpdate);

	ResultVo<Object> updateDoc(String index, String id, Object obj) throws IOException;

	ResultVo<Object> delete(String index, String id) throws IOException;

	ResultVo<Object> bulkDeleteDoc(String indexName, List<String> ids);

	ResultVo<Object> bulkUpdateDoc(String indexName, List<Object> objs, String idFileName);

	ResultVo<Object> bulkAddDoc(String indexName, List<?> objs,String[] includeFields, String idFieldName);

	ResultVo<Object> bulkAddDocByBean(List<?> objs);

	ResultVo<Object> deleteDoc(String indexName, String id);

	ResultVo<Object> updateDoc(String indexName, Object obj, String id);

	ResultVo<Object> addDoc(String indexName, Object obj, String id);

	long multiMatchSearchTotal(String[] indexNames, String[] fields, String queryStr);

	public <T> SearchPager<T> pagerMultiMatchSearch(String[] indexNames, String[] fields, int page, int size, String queryStr,
			SearchHitRowWapper<T> hitRowWapper) ;
	
	public <T  extends SearchFeature> SearchPager<T> pagerMultiMatchSearchOfBean(Class<T> clazz, int page, int size, String queryStr);
}
