package com.htax.turbo.innercompent.search.util;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.util.ReflectionUtils;

public class SearchUtil extends BeanUtilsBean {

	public static Map<String, Object> beanToMap(Object bean) throws Exception {
		if (bean == null) {
			return Collections.emptyMap();
		}
		Map<String, Object> map = new HashMap<String, Object>();

		Field[] moduleFs = bean.getClass().getDeclaredFields();
		for (int i = 0; i < moduleFs.length; i++) {
			Field f = moduleFs[i];
			map.put(f.getName(), f.get(bean));
		}
		return map;
	}

	public static Map<String, Object> beanToMap(Object bean, String[] inculdes) throws Exception {
		if (bean == null) {
			return Collections.emptyMap();
		}
		Map<String, Object> map = new HashMap<String, Object>();

		List<String> ls = (inculdes != null && inculdes.length != 0) ? Arrays.asList(inculdes) : null;
		Field[] moduleFs = bean.getClass().getDeclaredFields();
		for (int i = 0; i < moduleFs.length; i++) {
			Field f = moduleFs[i];
			if (ls != null && !ls.contains(f.getName())) {
				continue;
			}
			ReflectionUtils.makeAccessible(f);
			map.put(f.getName(), ReflectionUtils.getField(f, bean));
		}
		return map;
	}
}
