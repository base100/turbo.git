package com.htax.turbo.innercompent.search.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.PutMappingRequest;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;

import com.htax.core.tools.util.CollectionUtil;
import com.htax.core.tools.util.JsonUtil;
import com.htax.core.tools.util.StrUtil;
import com.htax.core.tools.util.api.ResultVo;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.FiledType;
import com.htax.turbo.innercompent.search.exp.SearchException;
import com.htax.turbo.innercompent.search.factory.SearchClient;
import com.htax.turbo.innercompent.search.parser.HighlightItem;
import com.htax.turbo.innercompent.search.parser.IndexInfo;
import com.htax.turbo.innercompent.search.parser.IndexMappingFieldVo;
import com.htax.turbo.innercompent.search.parser.IndexParser;
import com.htax.turbo.innercompent.search.parser.SearchFeature;
import com.htax.turbo.innercompent.search.util.SearchUtil;
/**
 * es 实现
 * @author eric
 *
 */
public class EsSearchClientImpl implements SearchClient {

	Logger logger = Logger.getLogger(EsSearchClientImpl.class);

	private String searchServiceIp;
	private int searchServicePort;

	private RestHighLevelClient restHighLevelClient;

	private static Object lock = new Object();

	public EsSearchClientImpl(String searchServiceIp, int searchServicePort) {
		this.searchServiceIp = searchServiceIp;
		this.searchServicePort = searchServicePort;
	}

	public RestHighLevelClient getClient() {
		synchronized (lock) {
			restHighLevelClient = new RestHighLevelClient(
					RestClient.builder(new HttpHost(searchServiceIp, searchServicePort, "http")));
			return restHighLevelClient;
		}
	}

	@Override
	public void testConnect() throws ServiceException {
		CountRequest countRequest = new CountRequest();
		try {
			CountResponse countResponse = getClient().count(countRequest, RequestOptions.DEFAULT);
			long countDoc = countResponse.getCount();
			logger.info("全文检索连接成功，共" + countDoc + "个文档...");
		} catch (IOException e) {
			throw new ServiceException("全文检索连接失败", e);
		}
	}

	// --search
	/**
	 * 分页查询
	 * 
	 * @param indexNames
	 * @param fields
	 * @param page
	 * @param size
	 * @param queryStr
	 * @param hitRowWapper
	 * @return
	 */
	@Override
	public <T extends SearchFeature> SearchPager<T> pagerMultiMatchSearchOfBean(Class<T> clazz, int page, int size, String queryStr) {
		IndexInfo indexInfo = IndexParser.getIndex(clazz);
		String[] fields = IndexParser.getIndexNoStoredStringFieldsName(clazz).toArray(new String[0]);
		return pagerMultiMatchSearch(new String[] {indexInfo.getIndexName()}, fields, page, size, queryStr, new SearchHitRowWapper<T>() {
			@Override
			public T wapper(SearchHit hit) {
				
				String json = hit.getSourceAsString();
				T t =  JsonUtil.toObject(json,clazz);
				Map<String, HighlightField> hl = hit.getHighlightFields();
				
				if(hl.size()!=0) {
					List<HighlightItem> hs = new ArrayList<HighlightItem>();
					for (Iterator<HighlightField> iterator = hl.values().iterator(); iterator.hasNext();) {
						HighlightField h = (HighlightField) iterator.next();
						HighlightItem hitem = new HighlightItem();
						hitem.setFiledName(h.getName());
						Text[] texts = h.fragments();
						List<String> htexts = new ArrayList<String>();
						for (int i = 0; i < texts.length; i++) {
							Text text = texts[i];
							htexts.add(text.string());
						}
						hitem.setTexts(htexts);
						hs.add(hitem);
					}
					t.setHighlightItems(hs);
					t.set_score(hit.getScore());
					t.setIndexName(indexInfo.getIndexName());
				}
				return t;
			}
		});
	}
	/**
	 * 分页查询
	 * 
	 * @param indexNames
	 * @param fields
	 * @param page
	 * @param size
	 * @param queryStr
	 * @param hitRowWapper
	 * @return
	 */
	@Override
	public <T> SearchPager<T> pagerMultiMatchSearch(String[] indexNames, String[] fields, int page, int size, String queryStr,
			SearchHitRowWapper<T> hitRowWapper) {
		SearchPager<T> resultVo = new SearchPager<T>();
		if (StrUtil.isBlank(queryStr)) {
			String errorMsg = "查询条件不能为空！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg);
			logger.error(errorMsg);
			return resultVo;
		}

		SearchRequest searchRequest = new SearchRequest(indexNames);

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		// BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

		MultiMatchQueryBuilder multiMatchQueryBuilder = new MultiMatchQueryBuilder(queryStr, fields);

		// 高亮设置
		searchSourceBuilder.highlighter(getCommonHighlightBuilder());

		// 分页
		if (page <= 0) {
			page = 1;
		}
		if (size <= 0) {
			size = 20;
		}
		int start = (page - 1) * size;
		searchSourceBuilder.from(start);
		searchSourceBuilder.size(size);

		/*
		 * //置匹配占比 multiMatchQueryBuilder.minimumShouldMatch("70%"); //提升另个字段的Boost值
		 * multiMatchQueryBuilder.field("name",10);
		 */

		// boolQueryBuilder.must(multiMatchQueryBuilder);
		searchSourceBuilder.query(multiMatchQueryBuilder);
		searchRequest.source(searchSourceBuilder);
		logger.info("检索字符串："+searchRequest.source().toString());
		try {
			SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
			// 结果集处理
			SearchHits hits = searchResponse.getHits();
			SearchHit[] searchHits = hits.getHits();
			// 记录总数
			long totalHits = hits.getTotalHits().value;
			resultVo.setTotal(totalHits);
			if (totalHits != 0 && size != 0) {// size=0时只查总数，不返回数据
				List<T> objects = new ArrayList<T>();
				for (SearchHit hit : searchHits) {
					objects.add(hitRowWapper.wapper(hit));
				}
				resultVo.setDatas(objects);
			}
		} catch (Exception e) {
			String errorMsg = "查询【" + queryStr + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	private HighlightBuilder getCommonHighlightBuilder() {
		HighlightBuilder highlightBuilder = new HighlightBuilder();
		highlightBuilder.preTags("<font class='eslight'>");
		highlightBuilder.postTags("</font>");
		highlightBuilder.field("*");
		return highlightBuilder;
	}

	/**
	 * 查总数
	 * 
	 * @param indexNames
	 * @param fields
	 * @param queryStr
	 * @return
	 */
	@Override
	public long multiMatchSearchTotal(String[] indexNames, String[] fields, String queryStr) {
		SearchPager<Object> searchPager = pagerMultiMatchSearch(indexNames, fields, 0, 0, queryStr, null);
		return searchPager.getTotal();
	}

	// --doc
	@Override
	public ResultVo<Object> addDoc(String indexName, Object obj, String id) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			IndexRequest docRequest = new IndexRequest("test").id(id);
			String source = JsonUtil.toJson(obj);
			docRequest.source(source, XContentType.JSON);
			restHighLevelClient.index(docRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			String errorMsg = "索引文档【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	@Override
	public ResultVo<Object> updateDoc(String indexName, Object obj, String id) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			UpdateRequest updateRequest = new UpdateRequest(indexName, id);
			String source = JsonUtil.toJson(obj);
			updateRequest.doc(source, XContentType.JSON);
			restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			String errorMsg = "修改文档【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	@Override
	public ResultVo<Object> deleteDoc(String indexName, String id) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			DeleteRequest deleteRequest = new DeleteRequest(indexName, id);
			restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			String errorMsg = "删除文档【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	@Override
	public ResultVo<Object> bulkAddDocByBean(List<?> objs) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		String msg = "";
		if (CollectionUtil.hasElement(objs)) {
			Class<?> clazz = objs.get(0).getClass();
			IndexInfo indexInfo = IndexParser.getIndex(clazz);
			if (indexInfo == null) {
				msg = "未找到【" + clazz + "】对象索引解析信息，无法建立索引!";
				resultVo.setMsg(msg);
				logger.error(msg);
			} else {
				String idFieldName = IndexParser.getIdFieldName(clazz);
				String[] mappingFieldsName = IndexParser.getIndexField(clazz).keySet().toArray(new String[0]);
				resultVo = bulkAddDoc(indexInfo.getIndexName(), objs, mappingFieldsName,idFieldName);
			}
		}
		return resultVo;
	}

	/**
	 * 批量索引文档
	 * 
	 * @param indexName
	 * @param objs
	 * @param idFileName
	 * @return
	 */
	@Override
	public ResultVo<Object> bulkAddDoc(String indexName, List<?> objs,String[] includeFields, String idFieldName) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			BulkRequest bulkAddRequest = new BulkRequest();
			for (int i = 0; i < objs.size(); i++) {
				Object obj = objs.get(i);
				Map<String,Object> datas = SearchUtil.beanToMap(obj, includeFields);
				
				IndexRequest indexRequest = new IndexRequest(indexName);

				if (StrUtil.isNotBlank(idFieldName)) {
					String id = getIdValue(obj, idFieldName);
					if (id != null) {
						indexRequest.id(id);
					}
				}
				indexRequest.source(datas);
				bulkAddRequest.add(indexRequest);
			}
			BulkResponse bulkAddResponse = restHighLevelClient.bulk(bulkAddRequest, RequestOptions.DEFAULT);
			if (bulkAddResponse.hasFailures()) {
				resultVo.setSuccess(false);
				resultVo.setMsg(bulkAddResponse.buildFailureMessage());
			}else {
				resultVo.setSuccess(true);
			}
		} catch (Exception e) {
			String errorMsg = "批量索引文档【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	/**
	 * 批量修改文档
	 * 
	 * @param indexName
	 * @param objs
	 * @param idFileName
	 * @return
	 */
	@Override
	public ResultVo<Object> bulkUpdateDoc(String indexName, List<Object> objs, String idFileName) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			BulkRequest bulkUpdateRequest = new BulkRequest();
			for (int i = 0; i < objs.size(); i++) {
				Object obj = objs.get(i);
				String source = JsonUtil.toJson(obj);
				String id = getIdValue(obj, idFileName);
				if (id == null) {
					throw new SearchException("id不能为空");
				}
				UpdateRequest updateRequest = new UpdateRequest(indexName, id);

				updateRequest.doc(source, XContentType.JSON);
				bulkUpdateRequest.add(updateRequest);
			}
			BulkResponse bulkAddResponse = restHighLevelClient.bulk(bulkUpdateRequest, RequestOptions.DEFAULT);
			if (bulkAddResponse.hasFailures()) {
				resultVo.setSuccess(false);
				resultVo.setMsg(bulkAddResponse.buildFailureMessage());
			}
		} catch (Exception e) {
			String errorMsg = "批量索引文档【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	/**
	 * 批量修改文档
	 * 
	 * @param indexName
	 * @param objs
	 * @param idFileName
	 * @return
	 */
	@Override
	public ResultVo<Object> bulkDeleteDoc(String indexName, List<String> ids) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			BulkRequest bulkDeleteRequest = new BulkRequest();
			for (int i = 0; i < ids.size(); i++) {
				String id = ids.get(i);
				if (StrUtil.isBlank(id)) {
					throw new SearchException("id不能为空");
				}
				DeleteRequest deleteRequest = new DeleteRequest(indexName, id);

				bulkDeleteRequest.add(deleteRequest);
			}
			BulkResponse bulkAddResponse = restHighLevelClient.bulk(bulkDeleteRequest, RequestOptions.DEFAULT);
			if (bulkAddResponse.hasFailures()) {
				resultVo.setSuccess(false);
				resultVo.setMsg(bulkAddResponse.buildFailureMessage());
			}
		} catch (Exception e) {
			String errorMsg = "批量索引文档【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	/**
	 * 更新记录信息
	 */
	@Override
	public ResultVo<Object> updateDoc(String index, String id, Object obj) throws IOException {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			UpdateRequest updateRequest = new UpdateRequest(index, id);
			String source = JsonUtil.toJson(obj);
			updateRequest.doc(source, XContentType.JSON);
			restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			String errorMsg = "更新索引文档【" + index + "】by id【" + id + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;

	}

	/**
	 * 删除记录
	 */
	@Override
	public ResultVo<Object> delete(String index, String id) throws IOException {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			DeleteRequest deleteRequest = new DeleteRequest(index, id);
			restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
		} catch (Exception e) {
			String errorMsg = "更新索引文档【" + index + "】by id【" + id + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	// ---index---
	@Override
	public ResultVo<Object> createOrUpdateIndexByBean(Class<?> clazz, boolean existsIndexUpdate) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		String msg = "";
		IndexInfo indexInfo = IndexParser.getIndex(clazz);
		if (indexInfo == null) {
			msg = "未找到【"+clazz+"】对象索引解析信息，无法建立索引!";
			resultVo.setMsg(msg);
			logger.error(msg);
		} else {
			Map<String, IndexMappingFieldVo> fields = IndexParser.getIndexField(clazz);
			if (fields == null || fields.isEmpty()) {
				msg = "未找到【"+clazz+"】对象字段解析信息，无法建立索引!";
				resultVo.setMsg(msg);
				logger.error(msg);
			} else {
				List<IndexMappingFieldVo> fieldVos = IndexParser.getIndexNoStoredFieldsBean(clazz);
				return createOrUpdateIndex(indexInfo.getIndexName(), fieldVos, existsIndexUpdate);
			}
		}
		return resultVo;
	}

	@Override
	public ResultVo<Object> createOrUpdateIndex(String indexName, List<IndexMappingFieldVo> propertiesList,
			boolean existsIndexUpdate) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			if (existsIndex(indexName)) {
				String msg = "索引【" + indexName + "】已经存在！";
				logger.warn(msg);
				if (existsIndexUpdate) {
					msg = "更新索引【" + indexName + "】！";
					logger.info(msg);
					return updataIndex(indexName, propertiesList);
				} else {
					resultVo.setSuccess(false);
					resultVo.setMsg(msg);
				}
			} else {
				return createIndex(indexName, propertiesList);
			}
		} catch (Exception e) {
			String errorMsg = "创建索引【" + indexName + "】with mapping失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	@Override
	public ResultVo<Object> createIndex(String indexName, List<IndexMappingFieldVo> propertiesList) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			CreateIndexRequest createIndexRequest = new CreateIndexRequest(indexName);
			XContentBuilder builder = XContentFactory.jsonBuilder();
			builder.startObject().field("dynamic","false")//不动态更新mapping
			.startObject("properties");
			// 属性
			if (propertiesList != null && !propertiesList.isEmpty()) {
				for (Iterator<IndexMappingFieldVo> iterator = propertiesList.iterator(); iterator.hasNext();) {
					IndexMappingFieldVo indexMappingFieldVo = (IndexMappingFieldVo) iterator.next();
					builder.startObject(indexMappingFieldVo.getFieldName()).field("type",
							indexMappingFieldVo.getFiledType().type);
					// 分词
					if (indexMappingFieldVo.getFiledType() == FiledType.Text) {
						builder.field("analyzer", indexMappingFieldVo.getAnalyzer().type).field("search_analyzer",
								indexMappingFieldVo.getSearchAnalyzer().type);
					}
					builder.endObject();
				}
			}

			builder.endObject()// properties
					.endObject();// root

			createIndexRequest.mapping(builder);
			createIndexRequest.setTimeout(TimeValue.timeValueMinutes(1));
			createIndexRequest.setMasterTimeout(TimeValue.timeValueMinutes(1));
			CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(createIndexRequest,
					RequestOptions.DEFAULT);
			if (createIndexResponse.isAcknowledged()) {
				resultVo.setSuccess(true);
			} else {
				resultVo.setSuccess(false);
			}
		} catch (Exception e) {
			String errorMsg = "创建索引【" + indexName + "】with mapping失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	@Override
	public ResultVo<Object> updataIndex(String indexName, List<IndexMappingFieldVo> propertiesList) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		try {
			PutMappingRequest putMappingRequest = new PutMappingRequest(indexName);
			XContentBuilder builder = XContentFactory.jsonBuilder();
			builder.startObject().startObject("properties");
			// 属性
			if (propertiesList != null && !propertiesList.isEmpty()) {
				for (Iterator<IndexMappingFieldVo> iterator = propertiesList.iterator(); iterator.hasNext();) {
					IndexMappingFieldVo indexMappingFieldVo = (IndexMappingFieldVo) iterator.next();
					builder.startObject(indexMappingFieldVo.getFieldName()).field("type",
							indexMappingFieldVo.getFiledType().type);
					// 分词
					if (indexMappingFieldVo.getFiledType() == FiledType.Text) {
						builder.field("analyzer", indexMappingFieldVo.getAnalyzer().type).field("search_analyzer",
								indexMappingFieldVo.getSearchAnalyzer().type);
					}
					builder.endObject();
				}
			}

			builder.endObject()// properties
					.endObject();// root
			putMappingRequest.source(builder);
			putMappingRequest.setTimeout(TimeValue.timeValueMinutes(1));
			putMappingRequest.setMasterTimeout(TimeValue.timeValueMinutes(1));
			AcknowledgedResponse response = restHighLevelClient.indices().putMapping(putMappingRequest,
					RequestOptions.DEFAULT);
			if (response.isAcknowledged()) {
				resultVo.setSuccess(true);
			} else {
				resultVo.setSuccess(false);
			}
		} catch (Exception e) {
			String errorMsg = "创建索引【" + indexName + "】with mapping失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	@Override
	public boolean existsIndex(String index) throws IOException {
		GetIndexRequest request = new GetIndexRequest(index);
		boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
		return exists;
	}

	@Override
	public ResultVo<Object> deleteIndex(String indexName) {
		ResultVo<Object> resultVo = new ResultVo<Object>(false);
		DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indexName);
		try {
			restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
			resultVo.setSuccess(true);
		} catch (IOException e) {
			String errorMsg = "删除索引【" + indexName + "】失败！";
			resultVo.setSuccess(false);
			resultVo.setMsg(errorMsg + e.getMessage());
			logger.error(errorMsg, e);
		}
		return resultVo;
	}

	// ---------------------tools
	/**
	 * 判断id是否有值
	 * 
	 * @param modulefeatureBean
	 * @throws ServiceException
	 */
	private String getIdValue(Object doc, String idFileName) throws ServiceException {
		try {
			doc.getClass().getDeclaredField(idFileName);
			String idValue = org.apache.commons.beanutils.BeanUtils.getProperty(doc, idFileName);
			if (StrUtil.isNotBLank(idValue)) {
				return idValue;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("无法从" + doc + "获取" + idFileName + "值！", e);
			throw new ServiceException("无法从" + doc + "获取" + idFileName + "值！", e);
		}
	}
}
