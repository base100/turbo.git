package com.htax.turbo.innercompent.search.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WsdIndexMappingField {

	boolean isId() default false;
	/**
	 * 字段名
	 */
	 String fieldName() default "";
	/**
	 * 字段类型
	 */
	 FiledType filedType() default FiledType.Text;
	/**
	 * 解析器
	 */
	 Analyzer analyzer() default Analyzer.IkMaxWord;
	/**
	 * 查询解析器
	 */
	 Analyzer searchAnalyzer() default Analyzer.IkMaxWord;
	 
	 public static  enum FiledType{
			Integer("integer"),
			Long("long"),
			Byte("byte"),
			Double("double"),
			
			Date("date"),
			
			GeoPoint("geo_point"),
			
			Ip("ip"),
			
			Text("text"),
			KeyWord("keyword"),
			//不索引，只是为了关联查询
			NotIndexButStore("_not_index_but_store");

			public String type;
			
			FiledType(String type){
				this.type = type;
			}
		}
		
		public static  enum Analyzer{
			Standard("standard"),
			IkSmart("ik_smart"),
			IkMaxWord("ik_max_word");
			
			public String type;
			
			Analyzer(String type){
				this.type = type;
			}
		}
}
