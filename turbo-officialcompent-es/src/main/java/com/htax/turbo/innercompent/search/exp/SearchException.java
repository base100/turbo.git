package com.htax.turbo.innercompent.search.exp;

import com.htax.turbo.innercompent.base.exception.Logable;
import com.htax.turbo.innercompent.base.exception.MessageAlertable;
/**
 * service 全文检索异常信息
 * @日期：2019-09-14下午11:17:32
 * @作者：joe
 */
public class SearchException extends Exception implements MessageAlertable,Logable{
	
		private static final long serialVersionUID = 1L;
	   
		public SearchException(Throwable e){
			super( e);
		}
		
		public SearchException(String msg,Throwable e){
			super(msg, e);
		}
		public SearchException(String msg){
			super(msg);
		}
	}