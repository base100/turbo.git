package com.htax.turbo.innercompent.search.parser;

import java.lang.reflect.Field;

/**
 * module 解析过滤接口
 * @日期：2012-12-14下午11:18:49
 * @作者：乔兵
 * @版权所有：joe
 * @版本：1.0
 */
public interface ModuleHandlerFilter {

	/**
	 * 自定义规则来决定当前的field是否不解析
	 * @param moduleField
	 * @return
	 */
	public boolean exclude(Field moduleField);
	
	/**
	 * 自定义规则来决定当前的field是否解析
	 * @param moduleField
	 * @return
	 */
	public boolean include(Field moduleField);
}
