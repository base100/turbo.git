package com.htax.turbo.innercompent.search.parser;

import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.Analyzer;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.FiledType;

/**
 * 
 * @author eric
 *
 */
public class IndexMappingFieldVo {

	private boolean isId;
	/**
	 * 字段名
	 */
	private String fieldName;
	/**
	 * 字段类型
	 */
	private FiledType filedType;
	/**
	 * 解析器
	 */
	private Analyzer analyzer;
	/**
	 * 查询解析器
	 */
	private Analyzer searchAnalyzer;
	
	public static IndexMappingFieldVo integerFieldVo(String filedName) {
		IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
		indexMappingFieldVo.setFieldName(filedName);
		indexMappingFieldVo.setFiledType(FiledType.Integer);
		return indexMappingFieldVo;
	}
	public static IndexMappingFieldVo dateFieldVo(String filedName) {
		IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
		indexMappingFieldVo.setFieldName(filedName);
		indexMappingFieldVo.setFiledType(FiledType.Date);
		return indexMappingFieldVo;
	}
	public static IndexMappingFieldVo doubleFieldVo(String filedName) {
		IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
		indexMappingFieldVo.setFieldName(filedName);
		indexMappingFieldVo.setFiledType(FiledType.Double);
		return indexMappingFieldVo;
	}
	public static IndexMappingFieldVo keywordFieldVo(String filedName) {
		IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
		indexMappingFieldVo.setFieldName(filedName);
		indexMappingFieldVo.setFiledType(FiledType.KeyWord);
		return indexMappingFieldVo;
	}
	public static IndexMappingFieldVo ikMaxWord_IkSmartFieldVo(String filedName) {
		IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
		indexMappingFieldVo.setFieldName(filedName);
		indexMappingFieldVo.setFiledType(FiledType.Text);
		indexMappingFieldVo.setAnalyzer(Analyzer.IkMaxWord);
		indexMappingFieldVo.setSearchAnalyzer(Analyzer.IkSmart);
		return indexMappingFieldVo;
	}
	public static IndexMappingFieldVo ikMaxWord_ikMaxWordFieldVo(String filedName) {
		IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
		indexMappingFieldVo.setFieldName(filedName);
		indexMappingFieldVo.setFiledType(FiledType.Text);
		indexMappingFieldVo.setAnalyzer(Analyzer.IkMaxWord);
		indexMappingFieldVo.setSearchAnalyzer(Analyzer.IkMaxWord);
		return indexMappingFieldVo;
	}
	

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Analyzer getAnalyzer() {
		return analyzer;
	}

	public void setAnalyzer(Analyzer analyzer) {
		this.analyzer = analyzer;
	}

	public Analyzer getSearchAnalyzer() {
		return searchAnalyzer;
	}

	public void setSearchAnalyzer(Analyzer searchAnalyzer) {
		this.searchAnalyzer = searchAnalyzer;
	}
	public FiledType getFiledType() {
		return filedType;
	}
	public void setFiledType(FiledType filedType) {
		this.filedType = filedType;
	}
	public boolean isId() {
		return isId;
	}
	public void setId(boolean isId) {
		this.isId = isId;
	}
	@Override
	public String toString() {
		return "IndexMappingFieldVo [isId=" + isId + ", fieldName=" + fieldName + ", filedType=" + filedType
				+ ", analyzer=" + analyzer + ", searchAnalyzer=" + searchAnalyzer + "]";
	}
	
	
}
