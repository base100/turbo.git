package com.htax.turbo.innercompent.search.impl;

import java.util.List;

import com.htax.core.tools.util.api.ResultVo;

public class SearchPager<T> extends ResultVo<T>{

	private Integer limit;
	private Integer page;
	
	private long total;
	private List<T> datas;
	
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer size) {
		this.limit = size;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer currentPage) {
		this.page = currentPage;
	}

	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<T> getDatas() {
		return datas;
	}
	public void setDatas(List<T> datas) {
		this.datas = datas;
	}
	@Override
	public String toString() {
		return "SearchPager [limit=" + limit + ", page=" + page + ", total=" + total + ", datas=" + datas + "]";
	}
	
	
}
