package com.htax.turbo.innercompent.search.impl;

import org.elasticsearch.search.SearchHit;

public interface SearchHitRowWapper<T > {

	public T wapper(SearchHit hit);
}
