package com.htax.turbo.innercompent.search.parser;

import java.util.List;

public class HighlightItem {

	private String filedName;
	private List<String> texts;
	public String getFiledName() {
		return filedName;
	}
	public void setFiledName(String filedName) {
		this.filedName = filedName;
	}
	public List<String> getTexts() {
		return texts;
	}
	public void setTexts(List<String> texts) {
		this.texts = texts;
	}
	
}
