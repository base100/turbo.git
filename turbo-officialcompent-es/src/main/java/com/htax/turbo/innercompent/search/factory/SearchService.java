package com.htax.turbo.innercompent.search.factory;

import org.apache.log4j.Logger;

import com.htax.turbo.innercompent.base.para.ParaGetTool;
import com.htax.turbo.innercompent.search.impl.EsSearchClientImpl;

/**
 * 创建全文检索的工厂
 * @author qiao
 * 2017-5-26 下午3:12:00
 */
public class SearchService {

	static Logger logger = Logger.getLogger(SearchService.class);
	
	private static String searchType = ParaGetTool.getKernelPara("compent.search.type","es");
	private static String searchServiceIp = ParaGetTool.getPara("compent.search.es.service.ip","localhost");
	private static int searchServicePort = ParaGetTool.getPara(Integer.class,"compent.search.es.service.port",9200);
	private static SearchClient searchClient = null;
	
	public static SearchClient getSearchClilent(){
		if(searchClient==null){
			createSearchClient();
		}
		return searchClient;
	}
	public static synchronized void createSearchClient(){
		if(searchType.trim().equalsIgnoreCase("es")) {
			searchClient = new EsSearchClientImpl(searchServiceIp,searchServicePort);
		}else {
			throw new  RuntimeException("不支持的全文检索服务类型->"+searchClient);
		}
		try {
			searchClient.testConnect();
		} catch (Exception e) {
			throw new  RuntimeException("【全文检索组件】启动失败！",e);
		}
		
		logger.info("【全文检索组件】启动成功，使用【"+searchType+"】");
	} 
	public static void closeSearchService(){
		
	}
}
