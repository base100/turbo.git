package com.htax.turbo.innercompent.search.starter;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.htax.turbo.innercompent.base.boot.BootParam;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.interceptor.InterceptorBean.SIGNAL;
import com.htax.turbo.innercompent.base.listener.AppInitListener;
import com.htax.turbo.innercompent.search.factory.SearchService;
import com.htax.turbo.innercompent.search.parser.IndexParser;


/**
 * 全文检索 linstener
 * @author joe
 * 2019-4-19 下午4:52:06
 */
public class SearchInitListener implements AppInitListener {
	
	private static Logger logger = Logger.getLogger(SearchInitListener.class);
	
	public void init(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
        try {
        	parseIndexs(contextEvent.getServletContext(),bootParam);
        	
        	SearchService.createSearchClient();
        	
        	printLog(contextEvent,"初始化全文检索服务成功.");
		} catch (Exception e) {
			printLog(contextEvent,"初始化全文检索服务失败");
			throw new InitException(e);
		}
	}

	 /**
	  * 解析module
     * @param servletContext
     * @param bootParam
     */
	private void parseIndexs(ServletContext servletContext,BootParam bootParam) {
		List<String> pkgList = bootParam.getModulePackage();
		String modulePackage = "";
		for (String pkg : pkgList) {
			modulePackage += pkg + "|";
		}
		modulePackage = StringUtils.substringBeforeLast(modulePackage, "|");
		IndexParser.MODULE_PACKAGE = modulePackage;
		IndexParser.getInstance().parase(servletContext);
	}
	
	public void destroyed(ServletContextEvent contextEvent, BootParam bootParam) throws InitException {
		SearchService.closeSearchService();
		printLog(contextEvent,"全文检索服务关闭成功.");
	}
	public SIGNAL onError(ServletContextEvent contextEvent, BootParam bootParam)
			throws InitException {
		return SIGNAL.STOP;
	}
	private void printLog(ServletContextEvent contextEvent,String msg) {
		if(contextEvent!=null){
			contextEvent.getServletContext().log(msg);
		}else {
			logger.info(msg);
		}
	}
}
