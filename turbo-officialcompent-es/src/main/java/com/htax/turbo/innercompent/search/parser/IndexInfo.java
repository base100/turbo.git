package com.htax.turbo.innercompent.search.parser;
/**
 * 索引名称
 * @author eric
 *
 */
public class IndexInfo {

	private String indexName;
	
	
	public IndexInfo(String indexName) {
		this.indexName = indexName;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	@Override
	public String toString() {
		return "IndexInfo [indexName=" + indexName + "]";
	}
	
}
