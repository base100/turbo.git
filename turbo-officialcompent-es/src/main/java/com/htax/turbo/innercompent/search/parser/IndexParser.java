package com.htax.turbo.innercompent.search.parser;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.ClassUtils;

import com.htax.turbo.innercompent.search.annotation.WsdIndex;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.Analyzer;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.FiledType;


/**
 * index类的扫描和解析类
 * @日期：2019-09-14下午11:18:56
 * @作者：joe
 */
public class IndexParser {

	public static IndexParser INSTANCE = new IndexParser();

	public static String MODULE_PACKAGE = "*.*";

	private IndexParser() {
	}

	public static IndexParser getInstance() {
		return INSTANCE;
	}

	private static Logger logger = Logger.getLogger(IndexParser.class);

	private ModuleHandlerFilter moduleHanderFilter = new DefaultModuleHandlerFilter();

	//module的table信息
	private static Map<Class<?>, IndexInfo> indexBeans = new HashMap<Class<?>, IndexInfo>();
	private static Map<Class<?>, Map<String, IndexMappingFieldVo>> indexFields = new HashMap<Class<?>, Map<String, IndexMappingFieldVo>>();

	private static Map<Class<?>, String> idFields = new HashMap<Class<?>, String>();
	
	public void parseIndexs(ModuleHandlerFilter moduleHanderFilter) {

		if (null == moduleHanderFilter)
			moduleHanderFilter = new DefaultModuleHandlerFilter();
		this.moduleHanderFilter = moduleHanderFilter;

		List<Class<?>> classes = getIndexModule(MODULE_PACKAGE);

		for (Iterator<Class<?>> iterator = classes.iterator(); iterator.hasNext();) {
			Class<?> clazz = (Class<?>) iterator.next();
			parseIndex(clazz);
		}
	}
	
	private List<Class<?>> getIndexModule(String basePackage) {
		
		final List<Class<?>> indexModules = new ArrayList<Class<?>>();
		
		ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(false);
		provider.addIncludeFilter(new TypeFilter() {
			public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory)
					throws IOException {
					Set<String> ms = metadataReader.getAnnotationMetadata().getAnnotationTypes();
					boolean hasIndexAnnonation = ms.contains(WsdIndex.class.getName());
				if (hasIndexAnnonation) {
					String className = metadataReader.getClassMetadata().getClassName();
					logger.debug("找到  索引类 >>>>" + className);
					try {
						indexModules.add((Class<?>) ClassUtils.forName(className,
								ClassUtils.getDefaultClassLoader()));
					} catch (Exception e) {
						logger.error("注册module[" + className + "]出错，查看是否有无参数public的构造函数", e);
					}
					return true;
				}
				return false;
			}
		});
		String[] ps = basePackage.split("\\|");
		for (int i = 0; i < ps.length; i++) {
			provider.findCandidateComponents(ps[i]);
		}

		return indexModules;
	}

	private void parseIndex(Class<?> clazz) {
		logger.debug("解析 index >>>>" + clazz.getName());
		WsdIndex wsdIndex = AnnotationUtils.findAnnotation(clazz, WsdIndex.class);
		String indexName = wsdIndex.indexName();
		
		if(StringUtils.isBlank(indexName)){
			logger.error("解析结束 index 出错 未配置indexName >>>>" + clazz.getName() + " ");
			return;
		}
		indexBeans.put(clazz, new IndexInfo(indexName));
		parseFileds(clazz);
		logger.debug("解析结束 index >>>>" + clazz.getName() + " ");
	}

	private void parseFileds(Class<?> clazz) {
		Field[] moduleFs = clazz.getDeclaredFields();
		indexFields.put(clazz, new LinkedHashMap<String, IndexMappingFieldVo>());
		for (Field field : moduleFs) {
			if (moduleHanderFilter.include(field) && !moduleHanderFilter.exclude(field)) {
				indexFields.get(clazz).putAll(parseFiled(clazz, field));
			}
		}
	}

	private Map<String, IndexMappingFieldVo> parseFiled(Class<?> clazz, Field field) {
		String fieldName = field.getName();
		Map<String, IndexMappingFieldVo> ccmap = new HashMap<String, IndexMappingFieldVo>();
		WsdIndexMappingField fieldAnnotation = field.getAnnotation(WsdIndexMappingField.class);
		if (null != fieldAnnotation) {
			String indexfieldName = fieldAnnotation.fieldName();
			indexfieldName = StringUtils.isNotBlank(indexfieldName) ? indexfieldName : fieldName;
			FiledType filedType = fieldAnnotation.filedType();
			Analyzer analyzer = fieldAnnotation.analyzer();
			Analyzer searchAnalyzer = fieldAnnotation.searchAnalyzer();
			boolean isId = fieldAnnotation.isId();
			if(filedType==null) {
				logger.error("字段[" + field + "]没有索引类型，无法索引。");
				throw new RuntimeException("字段[" + field + "]没有索引类型，无法索引。");
			}
			IndexMappingFieldVo indexMappingFieldVo = new IndexMappingFieldVo();
			indexMappingFieldVo.setFieldName(indexfieldName);
			indexMappingFieldVo.setFiledType(filedType);
			indexMappingFieldVo.setAnalyzer(analyzer);
			indexMappingFieldVo.setSearchAnalyzer(searchAnalyzer);
			indexMappingFieldVo.setId(isId);
			ccmap.put(fieldName, indexMappingFieldVo );
			if(isId&&idFields.containsKey(clazz)) {
				logger.error("[" + clazz + "]包含多个id类型字段，请检查!");
				throw new RuntimeException("[" + clazz + "]包含多个id类型字段，请检查!");
			}
			if(isId) {
				idFields.put(clazz, indexfieldName);
			}
			logger.info("成功解析字段[" + field + "]为索引字段>>"+indexMappingFieldVo);
			
		} else {
			logger.warn("字段[" + field + "]没有WsdIndexMappingField 注解，无法索引。");
		}
		return ccmap;

	}

	public void parase(ServletContext servletContext) {
		try {
			parseIndexs(moduleHanderFilter);
		} catch (Exception e) {
			if (servletContext != null)
				servletContext.log("解析index 失败！package>>" + MODULE_PACKAGE, e);
		}
		if (servletContext != null)
			servletContext.log("解析index 成功！package>>" + MODULE_PACKAGE);
	}


	public static IndexInfo getIndex(Class<?> clazz) {
		return indexBeans.get(clazz);
	}

	public static String getIdFieldName(Class<?> clazz) {
		return idFields.get(clazz);
	}
	public static Map<String, IndexMappingFieldVo> getIndexField(Class<?> clazz) {
		return indexFields.get(clazz);
	}
	public static List<String> getIndexNoStoredFieldsName(Class<?> clazz) {
		List<String> indexNoStoredFields = new ArrayList<String>();
		Map<String, IndexMappingFieldVo> all = indexFields.get(clazz);
		for (Iterator<String> iterator = all.keySet().iterator(); iterator.hasNext();) {
			String imf = (String) iterator.next();
			if(all.get(imf).getFiledType()!=FiledType.NotIndexButStore) {
				indexNoStoredFields.add(imf);
			}
		}
		return indexNoStoredFields;
	}
	public static List<IndexMappingFieldVo> getIndexNoStoredFieldsBean(Class<?> clazz) {
		List<IndexMappingFieldVo> indexNoStoredFields = new ArrayList<IndexMappingFieldVo>();
		Map<String, IndexMappingFieldVo> all = indexFields.get(clazz);
		for (Iterator<String> iterator = all.keySet().iterator(); iterator.hasNext();) {
			String imf = (String) iterator.next();
			if(all.get(imf).getFiledType()!=FiledType.NotIndexButStore) {
				indexNoStoredFields.add(all.get(imf));
			}
		}
		return indexNoStoredFields;
	}
	public static List<String> getIndexNoStoredStringFieldsName(Class<?> clazz) {
		List<String> indexNoStoredFields = new ArrayList<String>();
		Map<String, IndexMappingFieldVo> all = indexFields.get(clazz);
		for (Iterator<String> iterator = all.keySet().iterator(); iterator.hasNext();) {
			String imf = (String) iterator.next();
			IndexMappingFieldVo v = all.get(imf);
			if(v.getFiledType()==FiledType.KeyWord||v.getFiledType()==FiledType.Text) {
				indexNoStoredFields.add(imf);
			}
		}
		return indexNoStoredFields;
	}
	public ModuleHandlerFilter getModuleHanderFilter() {
		return moduleHanderFilter;
	}

	public void setModuleHanderFilter(ModuleHandlerFilter moduleHanderFilter) {
		this.moduleHanderFilter = moduleHanderFilter;
	}

}
