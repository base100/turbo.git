package com.htax.turbo.innercompent.search.db;

import java.util.Date;

import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;
import com.htax.turbo.innercompent.search.annotation.WsdIndex;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.Analyzer;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.FiledType;
import com.htax.turbo.innercompent.search.parser.SearchFeature;

/**
 * 数据源实体
 *@author qiaobing
*/
@WsdTable(name="source_meta")
@WsdIndex(indexName ="source_meta1")
public class SourceMetaVo extends SearchFeature{

	@WsdColumn(isId=true)
	@WsdIndexMappingField(isId = true,filedType = FiledType.KeyWord)
	private String id;
	
	@WsdIndexMappingField(filedType = FiledType.Text,analyzer = Analyzer.IkMaxWord,searchAnalyzer = Analyzer.IkSmart)
	private String sourceName;//数据源名称
	
	@WsdIndexMappingField(filedType = FiledType.KeyWord)
	private String sourceType;//数据源类型
	
	private String sourceUrl;//URL
	
	@WsdIndexMappingField(filedType = FiledType.KeyWord)
	private String userName;//账号
	
	private String passWord;//密码
	
	@WsdIndexMappingField(filedType = FiledType.Text,analyzer = Analyzer.IkMaxWord,searchAnalyzer = Analyzer.IkSmart)
	private String description;//描述

	private String tablelayout;//表排版
	
	public String getTablelayout() {
		return tablelayout;
	}

	public void setTablelayout(String tablelayout) {
		this.tablelayout = tablelayout;
	}

	private Date  ts;
	
	private String pid;//预留
	
	@WsdNotDbColumn
	private String schemaName;
	
	@WsdNotDbColumn
	private String title;
	
	@WsdNotDbColumn
	private String tableName;//英文标识

	public static enum DB_TYPE{
		MYSQL("1","MYSQL"),
		DM("2","DM"),
		ORACLE("3","ORACLE");
		
		
		public String code;
		public String name;
		DB_TYPE(String code,String name){
			this.code = code;
			this.name = name;
		}
		public static DB_TYPE getByCode(String code) {
			DB_TYPE[] dTypes = DB_TYPE.values();
			for (int i = 0; i < dTypes.length; i++) {
				if(dTypes[i].code.equalsIgnoreCase(code)) {
					return dTypes[i];
				}
			}
			return null;
		}
		
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) {
		this.schemaName = schemaName;
	}

	@Override
	public String toString() {
		return "SourceMetaVo [id=" + id + ", sourceName=" + sourceName + ", sourceType=" + sourceType + ", sourceUrl="
				+ sourceUrl + ", userName=" + userName + ", passWord=" + passWord + ", description=" + description
				+ ", tablelayout=" + tablelayout + ", ts=" + ts + ", pid=" + pid + ", schemaName=" + schemaName
				+ ", title=" + title + ", tableName=" + tableName + ", getHighlightItems()=" + getHighlightItems()
				+ ", get_score()=" + get_score() + "]";
	}


}
