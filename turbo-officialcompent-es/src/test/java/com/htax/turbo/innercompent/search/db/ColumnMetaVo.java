package com.htax.turbo.innercompent.search.db;

import java.util.Date;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;

/**
 * 列明信息实体类，用于存放列名信息的表
 * @author liulei
 * 脚本如下：
 */
@WsdTable(name="column_meta")
public class ColumnMetaVo extends ModuleFeatureBean{

	@WsdColumn(isId = true)
	private String id;
	
	@WsdColumn(name="column_name")
	private String columnName;//字段名
	
	@WsdColumn(name="column_ch")
	private String columnCh;//字段中文名
	
	@WsdColumn(name="table_id")
	private String tableId;//表源数据id
	
	@WsdColumn(name="source_id")
	private String sourceId;//数据源id,方便查询和重置的冗余字段。
	
	private Date ts;//时间戳
	
	@WsdColumn(name="column_type")
	private String columnType;//字段类型
	
	@WsdColumn(name="is_nullable")
	private String isNullable;//是否为空  0 不可为空 1 能为空
	
	public String getIsNullable() {
		return isNullable;
	}

	public void setIsNullable(String isNullable) {
		this.isNullable = isNullable;
	}

	@WsdNotDbColumn
	private String tableName;
	@WsdNotDbColumn
	private String colNameCh;//用于界面显示，同时包含中文和英文
	
	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnCh() {
		return columnCh;
	}

	public void setColumnCh(String columnCh) {
		this.columnCh = columnCh;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getTableName() {
		return tableName;
	}

	public String getColNameCh() {
		return colNameCh;
	}

	public void setColNameCh(String colNameCh) {
		this.colNameCh = colNameCh;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Override
	public String toString() {
		return "ColumnMetaVo [id=" + id + ", columnName=" + columnName + ", columnCh=" + columnCh + ", tableId="
				+ tableId + ", sourceId=" + sourceId + ", ts=" + ts + ", tableName=" + tableName + "]";
	}

	
}
