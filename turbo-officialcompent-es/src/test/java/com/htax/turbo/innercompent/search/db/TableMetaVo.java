package com.htax.turbo.innercompent.search.db;

import java.util.Date;
import java.util.List;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;

/**
 * 用于存放表名信息
 * 
 * @author liulei 脚本如下：
 */
@WsdTable(name = "table_meta")
public class TableMetaVo extends ModuleFeatureBean {

	@WsdColumn(isId = true)
	private String id;

	@WsdColumn(name = "table_name")
	private String tableName;// 表名

	@WsdColumn(name = "table_ch")
	private String tableCh;// 表中文名

	private Date ts;// 时间戳

	@WsdColumn(name = "source_id")
	private String sourceId;// 数据源id
	@WsdColumn(name = "row_num")
	private String rowNum;

	@WsdNotDbColumn
	private String sourceName;

	@WsdNotDbColumn
	private List<ColumnMetaVo> columnMetas;// ER图使用，字段信息

	@WsdNotDbColumn
	private List<TableMetaVo> relTables;// ER图使用，关联表
	@WsdNotDbColumn
	private String tableNameCh;// 用于前段显示使用

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableCh() {
		return tableCh;
	}

	public void setTableCh(String tableCh) {
		this.tableCh = tableCh;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public List<ColumnMetaVo> getColumnMetas() {
		return columnMetas;
	}

	public void setColumnMetas(List<ColumnMetaVo> columnMetas) {
		this.columnMetas = columnMetas;
	}

	public String getRowNum() {
		return rowNum;
	}

	public void setRowNum(String rowNum) {
		this.rowNum = rowNum;
	}

	public List<TableMetaVo> getRelTables() {
		return relTables;
	}

	public void setRelTables(List<TableMetaVo> relTables) {
		this.relTables = relTables;
	}

	public String getTableNameCh() {
		return tableNameCh;
	}

	public void setTableNameCh(String tableNameCh) {
		this.tableNameCh = tableNameCh;
	}

}
