package com.htax.turbo.innercompent.search;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.htax.core.tools.util.api.ResultVo;
import com.htax.turbo.innercompent.base.DaoService;
import com.htax.turbo.innercompent.base.exception.DaoException;
import com.htax.turbo.innercompent.base.exception.InitException;
import com.htax.turbo.innercompent.base.test.BaseTest;
import com.htax.turbo.innercompent.search.db.SourceMetaVo;
import com.htax.turbo.innercompent.search.factory.SearchClient;
import com.htax.turbo.innercompent.search.factory.SearchService;
import com.htax.turbo.innercompent.search.impl.SearchPager;
import com.htax.turbo.innercompent.search.parser.IndexParser;
import com.htax.turbo.innercompent.starter.DaoInitListener;

public class SearchTestWithDb extends BaseTest{

	private SearchClient searchClient;
	@Before
	public void init() throws InitException, Exception {
		IndexParser.MODULE_PACKAGE = "com.htax.turbo.innercompent.search";
		IndexParser.getInstance().parase(null);
		searchClient = SearchService.getSearchClilent();
		
		DaoInitListener daoInitListener = new DaoInitListener();
		daoInitListener.init(super.getContext());
	}
	
	Logger logger = Logger.getLogger(SearchTestWithDb.class);
	@Test
	public void testLoadTableDataToEs() throws DaoException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		List<SourceMetaVo> ss = DaoService.getDao().listModule("查询整个表", SourceMetaVo.class, null);
		ResultVo<Object> s = searchClient.bulkAddDocByBean(ss);
		System.out.println("索引成功："+s.isSuccess());
	}
	@Test
	public void testIndexTableEs() throws DaoException {
		
		searchClient.createOrUpdateIndexByBean(SourceMetaVo.class, false);
	}
	@Test
	public void testSearch() throws Exception {
		SearchPager<SourceMetaVo> s  = searchClient.pagerMultiMatchSearchOfBean(SourceMetaVo.class, 0, 100, "测试");
		System.out.println(s.getTotal());
		for (Iterator<SourceMetaVo> iterator = s.getDatas().iterator(); iterator.hasNext();) {
			SourceMetaVo r = (SourceMetaVo) iterator.next();
			System.out.println(r);
		}
	}
		
	
}
