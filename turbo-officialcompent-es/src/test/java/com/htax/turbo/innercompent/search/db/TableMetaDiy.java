package com.htax.turbo.innercompent.search.db;

import java.util.Date;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;

/**
 * 表元数据，和数据集元数据管理是同一个
 * 
 * @author qiao 2020年4月30日下午3:43:06
 */
@WsdTable(name = "table_meta_diy")
public class TableMetaDiy extends ModuleFeatureBean {

	@WsdColumn(isId = true)
	private String id;

	@WsdColumn(name = "dataset_id")
	private String dataSetId;// 此字段存表id或者数据集id

	private String description;

	@WsdColumn(name = "diy_value")
	private String diyValue;

	@WsdColumn(name = "dic_diy_value")
	private String dicDiyValue;// 自定义字典项的id，注意不是字典id
	
	@WsdNotDbColumn
	private String dicDiyValue2;// 自定义字典项的id，注意不是字典id

	public String getDicDiyValue2() {
		return dicDiyValue2;
	}

	public void setDicDiyValue2(String dicDiyValue2) {
		this.dicDiyValue = dicDiyValue2;
		this.dicDiyValue2 = dicDiyValue2;
	}

	@WsdColumn(name = "metaitem_id")
	private String metaItemId;

	@WsdNotDbColumn
	private String metaItemName;// 用于界面显示
	@WsdNotDbColumn
	private String metaItemCatalogName;// 用于界面元数据项分类合并显示
	@WsdNotDbColumn
	private String rawType;// 元数据项目类型
	@WsdNotDbColumn
	private String flag;// 是否固定值

	private Date ts;
	@WsdNotDbColumn
	private String dicDiyValueName;// 自定义字典项的名称，注意不是字典名称
	@WsdNotDbColumn
	private String stringFixValue;// 字符串固定值

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDicDiyValueName() {
		return dicDiyValueName;
	}

	public void setDicDiyValueName(String dicDiyValueName) {
		this.dicDiyValueName = dicDiyValueName;
	}

	public String getDataSetId() {
		return dataSetId;
	}

	public void setDataSetId(String dataSetId) {
		this.dataSetId = dataSetId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDicDiyValue() {
		return dicDiyValue;
	}

	public void setDicDiyValue(String dicDiyValue) {
		this.dicDiyValue = dicDiyValue;
	}

	public String getDiyValue() {
		return diyValue;
	}

	public void setDiyValue(String diyValue) {
		this.diyValue = diyValue;
	}

	public String getStringFixValue() {
		return stringFixValue;
	}

	public void setStringFixValue(String stringFixValue) {
		this.stringFixValue = stringFixValue;
	}

	public String getMetaItemId() {
		return metaItemId;
	}

	public void setMetaItemId(String metaItemId) {
		this.metaItemId = metaItemId;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getMetaItemName() {
		return metaItemName;
	}

	public void setMetaItemName(String metaItemName) {
		this.metaItemName = metaItemName;
	}

	public String getMetaItemCatalogName() {
		return metaItemCatalogName;
	}

	public void setMetaItemCatalogName(String metaItemCatalogName) {
		this.metaItemCatalogName = metaItemCatalogName;
	}

	public String getRawType() {
		return rawType;
	}

	public void setRawType(String rawType) {
		this.rawType = rawType;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}
