package com.htax.turbo.innercompent.search.db;

import java.util.Date;

import com.htax.turbo.innercompent.base.ModuleFeatureBean;
import com.htax.turbo.innercompent.orm.annotation.WsdColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdNotDbColumn;
import com.htax.turbo.innercompent.orm.annotation.WsdTable;

/**
 * 表字段元数据
 * 
 * @author qiaobing
 *
 */
@WsdTable(name = "table_column_meta_diy")
public class TableColumnMetaDiy extends ModuleFeatureBean {

	@WsdColumn(isId = true)
	private String id;

	@WsdColumn(name = "column_id")
	private String columnId;

	@WsdColumn(name = "diy_value")
	private String diyValue;

	@WsdColumn(name = "dic_diy_value")
	private String dicDiyValue;// 自定义字典项的id，注意不是字典id

	@WsdColumn(name = "dic_id")
	private String dicId;
	@WsdColumn(name = "metaitem_id")
	private String metaItemId;
	@WsdNotDbColumn
	private String dicSigle;// 关联字典是否单选项
	@WsdNotDbColumn
	private String rawType;// 元数据项目类型
	@WsdNotDbColumn
	private String flag;// 是否固定值

	private Date ts;

	@WsdNotDbColumn
	private String metaItemName;// 用于界面显示
	@WsdNotDbColumn
	private String columnName;// 用于界面显示
	@WsdNotDbColumn
	private String columnType;// 用于界面显示
	@WsdNotDbColumn
	private String columnCh;// 用于界面显示
	@WsdNotDbColumn
	private String metaItemCatalogName;// 用于界面元数据项分类合并显示
	@WsdNotDbColumn
	private String fixValue;

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getColumnId() {
		return columnId;
	}

	public String getDicDiyValue() {
		return dicDiyValue;
	}

	public void setDicDiyValue(String dicDiyValue) {
		this.dicDiyValue = dicDiyValue;
	}

	public String getDicSigle() {
		return dicSigle;
	}

	public void setDicSigle(String dicSigle) {
		this.dicSigle = dicSigle;
	}

	public void setColumnId(String columnId) {
		this.columnId = columnId;
	}

	public String getDiyValue() {
		return diyValue;
	}

	public void setDiyValue(String diyValue) {
		this.diyValue = diyValue;
	}

	public String getMetaItemId() {
		return metaItemId;
	}

	public void setMetaItemId(String metaItemId) {
		this.metaItemId = metaItemId;
	}

	public String getRawType() {
		return rawType;
	}

	public void setRawType(String rawType) {
		this.rawType = rawType;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getMetaItemName() {
		return metaItemName;
	}

	public void setMetaItemName(String metaItemName) {
		this.metaItemName = metaItemName;
	}

	public String getMetaItemCatalogName() {
		return metaItemCatalogName;
	}

	public void setMetaItemCatalogName(String metaItemCatalogName) {
		this.metaItemCatalogName = metaItemCatalogName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnCh() {
		return columnCh;
	}

	public void setColumnCh(String columnCh) {
		this.columnCh = columnCh;
	}

	public String getFixValue() {
		return fixValue;
	}

	public void setFixValue(String fixValue) {
		this.fixValue = fixValue;
	}

	public String getDicId() {
		return dicId;
	}

	public void setDicId(String dicId) {
		this.dicId = dicId;
	}
}
