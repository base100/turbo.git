package com.htax.turbo.innercompent.search;

import java.util.Date;

import com.htax.turbo.innercompent.search.annotation.WsdIndex;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.Analyzer;
import com.htax.turbo.innercompent.search.annotation.WsdIndexMappingField.FiledType;
import com.htax.turbo.innercompent.search.parser.SearchFeature;
@WsdIndex(indexName = "test_vo1")
public class TestVo extends SearchFeature{

	@WsdIndexMappingField(isId = true,filedType = FiledType.KeyWord)
	private String id;
	
	@WsdIndexMappingField(filedType = FiledType.KeyWord)
	private String name;
	
	@WsdIndexMappingField(filedType = FiledType.Integer)
	private int age;
	
	@WsdIndexMappingField(filedType = FiledType.Double)
	private double salary;
	
	@WsdIndexMappingField(filedType = FiledType.Text,analyzer = Analyzer.IkMaxWord,searchAnalyzer = Analyzer.IkSmart)
	private String remark;
	
	@WsdIndexMappingField(filedType = FiledType.Text,analyzer = Analyzer.IkMaxWord,searchAnalyzer = Analyzer.IkSmart)
	private String content;
	
	@WsdIndexMappingField(filedType = FiledType.Date)
	private Date ts;
	
	@WsdIndexMappingField(filedType = FiledType.NotIndexButStore)
	private String extName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getTs() {
		return ts;
	}
	public void setTs(Date ts) {
		this.ts = ts;
	}
	
	public String getExtName() {
		return extName;
	}
	public void setExtName(String extName) {
		this.extName = extName;
	}
	@Override
	public String toString() {
		return "TestVo [id=" + id + ", name=" + name + ", age=" + age + ", salary=" + salary + ", remark=" + remark
				+ ", content=" + content + ", ts=" + ts + "]";
	}
	
}
