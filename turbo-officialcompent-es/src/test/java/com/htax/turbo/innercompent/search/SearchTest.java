package com.htax.turbo.innercompent.search;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.search.SearchHit;
import org.junit.Before;
import org.junit.Test;

import com.htax.core.tools.util.UUIDGenerator;
import com.htax.core.tools.util.api.ResultVo;
import com.htax.turbo.innercompent.base.exception.ServiceException;
import com.htax.turbo.innercompent.search.factory.SearchClient;
import com.htax.turbo.innercompent.search.factory.SearchService;
import com.htax.turbo.innercompent.search.impl.SearchHitRowWapper;
import com.htax.turbo.innercompent.search.impl.SearchPager;
import com.htax.turbo.innercompent.search.parser.IndexParser;

/**
 * Unit test for simple App.
 */
public class SearchTest {

	private SearchClient searchClient;
	@Before
	public void init() {
		IndexParser.MODULE_PACKAGE = "com.htax.turbo.innercompent.search";
		IndexParser.getInstance().parase(null);
		searchClient = SearchService.getSearchClilent();
	}
	
	Logger logger = Logger.getLogger(SearchTest.class);
	
	@Test
	public void testIndex() throws ServiceException, IOException {
		
		ResultVo<Object> createIndexResult = searchClient.createOrUpdateIndexByBean(TestVo.class, false);
		System.out.println(createIndexResult);
	}
	@Test
	public void testExistsIndex() throws IOException   {
		System.out.println("existsIndex: " + searchClient.existsIndex("test"));
	}
	@Test
    public void testAddDoc() throws Throwable {
		List<TestVo> testVos = new ArrayList<TestVo>();
		List<String> lines = FileUtils.readLines(new File("d:\\index.txt"), "utf-8");
	    for (int i = 0; i < 100; i++) {
	        TestVo testVo = new TestVo();
	        testVo.setId(UUIDGenerator.getUUID());
	        testVo.setName("qiao-bing"+i);
	        testVo.setAge(20+i);
	        testVo.setSalary(1000.12d+i);
	        testVo.setContent(lines.get(i));
	        testVo.setRemark(lines.get(i+2));
	        testVo.setTs(new Date());
	        testVo.setExtName("Linux _name");
	        testVos.add(testVo);
	     }
	    searchClient.bulkAddDocByBean(testVos);
    }
	@Test
    public void testSearch() {
		SearchPager<TestVo>  ps = searchClient.pagerMultiMatchSearchOfBean(TestVo.class, 0, 10, "Linux ");
		for (Iterator<TestVo> iterator = ps.getDatas().iterator(); iterator.hasNext();) {
			TestVo t = (TestVo) iterator.next();
			System.out.println(t);
		}
		
    }
}
